<?php
ob_start("ob_gzhandler");
define('TIMEZONE', 'America/Mexico_City');
date_default_timezone_set(TIMEZONE);
setlocale(LC_ALL,"es_ES");
// Conexion a BD
	require_once("conexion.php"); // conexion a BD
	session_start(); // iniciar variables de sesion
	if(isset($_GET["cerrar"])){	// Si cerro sesion el usuario, ser&Aacute; redireccionado al login
		session_destroy();	// Destruir variables de seson
		header("Location:index.php");	// redireccion al login
	}
	if(@$_SESSION["iniciada"]==NULL){	// Si el usuario no ha iniciado sesion sera redireciconado al login
		header("Location:index.php");	// redireccion al login
	}else{								// Si el usuario ya inicio sesion ser&Aacute; redireccionado al index.php
?>
<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html><!--<![endif]-->

<!-- Specific Page Data -->

<!-- End of Data -->

<head>
    <meta charset="utf-8" />
    <title>Universidad Lasalle | Administrador Maestro</title>
    <meta name="keywords" content="CMS Maestro, Universidad Lasalle" />
    <meta name="description" content="CMS Maestro, Universidad Lasalle">
    <meta name="author" content="Universidad Lasalle">
    
    <!-- Set the viewport width to device width for mobile -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">    
    
    
    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="img/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="img/ico/favicon.png">
    
    
    <!-- CSS -->
       
    <!-- Bootstrap & FontAwesome & Entypo CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if IE 7]><link type="text/css" rel="stylesheet" href="css/font-awesome-ie7.min.css"><![endif]-->
    <link href="css/font-entypo.css" rel="stylesheet" type="text/css">    

    <!-- Fonts CSS -->
    <link href="css/fonts.css"  rel="stylesheet" type="text/css">
               
    <!-- Plugin CSS -->
    <link href="plugins/jquery-ui/jquery-ui.custom.min.css" rel="stylesheet" type="text/css">    
    <link href="plugins/prettyPhoto-plugin/css/prettyPhoto.css" rel="stylesheet" type="text/css">
    <link href="plugins/isotope/css/isotope.css" rel="stylesheet" type="text/css">
    <link href="plugins/pnotify/css/jquery.pnotify.css" media="screen" rel="stylesheet" type="text/css">    
	<link href="plugins/google-code-prettify/prettify.css" rel="stylesheet" type="text/css"> 
   
         
    <link href="plugins/mCustomScrollbar/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css">
    <link href="plugins/tagsInput/jquery.tagsinput.css" rel="stylesheet" type="text/css">
    <link href="plugins/bootstrap-switch/bootstrap-switch.css" rel="stylesheet" type="text/css">    
    <link href="plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css">    
    <link href="plugins/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css">
    <link href="plugins/colorpicker/css/colorpicker.css" rel="stylesheet" type="text/css">            

	<!-- Specific CSS -->
	<link href="plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"><link href="plugins/fullcalendar/fullcalendar.print.css" rel="stylesheet" type="text/css"><link href="plugins/introjs/css/introjs.min.css" rel="stylesheet" type="text/css">    
     
    <!-- Theme CSS -->
    <link href="css/theme.min.css" rel="stylesheet" type="text/css">
    <!--[if IE]> <link href="css/ie.css" rel="stylesheet" > <![endif]-->
    <link href="css/chrome.css" rel="stylesheet" type="text/chrome"> <!-- chrome only css -->    

	<!-- Texto enriquezido -->
	<link rel="stylesheet" type="text/css" href="css/bootstrap-wysihtml5.css">
    <link href="css/summernote.css" rel="stylesheet">
        
    <!-- Responsive CSS -->
	<link href="css/theme-responsive.min.css" rel="stylesheet" type="text/css"> 

    <!-- for specific page in style css -->
        
    <!-- for specific page responsive in style css -->
    
    <!-- Custom CSS -->
    <link href="custom/custom.css" rel="stylesheet" type="text/css">



    <!-- Head SCRIPTS -->
    <script type="text/javascript" src="js/modernizr.js"></script> 
    <script type="text/javascript" src="js/mobile-detect.min.js"></script> 
    <script type="text/javascript" src="js/mobile-detect-modernizr.js"></script> 
 
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script type="text/javascript" src="js/html5shiv.js"></script>
      <script type="text/javascript" src="js/respond.min.js"></script>     
    <![endif]-->
    <style>
		.foto-docente{display:block}
	</style>
</head>    

<body id="dashboard" class="full-layout nav-left-medium nav-right-hide nav-right-start-hide  nav-top-fixed      responsive    clearfix" data-active="dashboard "  data-smooth-scrolling="1">     
<div class="vd_body">
<!-- Header Start -->
  <header class="header-1" id="header">
      <div class="vd_top-menu-wrapper">
        <div class="container ">
          <div class="vd_top-nav vd_nav-width  ">
          <div class="vd_panel-header">
          	<div class="logo">
            	<a href="index.html"><img alt="logo" src="img/logo.png"></a>
            </div>
            <!-- logo
            <div class="vd_panel-menu  hidden-sm hidden-xs" data-intro="<strong>Mininzar menú izquierda</strong><br/>Puede ampliar o reducir el menu." data-step=1>
            		                	<span class="nav-medium-button menu" data-toggle="tooltip" data-placement="bottom" data-original-title="Menú Bloques" data-action="nav-left-medium">
	                    <i class="fa fa-bars"></i>
                    </span>
                                       
            </div>
            <div class="vd_panel-menu left-pos visible-sm visible-xs">
                                 
                        <span class="menu" data-action="toggle-navbar-left">
                            <i class="fa fa-ellipsis-v"></i>
                        </span>  
                            
                              
            </div>
            -->
            <div class="vd_panel-menu visible-sm visible-xs">
                	<span class="menu visible-xs" data-action="submenu">
	                    <i class="fa fa-bars"></i>
                    </span>        
                          
                    <!--<span class="menu visible-sm visible-xs" data-action="toggle-navbar-right">
                            <i class="fa fa-comments"></i>
                        </span> -->                  
                   	 
                </div>
          

            <!-- vd_panel-menu -->
          </div>
          <!-- vd_panel-header -->
            
          </div>    
          <div class="vd_container">
          	<div class="row">
            	<div class="col-sm-5 col-xs-12">
                </div>
                <div class="col-sm-7 col-xs-12">
              		<div class="vd_mega-menu-wrapper">
                    	<div class="vd_mega-menu pull-right">
            				<ul class="mega-ul">
    <li id="top-menu-1" class="one-icon mega-li"> 


    </li>
      </a>
      
     <!-- perfil administrador maestro --> 
    <li id="top-menu-profile" class="profile mega-li"> 
        <a href="#" class="mega-link"  data-action="click-trigger"> 
            <span  class="mega-image">
                <img src="img/avatar/avatar0.jpg" alt="example image" />               
            </span>
            <span class="mega-name">
                Universidad Lasalle / Admin Master <i class="fa fa-caret-down fa-fw"></i> 
            </span>
        </a> 
      <div class="vd_mega-menu-content  width-xs-3  left-xs left-sm" data-action="click-target">
        <div class="child-menu"> 
        	<div class="content-list content-menu">
                <ul class="list-wrapper pd-lr-10">
                    <li> <a href="#"> <div class="menu-icon"><i class=" fa fa-user"></i></div> <div class="menu-text">Perfil Administrador</div> </a> </li>
                    <li> <a href="lockscreen.html"> <div class="menu-icon"><i class="  fa fa-key"></i></div> <div class="menu-text">Bloquear Cuenta</div> </a> </li>
                    <li class="line"></li>                
                    <li> <a href="logout.html"> <div class="menu-icon"><i class=" fa fa-sign-out"></i></div> <div class="menu-text">Salirme</div> </a> </li> 
                </ul>
            </div> 
        </div> 
      </div>     
  
    </li>               
       

	</ul>
<!-- Head menu search form ends -->                         
                        </div>
                    </div>
                </div>

            </div>
          </div>
        </div>
        <!-- container --> 
      </div>
      <!-- vd_primary-menu-wrapper --> 

  </header>
  <!-- Header Ends --> 
<div class="content">
  <div class="container">
    <div class="vd_navbar vd_nav-width vd_navbar-tabs-menu vd_navbar-left ">
	<div class="navbar-tabs-menu clearfix">
			<span class="expand-menu" data-action="expand-navbar-tabs-menu">
            	<span class="menu-icon menu-icon-left">
            		<i class="fa fa-ellipsis-h"></i>
                 
                </span>
            	<span class="menu-icon menu-icon-right">
            		<i class="fa fa-ellipsis-h"></i>
                    <span class="badge vd_bg-red">
                        20
                    </span>                    
                </span>                
            </span>
    </div>
	<div class="navbar-menu clearfix">
        <div class="vd_menu">
			<!-- Head menu search form ends -->  
			<?php include "sidebar.php"; ?>
		</div>             
    </div>
    <div class="navbar-spacing clearfix">
    </div>
    <div class="vd_menu vd_navbar-bottom-widget">
        <ul>
            <li>
                <a href="pages-logout.html">
                    <span class="menu-icon"><i class="fa fa-sign-out"></i></span>          
                    <span class="menu-text">Salirme</span>             
                </a>
                
            </li>
        </ul>
    </div>     
</div>       
    <!-- Middle Content Start -->
    
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <div class="vd_head-section clearfix">
            <div class="vd_panel-header">
              <ul class="breadcrumb">
                <li><a href="dashboard.php">Home</a> </li>
                <li class="active">Perfiles</li>
              </ul>
              <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step=5  data-position="left">
    <div data-action="remove-navbar" data-original-title="Quitar Barra de navegación" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
      <div data-action="remove-header" data-original-title="Quitar Menú superior " data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
      <div data-action="fullscreen" data-original-title="Quitar Barra de navegación y Menú superior" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>
      
</div>
 
            </div>
          </div>
          <!-- vd_head-section -->
          
          <div class="vd_title-section clearfix">
            <div class="vd_panel-header">
              <div class="vd_panel-header">
                <h1>Dashboard</h1>
                <small class="subtitle">Bienvenido(a) al administrador de Universidad Lasalle.</small>

 <!-- vd_panel-menu -->
              </div>
              <h1>&nbsp;</h1>
            </div>
            <!-- vd_panel-header --> 
          </div>
          <!-- vd_title-section -->   
          
          <div class="vd_content-section clearfix">
            <div class="row">              
                        
            <div class="clearfix"></div>
            
            <div class="col-md-12">
             
         
        <div class="panel widget" style="margin: 0 auto;">
			 <div class="panel-heading vd_bg-grey">
			   <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-pencil-square-o"></i> </span> Catálogo de tabs </h3>
			 </div>
			<?php if(isset($_GET["listado"])){ ?>
                <div class="vd_panel-menu">
                      <div data-action="refresh" class="menu entypo-icon smaller-font" data-placement="bottom" data-toggle="tooltip" data-original-title="Actualizar"> <i class="icon-cycle"></i> </div>
					  
					  <a class="btn btn-success btn-xs" href="micrositios-modulos-tabs.php?item=<?php echo $_GET["item"]; ?>&listado=1">+ tab</a>
					  <a class="btn btn-success btn-xs" href="micrositios-modulos-tabs_slides.php?item=<?php echo $_GET["item"]; ?>&listado=1">+ slider</a>
					  <a class="btn btn-success btn-xs" href="micrositios-modulos-tabs_categorias.php?item=<?php echo $_GET["item"]; ?>&nuevo=1">+ categoria</a>
                </div>
             <div class="panel-body table-responsive">
               <table class="table" id="table-cashiers"> <!-- table-striped class for table for zebra striped style-->
                      <thead>
                        <tr>
						<th style="width: 25%;">Categoria</th>
                          <th style="width: 10%;"></th>
                        </tr>
                      </thead>
					<tbody id="tbody-cashiers">        
						<?php
						// Seleccionamos las facultades
						function cats(){
							$sql= "select * from micrositios_tabs_categorias where id_micrositio=".$_GET["item"]." order by id asc";
							$query=mysql_query($sql) or die(mysql_error());
							while ($resultados = mysql_fetch_assoc($query)){
								$id[] = $resultados["id"];
								$texto[] = $resultados["texto"];
							}mysql_free_result($query);
							$i=0; while($i < count(@$id)){
						?>
						<tr>
							
							<td><?php echo $texto[$i]; ?></td>
							<td>
								<a href="micrositios-modulos-tabs_categorias.php?id=<?php echo $id[$i]; ?>&item=<?php echo $_GET["item"]; ?>&editar=1" class="btn btn-success btn-xs"><i class="icon-pencil"></i></a>
								<a href="ajax.php?id=<?php echo $id[$i]; ?>&tabla=micrositios_tabs_categorias&eliminar=1" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></a>
							</td>
						</tr>
						<?php $i++; } } cats(); ?>
					</tbody>
               </table>
             </div>
			 <?php }else{
				if(isset($_GET["nuevo"])){ ?>
					<div class="panel-body table-responsive">
					   <div class="row">
							<form action="" method="post" enctype="multipart/form-data" onsubmit="validar();">
								<div class="col-md-12">
									<label for="nuevo-texto">Texto:</label>
									<input type="text" name="nuevo-texto" id="nuevo-texto">
								</div>
								<div class="col-md-6" style="display:none">
									<label for="nuevo-icono">Icono:</label>
									<input type="file" id="nuevo-icono" name="nuevo-icono">
								</div>
								<div class="col-md-6">
									<label for="nuevo-slide">Slide:</label>
									<select id="nuevo-slide" name="nuevo-slider">
										<option value="0">Selecciona</option>
											<?php function slides(){
												$sql= "select id from micrositios_tabs where id_micrositio=".$_GET["item"]." order by id asc";	
												$query=mysql_query($sql) or die(mysql_error());			
												while ($resultados = mysql_fetch_assoc($query)){
													$id_tabs[] = $resultados["id"];
												}mysql_free_result($query);
												$implode_tabs = implode(",",$id_tabs);
												if(count($id_tabs) == 0){$implode_tabs = 0;}
											
												$sql= "select id,titulo from micrositios_tabs_sliders where id_tab IN(".$implode_tabs.") order by id asc";	
												$query=mysql_query($sql) or die(mysql_error());			
													while ($resultados = mysql_fetch_assoc($query)){
														$id[] = $resultados["id"];
														$nombre[] = $resultados["titulo"];
													}mysql_free_result($query);
													$i=0; while($i < count(@$id)){ ?>
										<option value="<?php echo $id[$i]; ?>"><?php echo $nombre[$i]; ?></option>
										<?php $i++; } } slides(); ?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="nuevo-archivos">Archivos:</label>
									<input type="file" id="nuevo-archivos" name="nuevo-archivos[]" multiple>
								</div>
								<input type="hidden" name="nuevo-micrositio" id="nuevo-micrositio" value="<?php echo $_GET["item"]; ?>">
								<div class="col-md-12">
									<button type="submit" name="nuevatabcategoria" class="btn btn-success">GUARDAR</button>
								</div>
							</form>
						</div>
					</div>
				<?php }
				if(isset($_GET["editar"])){
					$sql= "select * from micrositios_tabs_categorias where id=".$_GET["id"]."";
					$query=mysql_query($sql) or die(mysql_error());
					while ($resultados = mysql_fetch_assoc($query)){
						$id = $resultados["id"];
						$texto = $resultados["texto"];
						$icono = $resultados["icono"];
						$id_slider = $resultados["id_slider"];
					}mysql_free_result($query);
				?>	<div class="panel-body table-responsive">
						<div class="row">
							<form action="" method="post" enctype="multipart/form-data" onsubmit="validar();">
								<input type="hidden" name="nuevo-micrositio" id="nuevo-micrositio" value="<?php echo $_GET["item"]; ?>">
								<div class="col-md-12">
									<label for="nuevo-texto">Texto:</label>
									<input type="text" id="nuevo-texto" name="nuevo-texto" value="<?php echo $texto; ?>">
								</div>
								<div class="col-md-6" style="display:none">
									<label for="nuevo-icono">Icono:</label>
									<input type="file" id="nuevo-icono" name="nuevo-icono">
									<img src="componentes/img/<?php echo $icono; ?>" style="display:block;width:90%">
								</div>
								<div class="col-md-6">
									<label for="nuevo-slide">Slide:</label>
									<select id="nuevo-slide" name="nuevo-slider">
											<?php function slides2($id_slider){
												$sql= "select id from micrositios_tabs where id_micrositio=".$_GET["item"]." order by id desc";	
												$query=mysql_query($sql) or die(mysql_error());			
												while ($resultados = mysql_fetch_assoc($query)){
													$id_tabs[] = $resultados["id"];
												}mysql_free_result($query);
												$implode_tabs = implode(",",$id_tabs);
												if(count($id_tabs) == 0){$implode_tabs = 0;}
											
												$sql= "select id,titulo from micrositios_tabs_sliders where id_tab IN(".$implode_tabs.") order by id=".$id_slider." desc";	
												$query=mysql_query($sql) or die(mysql_error());			
													while ($resultados = mysql_fetch_assoc($query)){
														$id[] = $resultados["id"];
														$nombre[] = $resultados["titulo"];
													}mysql_free_result($query);
													$i=0; while($i < count(@$id)){ ?>
										<option value="<?php echo $id[$i]; ?>"><?php echo $nombre[$i]; ?></option>
										<?php $i++; } } slides2($id_slider); ?>
									</select>
								</div>
								<div class="col-md-6">
									<label for="nuevo-archivos">Archivos:</label>
									<input type="file" id="nuevo-archivos" name="nuevo-archivos[]" multiple>
								</div>
								<input type="hidden" id="nuevo-id" name="nuevo-id" value="<?php echo $id; ?>">
								<div class="col-md-12">
									<button type="submit" name="editartabcategoria" class="btn btn-success">GUARDAR</button>
								</div>
							</form>
						</div>
					</div>
			<?php } } ?>
        </div>
            </div>     
            
              <div class="col-md-5">
                <div class="row">
                  <div class="col-md-12">
                    <div class="vd_status-widget vd_bg-green widget">
    <div class="vd_panel-menu">
  <div data-action="refresh" data-original-title="Actualizar" data-rel="tooltip" class=" menu entypo-icon smaller-font"> <i class="icon-cycle"></i> </div>
</div>
<!-- vd_panel-menu --> </div>                    
                  </div>
                  <!--col-md-12 --> 
                </div>
                <!-- .row -->
                <div class="row">
                  <div class="col-xs-6">
                    <div class="vd_status-widget vd_bg-red  widget">
    <div class="vd_panel-menu">
  <div data-action="refresh" data-original-title="Actualizar" data-rel="tooltip" class=" menu entypo-icon smaller-font"> <i class="icon-cycle"></i> </div>
</div>
<!-- vd_panel-menu -->                                  
                                                                 
</div>                    </div>
                  <!--col-xs-6 -->
                  <div class="col-xs-6">
                    <div class="vd_status-widget vd_bg-blue widget">
    <div class="vd_panel-menu">
  <div data-action="refresh" data-original-title="Actualizar" data-rel="tooltip" class=" menu entypo-icon smaller-font"> <i class="icon-cycle"></i> </div>
</div>
<!-- vd_panel-menu -->                             
                 </div>                   
                  </div>
                  <!--col-xs-6 --> 
                </div>
                <!-- .row -->
                <div class="row">
                  <div class="col-xs-6">
                    <div class="vd_status-widget vd_bg-yellow widget">
    <div class="vd_panel-menu">
  <div data-action="refresh" data-original-title="Actualizar" data-rel="tooltip" class=" menu entypo-icon smaller-font"> <i class="icon-cycle"></i> </div>
</div>
<!-- vd_panel-menu --> 
                                
                                                                
</div>                    
                  </div>
                  <!--col-xs-6 -->
                  <div class="col-xs-6">
                    <div class="vd_status-widget vd_bg-grey widget">
    <div class="vd_panel-menu">
  <div data-action="refresh" data-original-title="Actualizar" data-rel="tooltip" class=" menu entypo-icon smaller-font"> <i class="icon-cycle"></i> </div>
</div>
<!-- vd_panel-menu -->                                   
                                                                    
                    </div>                   
                  </div>
                  <!--col-md-xs-6 --> 
                </div>
                <!-- .row --> 
                
              </div>
              <!-- .col-md-5 --> 
            </div>
            <div class="row">
              <div class="col-md-12">                

<!-- Panel Widget -->
              </div>
              <!--col-md-5-->
              <div class="col-md-4">
                

<!-- vd_panel-menu --> 

<!-- Panel Widget -->              
              </div>
              <!--col-md-4--> 

              
            </div>
            <!-- row --> 
            
          </div>
          <!-- .vd_content-section --> 
          
        </div>
        <!-- .vd_content --> 
      </div>
      <!-- .vd_container --> 
    </div>
    <!-- .vd_content-wrapper --> 
    
    <!-- Middle Content End --> 
    
  </div>
  <!-- .container --> 
</div>
<!-- .content -->

<!-- Footer Start -->
  <footer class="footer-1"  id="footer">      
    <div class="vd_bottom ">
        <div class="container">
            <div class="row">
              <div class=" col-xs-12">
                <div class="copyright">
                    Copyright &copy;2016 Universidad Lasalle. All Rights Reserved 
                </div>
              </div>
            </div><!-- row -->
        </div><!-- container -->
    </div>
  </footer>
<!-- Footer END -->

<!-- .vd_body END  -->
<a id="back-top" href="#" data-action="backtop" class="vd_back-top visible"> <i class="fa  fa-angle-up"> </i> </a>

<!--
<a class="back-top" href="#" id="back-top"> <i class="icon-chevron-up icon-white"> </i> </a> -->

<!-- Javascript =============================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script type="text/javascript" src="js/jquery.js"></script> 
<!--[if lt IE 9]>
  <script type="text/javascript" src="js/excanvas.js"></script>      
<![endif]-->
<script type="text/javascript" src="js/bootstrap.min.js"></script> 
<script type="text/javascript" src='plugins/jquery-ui/jquery-ui.custom.min.js'></script>
<script type="text/javascript" src="plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

<script type="text/javascript" src="js/caroufredsel.js"></script> 
<script type="text/javascript" src="js/plugins.js"></script>

<script type="text/javascript" src="plugins/breakpoints/breakpoints.js"></script>
<script type="text/javascript" src="plugins/dataTables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="plugins/prettyPhoto-plugin/js/jquery.prettyPhoto.js"></script> 

<script type="text/javascript" src="plugins/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="plugins/tagsInput/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="plugins/bootstrap-switch/bootstrap-switch.min.js"></script>
<script type="text/javascript" src="plugins/blockUI/jquery.blockUI.js"></script>
<script type="text/javascript" src="plugins/pnotify/js/jquery.pnotify.min.js"></script>

<script type="text/javascript" src="js/theme.js"></script>
<script type="text/javascript" src="custom/custom.js"></script>
 
<!-- Specific Page Scripts Put Here -->
<!-- Flot Chart  -->
<script type="text/javascript" src="plugins/flot/jquery.flot.min.js"></script>
<script type="text/javascript" src="plugins/flot/jquery.flot.resize.min.js"></script>
<script type="text/javascript" src="plugins/flot/jquery.flot.pie.min.js"></script>
<script type="text/javascript" src="plugins/flot/jquery.flot.categories.min.js"></script>
<script type="text/javascript" src="plugins/flot/jquery.flot.time.min.js"></script>
<script type="text/javascript" src="plugins/flot/jquery.flot.animator.min.js"></script>

<!-- Vector Map -->
<script type="text/javascript" src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script type="text/javascript" src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>

<!-- Calendar -->
<script type="text/javascript" src='plugins/moment/moment.min.js'></script>
<script type="text/javascript" src='plugins/jquery-ui/jquery-ui.custom.min.js'></script>
<script type="text/javascript" src='plugins/fullcalendar/fullcalendar.min.js'></script>

<!-- Intro JS (Tour) -->
<script type="text/javascript" src='plugins/introjs/js/intro.min.js'></script>

<!-- Sky Icons -->
<script type="text/javascript" src='plugins/skycons/skycons.js'></script>

<!--Texto enriquezido-->
<script type="text/javascript" src="js/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="js/bootstrap-wysihtml5.js"></script>
<script src="js/summernote.min.js"></script>
<script type="text/javascript">
// Texto enriquezido
	$(document).ready(function(){
		$('.wysihtml5').wysihtml5();
		$('textarea').summernote({
			height: 200,                 // set editor height

			minHeight: null,             // set minimum height of editor
			maxHeight: null,             // set maximum height of editor

			focus: true                 // set focus to editable area after initializing summernote
		});

	});
function validar(){
	var datos = new FormData();   // FormData (Conjunto de datos) llamado "datos"
	var texto = $('#nuevo-texto').val();
	var slide = $('#nuevo-slide').val();
	
	if(texto == ""){alert("El texto no puede estar vacio");return false;}
	if(slide == 0){alert("Debe seleccionar un slide");return false;}
}

$(window).load(function () 
	{




		$.fn.UseTooltip = function () {
			var previousPoint = null;
			 
			$(this).bind("plothover", function (event, pos, item) {        
					if (item) {
						if (previousPoint != item.dataIndex) {
		
							previousPoint = item.dataIndex;
		
							$("#tooltip").remove();
							var x = item.datapoint[0].toFixed(2),
							y = item.datapoint[1].toFixed(2);
		
							showTooltip(item.pageX, item.pageY,
								"<p class='vd_bg-green'><strong class='mgr-10 mgl-10'>" + Math.round(x)  + " ENE 2015 </strong></p>" +
								"<div style='padding: 0 10px 10px;'>" +
								"<div>" + item.series.label +": <strong>"+ Math.round(y)  +"</strong></div>" +
//								"<div> Profit: <strong>$"+ Math.round(y)*7  +"</strong></div>" +
								"</div>"
							);
						}
					} else {
						$("#tooltip").remove();
						previousPoint = null;            
					}
			});
		};
		 
		function showTooltip(x, y, contents) {
			$('<div id="tooltip">' + contents + '</div>').css({
				position: 'absolute',
				display: 'none',
				top: y + 5,
				left: x + 20,    
				size: '10',  
//				'border-top' : '3px solid #1FAE66',
				'background-color': '#111111',
				color: "#FFFFFF",
				opacity: 0.85
			}).appendTo("body").fadeIn(200);
		}


/* REVENUE LINE CHART */

	var d2 = [ [1, 250],
            [2, 150],
            [3, 50],
            [4, 200],
            [5,50],
            [6, 150],
            [7, 150],
            [8, 200],
            [9, 100],
            [10, 250],
            [11,250],
            [12, 200],
            [13, 300]			

];
	var d1 = [
			[1, 650],
            [2, 550],
            [3, 450],
            [4, 550],
            [5, 350],
            [6, 500],
            [7, 600],
            [8, 450],
            [9, 300],
            [10, 600],
            [11, 400],
            [12, 500],
            [13, 700]					
			
];
	var plot = $.plotAnimator($("#revenue-line-chart"), [
			{  	label: "Usuarios",
				data: d2, 	
				lines: {				
					fill: 0.4,
					lineWidth: 0,				
				},
				color:['#f2be3e']
			},{ 
				data: d1, 
				animator: {steps: 60, duration: 1000, start:0}, 		
				lines: {lineWidth:2},	
				shadowSize:0,
				color: '#F85D2C'
			},{
				label: "Usuarios",
				data: d1, 
				points: { show: true, fill: true, radius:6,fillColor:"#F85D2C",lineWidth:3 },	
				color: '#fff',				
				shadowSize:0
			},
			{	label: "Marcas",
				data: d2, 
				points: { show: true, fill: true, radius:6,fillColor:"#f2be3e",lineWidth:3 },	
				color: '#fff',				
				shadowSize:0
			}
		],{	xaxis: {
		tickLength: 0,
		tickDecimals: 0,
		min:2,

				font :{
					lineHeight: 13,
					style: "normal",
					weight: "bold",
					family: "sans-serif",
					variant: "small-caps",
					color: "#6F7B8A"
				}
			},
			yaxis: {
				ticks: 3,
                tickDecimals: 0,
				tickColor: "#f0f0f0",
				font :{
					lineHeight: 13,
					style: "normal",
					weight: "bold",
					family: "sans-serif",
					variant: "small-caps",
					color: "#6F7B8A"
				}
			},
			grid: {
				backgroundColor: { colors: [ "#fff", "#fff" ] },
				borderWidth:1,borderColor:"#f0f0f0",
				margin:0,
				minBorderMargin:0,							
				labelMargin:20,
				hoverable: true,
				clickable: true,
				mouseActiveRadius:6
			},
			legend: { show: false}
		});

 		$("#revenue-line-chart").UseTooltip();		

		$(window).on("resize", function(){
			plot.resize();
			plot.setupGrid();
			plot.draw();
		});
				


		



/* FULL CALENDAR  */
		
		var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();
		
		$('#calendar').fullCalendar({
			header: {
				left:   'title',
				center: '',
				right:  'today prev,next'
			},
			editable: true,
			events: [
				{
					title: 'All Day Event',
					start: new Date(y, m, 1)
				},
				{
					title: 'Long Event',
					start: new Date(y, m, d-5),
					end: new Date(y, m, d-2)
				},
				{
					id: 999,
					title: 'Repeating Event',
					start: new Date(y, m, d-3, 16, 0),
					allDay: false
				},
				{
					id: 999,
					title: 'Repeating Event',
					start: new Date(y, m, d+4, 16, 0),
					allDay: false
				},
				{
					title: 'Meeting',
					start: new Date(y, m, d, 10, 30),
					allDay: false
				},
				{
					title: 'Lunch',
					start: new Date(y, m, d, 12, 0),
					end: new Date(y, m, d, 14, 0),
					allDay: false
				},
				{
					title: 'Birthday Party',
					start: new Date(y, m, d+1, 19, 0),
					end: new Date(y, m, d+1, 22, 30),
					allDay: false
				},
				{
					title: 'Click for Google',
					start: new Date(y, m, 28),
					end: new Date(y, m, 29),
					url: 'http://google.com/'
				}
			]
		});
		

// Skycons

      var icons = new Skycons({"color": "white","resizeClear": true}),
	  	  icons_btm = new Skycons({"color": "#F89C2C","resizeClear": true}),
          list  = "clear-day",
		  livd_btm = ["rain", "wind"
		  ];
		  icons.set(list,list)
      for(var i = livd_btm.length; i--; )
        icons_btm.set(livd_btm[i], livd_btm[i]);

      icons.play();
	  icons_btm.play();

/* News Widget */
	   $(".vd_news-widget .vd_carousel").carouFredSel({
			prev:{
				button : function()
				{
					return $(this).parent().parent().children('.vd_carousel-control').children('a:first-child')
				}
			},
			next:{
				button : function()
				{
					return $(this).parent().parent().children('.vd_carousel-control').children('a:last-child')
				}
			},		
			scroll: {
				fx: "crossfade",
				onBefore: function(){
						var target = "#front-1-clients";
						$(target).css("transition","all .5s ease-in-out 0s");				
					if ($(target).hasClass("vd_bg-soft-yellow")){						
						$(target).removeClass("vd_bg-soft-yellow");
						$(target).addClass("vd_bg-soft-red");		
					} else
					if ($(target).hasClass("vd_bg-soft-red")){						
						$(target).removeClass("vd_bg-soft-red");
						$(target).addClass("vd_bg-soft-blue");		
					} else
					if ($(target).hasClass("vd_bg-soft-blue")){						
						$(target).removeClass("vd_bg-soft-blue");
						$(target).addClass("vd_bg-soft-green");		
					} else
					if ($(target).hasClass("vd_bg-soft-green")){						
						$(target).removeClass("vd_bg-soft-green");
						$(target).addClass("vd_bg-soft-yellow");		
					} 					
				}
			},
			width: "auto",
			height: "responsive",
			responsive: true,
			auto:3000
			
		});



});
</script>
<!-- Specific Page Scripts END -->




<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information. -->

<script>
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-XXXXX-X']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script> 

</body>
</html>
<?php
// Unset para todas las variables declaradas
$variables = array_keys(get_defined_vars());
for ($i = 0; $i < count($variables); $i++) {
    unset($variables[$i]);
}
unset($variables,$i);

// MICROSITIOS
// nuevo proyecto
if(isset($_POST["nuevatabcategoria"])){ // 
	
	$sql="INSERT INTO micrositios_tabs_categorias SET texto='".$_POST["nuevo-texto"]."',id_slider='".$_POST["nuevo-slider"]."',id_micrositio='".$_POST["nuevo-micrositio"]."'";
	mysql_query($sql) or die(mysql_error());
	
	$query = mysql_query("SELECT MAX(id) FROM micrositios_tabs_categorias");
	$results = mysql_fetch_array($query); mysql_free_result($query);
	$id = $results['MAX(id)'];
	
	if(isset($_FILES["nuevo-icono"])){
		// Insertamos la foto
			$ruta = '../lasalle/componentes/img/';
			foreach($_FILES['nuevo-icono']['tmp_name'] as $key => $tmp_name )
			{
				
					$NombreOriginal = $key.$_FILES['nuevo-icono']['name'][$key];//Obtenemos el nombre original del archivo
					$tamano = $_FILES['nuevo-icono']['size'][$key];//Obtenemos el tamaño del archivo
					$tipo = $_FILES['nuevo-icono']['type'][$key];//Obtenemos el tipo del archivo
					$temporal = $_FILES['nuevo-icono']['tmp_name'][$key]; //Obtenemos la ruta Original del archivo
					$Destino = $ruta.$NombreOriginal;	//Creamos una ruta de destino con la variable ruta y el nombre original del archivo	
					move_uploaded_file($temporal, $Destino); //Movemos el archivo temporal a la ruta especificada					
					if($tipo != NULL){
						//*** Insert Record ***//
						$strSQL = "UPDATE micrositios_tabs_categorias SET icono='".$NombreOriginal."' where id=".$id."";
						$objQuery = mysql_query($strSQL);
					}
			}
	}
	if(isset($_FILES["nuevo-archivos"])){
		// Insertamos la foto
			$ruta = '../lasalle/componentes/archivos/';
			foreach($_FILES['nuevo-archivos']['tmp_name'] as $key => $tmp_name )
			{
				
					$NombreOriginal = $key.$_FILES['nuevo-archivos']['name'][$key];//Obtenemos el nombre original del archivo
					$tamano = $_FILES['nuevo-archivos']['size'][$key];//Obtenemos el tamaño del archivo
					$tipo = $_FILES['nuevo-archivos']['type'][$key];//Obtenemos el tipo del archivo
					$temporal = $_FILES['nuevo-archivos']['tmp_name'][$key]; //Obtenemos la ruta Original del archivo
					$Destino = $ruta.$NombreOriginal;	//Creamos una ruta de destino con la variable ruta y el nombre original del archivo	
					move_uploaded_file($temporal, $Destino); //Movemos el archivo temporal a la ruta especificada					
					if($tipo != NULL){
						$extension = explode(".",$NombreOriginal);
						$contador_extension = count(@$extension);
						$tipo_file = "archivo";
						if($extension[$contador_extension - 1] == 'mp4'){$tipo_file = "video";}
						//*** Insert Record ***//
						$strSQL = "INSERT INTO micrositios_tabs_categorias_archivos SET tipo='".$tipo_file."',id_categoria=".$id.",archivo='".$NombreOriginal."'";
						$objQuery = mysql_query($strSQL);
					}
			}
	}
	header("Location: micrositios-modulos-tabs_categorias.php?item=".$_GET["item"]."&listado=1");
}
if(isset($_POST["editartabcategoria"])){ // 
	
	$sql="UPDATE micrositios_tabs_categorias SET texto='".$_POST["nuevo-texto"]."',id_slider='".$_POST["nuevo-slider"]."',id_micrositio='".$_POST["nuevo-micrositio"]."' where id='".$_POST["nuevo-id"]."'";
	mysql_query($sql) or die(mysql_error());
	
	$id = $_POST['nuevo-id'];
	
	if(isset($_FILES["nuevo-icono"])){
		// Insertamos la foto
			$ruta = '../lasalle/componentes/img/';
			foreach($_FILES['nuevo-icono']['tmp_name'] as $key => $tmp_name )
			{
				
					$NombreOriginal = $key.$_FILES['nuevo-icono']['name'][$key];//Obtenemos el nombre original del archivo
					$tamano = $_FILES['nuevo-icono']['size'][$key];//Obtenemos el tamaño del archivo
					$tipo = $_FILES['nuevo-icono']['type'][$key];//Obtenemos el tipo del archivo
					$temporal = $_FILES['nuevo-icono']['tmp_name'][$key]; //Obtenemos la ruta Original del archivo
					$Destino = $ruta.$NombreOriginal;	//Creamos una ruta de destino con la variable ruta y el nombre original del archivo	
					move_uploaded_file($temporal, $Destino); //Movemos el archivo temporal a la ruta especificada					
					if($tipo != NULL){
						//*** Insert Record ***//
						$strSQL = "UPDATE micrositios_tabs_categorias SET icono='".$NombreOriginal."' where id=".$id."";
						$objQuery = mysql_query($strSQL);
					}
			}
	}
	if(isset($_FILES["nuevo-archivos"])){
		// Insertamos la foto
			$ruta = '../lasalle/componentes/archivos/';
			foreach($_FILES['nuevo-archivos']['tmp_name'] as $key => $tmp_name )
			{
				
					$NombreOriginal = $key.$_FILES['nuevo-archivos']['name'][$key];//Obtenemos el nombre original del archivo
					$tamano = $_FILES['nuevo-archivos']['size'][$key];//Obtenemos el tamaño del archivo
					$tipo = $_FILES['nuevo-archivos']['type'][$key];//Obtenemos el tipo del archivo
					$temporal = $_FILES['nuevo-archivos']['tmp_name'][$key]; //Obtenemos la ruta Original del archivo
					$Destino = $ruta.$NombreOriginal;	//Creamos una ruta de destino con la variable ruta y el nombre original del archivo	
					move_uploaded_file($temporal, $Destino); //Movemos el archivo temporal a la ruta especificada					
					if($tipo != NULL){
						$extension = explode(".",$NombreOriginal);
						$contador_extension = count(@$extension);
						$tipo_file = "archivo";
						if($extension[$contador_extension - 1] == 'mp4'){$tipo_file = "video";}
						//*** Insert Record ***//
						$strSQL = "INSERT INTO micrositios_tabs_categorias_archivos SET tipo='".$tipo_file."',id_categoria=".$id.",archivo='".$NombreOriginal."'";
						$objQuery = mysql_query($strSQL);
					}
			}
	}
	header("Location: micrositios-modulos-tabs_categorias.php?item=".$_GET["item"]."&listado=1");
}
//echo memory_get_usage();
		mysql_close($conx);
	}	// Cierre del ELSE
?>