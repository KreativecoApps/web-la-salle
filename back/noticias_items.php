<?php
ob_start("ob_gzhandler");
define('TIMEZONE', 'America/Mexico_City');
date_default_timezone_set(TIMEZONE);
setlocale(LC_ALL,"es_ES");
// Conexion a BD
	require_once("conexion.php"); // conexion a BD
	session_start(); // iniciar variables de sesion
	if(isset($_GET["cerrar"])){	// Si cerro sesion el usuario, ser&Aacute; redireccionado al login
		session_destroy();	// Destruir variables de seson
		header("Location:index.php");	// redireccion al login
	}
	if(@$_SESSION["iniciada"]==NULL){	// Si el usuario no ha iniciado sesion sera redireciconado al login
		header("Location:index.php");	// redireccion al login
	}else{								// Si el usuario ya inicio sesion ser&Aacute; redireccionado al index.php
?>
<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html><!--<![endif]-->

<!-- Specific Page Data -->

<!-- End of Data -->

<head>
    <meta charset="utf-8" />
    <title>Universidad Lasalle | Administrador Maestro</title>
    <meta name="keywords" content="CMS Maestro, Universidad Lasalle" />
    <meta name="description" content="CMS Maestro, Universidad Lasalle">
    <meta name="author" content="Universidad Lasalle">
    
    <!-- Set the viewport width to device width for mobile -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">    
    
    
    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="img/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="img/ico/favicon.png">
    
    
    <!-- CSS -->
       
    <!-- Bootstrap & FontAwesome & Entypo CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if IE 7]><link type="text/css" rel="stylesheet" href="css/font-awesome-ie7.min.css"><![endif]-->
    <link href="css/font-entypo.css" rel="stylesheet" type="text/css">    

    <!-- Fonts CSS -->
    <link href="css/fonts.css"  rel="stylesheet" type="text/css">
               
    <!-- Plugin CSS -->
    <link href="plugins/jquery-ui/jquery-ui.custom.min.css" rel="stylesheet" type="text/css">    
    <link href="plugins/prettyPhoto-plugin/css/prettyPhoto.css" rel="stylesheet" type="text/css">
    <link href="plugins/isotope/css/isotope.css" rel="stylesheet" type="text/css">
    <link href="plugins/pnotify/css/jquery.pnotify.css" media="screen" rel="stylesheet" type="text/css">    
	<link href="plugins/google-code-prettify/prettify.css" rel="stylesheet" type="text/css"> 
   
         
    <link href="plugins/mCustomScrollbar/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css">
    <link href="plugins/tagsInput/jquery.tagsinput.css" rel="stylesheet" type="text/css">
    <link href="plugins/bootstrap-switch/bootstrap-switch.css" rel="stylesheet" type="text/css">    
    <link href="plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css">    
    <link href="plugins/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css">
    <link href="plugins/colorpicker/css/colorpicker.css" rel="stylesheet" type="text/css">            

	<!-- Specific CSS -->
	<link href="plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"><link href="plugins/fullcalendar/fullcalendar.print.css" rel="stylesheet" type="text/css"><link href="plugins/introjs/css/introjs.min.css" rel="stylesheet" type="text/css">    
     
    <!-- Theme CSS -->
    <link href="css/theme.min.css" rel="stylesheet" type="text/css">
    <!--[if IE]> <link href="css/ie.css" rel="stylesheet" > <![endif]-->
    <link href="css/chrome.css" rel="stylesheet" type="text/chrome"> <!-- chrome only css -->    

	<!-- Texto enriquezido -->
	<link rel="stylesheet" type="text/css" href="css/bootstrap-wysihtml5.css">
    <link href="css/summernote.css" rel="stylesheet">
       
    <!-- Responsive CSS -->
    <link href="css/theme-responsive.min.css" rel="stylesheet" type="text/css"> 

	  
 
 
    <!-- for specific page in style css -->
        
    <!-- for specific page responsive in style css -->
        
    
    <!-- Custom CSS -->
    <link href="custom/custom.css" rel="stylesheet" type="text/css">



    <!-- Head SCRIPTS -->
    <script type="text/javascript" src="js/modernizr.js"></script> 
    <script type="text/javascript" src="js/mobile-detect.min.js"></script> 
    <script type="text/javascript" src="js/mobile-detect-modernizr.js"></script> 
 
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script type="text/javascript" src="js/html5shiv.js"></script>
      <script type="text/javascript" src="js/respond.min.js"></script>     
    <![endif]-->
    
</head>    

<body id="dashboard" class="full-layout nav-left-medium nav-right-hide nav-right-start-hide  nav-top-fixed      responsive    clearfix" data-active="dashboard "  data-smooth-scrolling="1">     
<div class="vd_body">
<!-- Header Start -->
  <header class="header-1" id="header">
      <div class="vd_top-menu-wrapper">
        <div class="container ">
          <div class="vd_top-nav vd_nav-width  ">
          <div class="vd_panel-header">
          	<div class="logo">
            	<a href="index.html"><img alt="logo" src="img/logo.png"></a>
            </div>
            <!-- logo
            <div class="vd_panel-menu  hidden-sm hidden-xs" data-intro="<strong>Mininzar menú izquierda</strong><br/>Puede ampliar o reducir el menu." data-step=1>
            		                	<span class="nav-medium-button menu" data-toggle="tooltip" data-placement="bottom" data-original-title="Menú Bloques" data-action="nav-left-medium">
	                    <i class="fa fa-bars"></i>
                    </span>
                                       
            </div>
            <div class="vd_panel-menu left-pos visible-sm visible-xs">
                                 
                        <span class="menu" data-action="toggle-navbar-left">
                            <i class="fa fa-ellipsis-v"></i>
                        </span>  
                            
                              
            </div>
            -->
            <div class="vd_panel-menu visible-sm visible-xs">
                	<span class="menu visible-xs" data-action="submenu">
	                    <i class="fa fa-bars"></i>
                    </span>        
                          
                    <!--<span class="menu visible-sm visible-xs" data-action="toggle-navbar-right">
                            <i class="fa fa-comments"></i>
                        </span> -->                  
                   	 
                </div>
          

            <!-- vd_panel-menu -->
          </div>
          <!-- vd_panel-header -->
            
          </div>    
          <div class="vd_container">
          	<div class="row">
            	<div class="col-sm-5 col-xs-12">
                </div>
                <div class="col-sm-7 col-xs-12">
              		<div class="vd_mega-menu-wrapper">
                    	<div class="vd_mega-menu pull-right">
            				<ul class="mega-ul">
    <li id="top-menu-1" class="one-icon mega-li"> 


    </li>
      </a>
      
     <!-- perfil administrador maestro --> 
    <li id="top-menu-profile" class="profile mega-li"> 
        <a href="#" class="mega-link"  data-action="click-trigger"> 
            <span  class="mega-image">
                <img src="img/avatar/avatar0.jpg" alt="example image" />               
            </span>
            <span class="mega-name">
                Universidad Lasalle / Admin Master <i class="fa fa-caret-down fa-fw"></i> 
            </span>
        </a> 
      <div class="vd_mega-menu-content  width-xs-3  left-xs left-sm" data-action="click-target">
        <div class="child-menu"> 
        	<div class="content-list content-menu">
                <ul class="list-wrapper pd-lr-10">
                    <li> <a href="#"> <div class="menu-icon"><i class=" fa fa-user"></i></div> <div class="menu-text">Perfil Administrador</div> </a> </li>
                    <li> <a href="lockscreen.html"> <div class="menu-icon"><i class="  fa fa-key"></i></div> <div class="menu-text">Bloquear Cuenta</div> </a> </li>
                    <li class="line"></li>                
                    <li> <a href="logout.html"> <div class="menu-icon"><i class=" fa fa-sign-out"></i></div> <div class="menu-text">Salirme</div> </a> </li> 
                </ul>
            </div> 
        </div> 
      </div>     
  
    </li>               
       

	</ul>
<!-- Head menu search form ends -->                         
                        </div>
                    </div>
                </div>

            </div>
          </div>
        </div>
        <!-- container --> 
      </div>
      <!-- vd_primary-menu-wrapper --> 

  </header>
  <!-- Header Ends --> 
<div class="content">
  <div class="container">
    <div class="vd_navbar vd_nav-width vd_navbar-tabs-menu vd_navbar-left ">
	<div class="navbar-tabs-menu clearfix">
			<span class="expand-menu" data-action="expand-navbar-tabs-menu">
            	<span class="menu-icon menu-icon-left">
            		<i class="fa fa-ellipsis-h"></i>
                 
                </span>
            	<span class="menu-icon menu-icon-right">
            		<i class="fa fa-ellipsis-h"></i>
                    <span class="badge vd_bg-red">
                        20
                    </span>                    
                </span>                
            </span>
    </div>
	<div class="navbar-menu clearfix">
        <div class="vd_menu">
			<!-- Head menu search form ends -->  
			<?php include "sidebar.php"; ?>
		</div>             
    </div>
    <div class="navbar-spacing clearfix">
    </div>
    <div class="vd_menu vd_navbar-bottom-widget">
        <ul>
            <li>
                <a href="pages-logout.html">
                    <span class="menu-icon"><i class="fa fa-sign-out"></i></span>          
                    <span class="menu-text">Salirme</span>             
                </a>
                
            </li>
        </ul>
    </div>     
</div>       
    <!-- Middle Content Start -->
    
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <div class="vd_head-section clearfix">
            <div class="vd_panel-header">
              <ul class="breadcrumb">
                <li><a href="index.html">Home</a> </li>
                <li class="active">Dashboard</li>
              </ul>
              <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step=5  data-position="left">
    <div data-action="remove-navbar" data-original-title="Quitar Barra de navegación" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
      <div data-action="remove-header" data-original-title="Quitar Menú superior " data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
      <div data-action="fullscreen" data-original-title="Quitar Barra de navegación y Menú superior" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>
      
</div>
 
            </div>
          </div>
          <!-- vd_head-section -->
          
          <div class="vd_title-section clearfix">
            <div class="vd_panel-header">
              <div class="vd_panel-header">
                <h1>Dashboard</h1>
                <small class="subtitle">Bienvenido(a) al administrador de Universidad Lasalle.</small>

 <!-- vd_panel-menu -->
              </div>
              <h1>&nbsp;</h1>
            </div>
            <!-- vd_panel-header --> 
          </div>
          <!-- vd_title-section -->   
          
          <div class="vd_content-section clearfix">
            <div class="row">              
                        
            <div class="clearfix"></div>
            
            <div class="col-md-12">
				<div class="panel widget" style="margin: 0 auto;">
				<?php if(isset($_GET["nuevanoticia"])){ ?>
					<div class="panel-heading vd_bg-grey">
					   <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-pencil-square-o"></i> </span> Nueva noticia </h3>
					</div>
					<div class="panel-body table-responsive">
						<div class="row">
								<div class="col-md-6">
									<label for="nuevo-titulo">Título</label>
									<input type="text" id="nuevo-titulo">
								</div>
								<div class="col-md-3">
									<label for="nuevo-titulo">Categoría</label>
									<select id="nuevo-categoria">
										<?php
										// Checamos si la categoria que esta mostrando esta en calendario
											$query = mysql_query("SELECT es_calendario FROM noticias_categorias where id=".$_GET["categoria"]."");
											$results = mysql_fetch_array($query); mysql_free_result($query);
											$es_calendario = $results['es_calendario'];
											
											$campo_subtitulo_lugar = NULL;
											$campo_parrafo_descripcion = NULL;
											
											if($es_calendario == 1){
												$campo_subtitulo_lugar = 'Lugar';
												$campo_parrafo_descripcion = 'Descripci&oacute;n';
											}else{
												$campo_subtitulo_lugar = 'Subt&iacute;tulo';
												$campo_parrafo_descripcion = 'P&aacute;rrafo';
											}
										// categorias
											$sql= "select * from noticias_categorias where id=".$_GET["categoria"]." order by id=".$_GET["categoria"]." asc";
											$query=mysql_query($sql) or die(mysql_error());
											while ($resultados = mysql_fetch_assoc($query)){
												$id_cat[] = $resultados["id"];
												$nombre_cat[] = $resultados["nombre"];
											}mysql_free_result($query);
											$i=0; while($i < count(@$id_cat)){ ?>
												<option value="<?php echo $id_cat[$i]; ?>"><?php echo $nombre_cat[$i]; ?></option>
											<?php $i++; } ?>
									</select>
								</div>
								<div class="col-md-3">
									<label for="nuevo-autor">Autor</label>
									<select id="nuevo-autor">
										<?php
											$sql= "select * from noticias_autores";
											$query=mysql_query($sql) or die(mysql_error());
											while ($resultados = mysql_fetch_assoc($query)){
												$id_autor[] = $resultados["id"];
												$nombre_autor[] = $resultados["nombre"];
											}mysql_free_result($query);
											$i=0; while($i < count(@$id_autor)){ ?>
												<option value="<?php echo $id_autor[$i]; ?>"><?php echo $nombre_autor[$i]; ?></option>
											<?php $i++; } ?>
									</select>
								</div>
								<div class="col-md-12">
									<label for="nuevo-subtitulo"><?php echo $campo_subtitulo_lugar; ?></label>
									<input type="text" id="nuevo-subtitulo">
								</div>
								<div class="col-md-6">
									<label for="nuevo-fecha">Fecha y hora</label>
									<input type="datetime-local" id="nuevo-fecha">
								</div>
								<div class="col-md-6">
									<label for="nuevo-tags">Tags (Presione Ctrl o shift para seleccionar varios)</label>
									<select id="nuevo-tags" multiple='multiple'>
										<?php
										// tags
											$sql= "select * from tags";
											$query=mysql_query($sql) or die(mysql_error());
											while ($resultados = mysql_fetch_assoc($query)){
												$id_tag[] = $resultados["id"];
												$nombre_tag[] = $resultados["tag"];
											}mysql_free_result($query);
											$i=0; while($i < count(@$id_tag)){ ?>
												<option value="<?php echo $id_tag[$i]; ?>"><?php echo $nombre_tag[$i]; ?></option>
											<?php $i++; } ?>
									</select>
								</div>								
								<div class="col-md-12">
									<label for="nuevo-parrafo"><?php echo $campo_parrafo_descripcion; ?></label>
									<textarea id="nuevo-parrafo"></textarea>
								</div>
								<div class="col-md-6">
									<label for="nuevo-imagen">Imágen</label>
									<input type="file" id="nuevo-imagen" accept="image/*">
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<button id="nuevo-guardar" class="btn btn-success">Guardar</button>
								</div>
							</div>
						
					</div>
				<?php } if(isset($_GET["edicion"])){
					// Hacemo consulta
					$sql= "select * from noticias where id=".$_GET["id"]."";
					$query=mysql_query($sql) or die(mysql_error());
					while ($resultados = mysql_fetch_assoc($query)){
						$imagen = $resultados["imagen"];
						$titulo = $resultados["titulo"];
						$parrafo1 = $resultados["parrafo_1"];
						$subtitulo = $resultados["subtitulo"];
						$fecha = $resultados["fecha_creacion"];
						$tags = $resultados["tags"];
						$autor = $resultados["id_autor"];
					}mysql_free_result($query);
					$formato_fecha_explode = explode(" ",$fecha);
					$formato_fecha = $formato_fecha_explode[0]."T".$formato_fecha_explode[1];
				?>
					<div class="panel-heading vd_bg-grey">
					   <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-pencil-square-o"></i> </span> Editar noticia </h3>
					</div>
					<div class="panel-body table-responsive">
						<div class="row">
							<div class="col-md-6">
								<label for="editar-titulo">Título</label>
								<input type="text" id="editar-titulo" value="<?php echo $titulo; ?>">
							</div>
							<div class="col-md-3">
									<label for="editar-categoria">Categoría</label>
									<select id="editar-categoria">
										<?php
										// Checamos si la categoria que esta mostrando esta en calendario
											$query = mysql_query("SELECT es_calendario FROM noticias_categorias where id=".$_GET["categoria"]."");
											$results = mysql_fetch_array($query); mysql_free_result($query);
											$es_calendario = $results['es_calendario'];
											
											$campo_subtitulo_lugar = NULL;
											$campo_parrafo_descripcion = NULL;
											
											if(isset($_GET["calendar"])){
												$campo_subtitulo_lugar = 'Lugar';
												$campo_parrafo_descripcion = 'Descripci&oacute;n';
											}else{
												$campo_subtitulo_lugar = 'Subt&iacute;tulo';
												$campo_parrafo_descripcion = 'P&aacute;rrafo';
											}
										
										// categorias
											$sql= "select * from noticias_categorias where id=".$_GET["categoria"]." order by id=".$_GET["categoria"]." asc";
											$query=mysql_query($sql) or die(mysql_error());
											while ($resultados = mysql_fetch_assoc($query)){
												$id_cat[] = $resultados["id"];
												$nombre_cat[] = $resultados["nombre"];
											}mysql_free_result($query);
											$i=0; while($i < count(@$id_cat)){ ?>
												<option value="<?php echo $id_cat[$i]; ?>"><?php echo $nombre_cat[$i]; ?></option>
											<?php $i++; } ?>
									</select>
								</div>
								<div class="col-md-3">
									<label for="editar-autor">Autor</label>
									<select id="editar-autor">
										<?php
										if($autor == NULL){$autor = 1;}
											$sql= "select * from noticias_autores order by id=".$autor." asc";
											$query=mysql_query($sql) or die(mysql_error());
											while ($resultados = mysql_fetch_assoc($query)){
												$id_autor[] = $resultados["id"];
												$nombre_autor[] = $resultados["nombre"];
											}mysql_free_result($query);
											$i=0; while($i < count(@$id_autor)){ ?>
												<option value="<?php echo $id_autor[$i]; ?>"><?php echo $nombre_autor[$i]; ?></option>
											<?php $i++; } ?>
									</select>
								</div>
							<div class="col-md-12">
								<label for="editar-subtitulo"><?php echo $campo_subtitulo_lugar; ?></label>
								<input type="text" id="editar-subtitulo" value="<?php echo $subtitulo; ?>">
							</div>
							<div class="col-md-6">
								<label for="editar-fecha">Fecha y hora</label>
								<input type="datetime-local" id="editar-fecha" value="<?php echo $formato_fecha; ?>">
							</div>
							<div class="col-md-6">
								<label for="editar-tags">Tags (Presione Ctrl o shift para seleccionar varios)</label>
								<select id="editar-tags" multiple='multiple'>
									<?php
									// tags
										$tags_array = explode(",",$tags);
										$sql= "select * from tags";
										$query=mysql_query($sql) or die(mysql_error());
										while ($resultados = mysql_fetch_assoc($query)){
											$id_tag[] = $resultados["id"];
											$nombre_tag[] = $resultados["tag"];
										}mysql_free_result($query);
										$i=0; while($i < count(@$id_tag)){ ?>
											<option value="<?php echo $id_tag[$i]; ?>" <?php if(in_array($id_tag[$i],$tags_array)){ echo 'selected'; } ?>><?php echo $nombre_tag[$i]; ?></option>
										<?php $i++; } ?>
								</select>
							</div>
							<div class="col-md-12">
								<label for="editar-parrafo"><?php echo $campo_parrafo_descripcion; ?></label>
								<textarea id="editar-parrafo"><?php echo $parrafo1; ?></textarea>
							</div>
							<div class="col-md-6">
								<img src="../lasalle/noticias/img/<?php echo $imagen; ?>" class="imagen-noticia">
								<label for="editar-imagen">Imágen</label>
								<input type="file" id="editar-imagen" accept="image/*">
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<button id="editar-guardar" class="btn btn-success">Guardar</button>
							</div>
						</div>
					</div>	
					
					<div class="panel-heading vd_bg-grey">
					   <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-pencil-square-o"></i> </span> Comentarios </h3>
					</div>
					<div class="panel-body table-responsive">
						<?php
							// COMENTARIOS
							function comentarios(){
							$sql= "select * from comentarios where id_noticia=".$_GET["id"]." order by id desc";
							$query=mysql_query($sql) or die(mysql_error());
							while ($resultados = mysql_fetch_assoc($query)){
								$id_comentario[] = $resultados["id"];
								$comentario[] = $resultados["comentario"];
								$autor[] = $resultados["autor"];
								$email[] = $resultados["email"];
								$fecha= $resultados["fecha"];
							}mysql_free_result($query);
							if(count($id_comentario) > 0){
							$i=0; while($i < count(@$id_comentario)){ ?>
						<div class="row row-comentario-<?php echo $id_comentario[$i]; ?>" style="background: #d2d2d2;padding: 18px;">
							<div class="col-md-6">
								<label for="nuevo-titulo">Autor:</label>
								<div><?php echo $autor[$i]; ?></div>
							</div>
							<div class="col-md-6">
								<label for="nuevo-titulo">Email:</label>
								<div><?php echo $email[$i]; ?></div>
							</div>
							<div class="col-md-12">
								<label for="comentario">Comentario:</label>
								<div style="word-break: break-all;"><?php echo $comentario[$i]; ?></div>
							</div>
							<div class="col-md-12">
								<button class="btn btn-danger" onclick="EliminarComentario(<?php echo $id_comentario[$i]; ?>);" style="margin-top:7px">ELIMINAR</button>
							</div>
						</div>
						<?php $i++; } } } comentarios();  ?>
					</div>
				<?php } ?>
				</div>
            </div>     
            
              <div class="col-md-5">
                <div class="row">
                  <div class="col-md-12">
                    <div class="vd_status-widget vd_bg-green widget">
    <div class="vd_panel-menu">
  <div data-action="refresh" data-original-title="Actualizar" data-rel="tooltip" class=" menu entypo-icon smaller-font"> <i class="icon-cycle"></i> </div>
</div>
<!-- vd_panel-menu --> </div>                    
                  </div>
                  <!--col-md-12 --> 
                </div>
                <!-- .row -->
                <div class="row">
                  <div class="col-xs-6">
                    <div class="vd_status-widget vd_bg-red  widget">
    <div class="vd_panel-menu">
  <div data-action="refresh" data-original-title="Actualizar" data-rel="tooltip" class=" menu entypo-icon smaller-font"> <i class="icon-cycle"></i> </div>
</div>
<!-- vd_panel-menu -->                                  
                                                                 
</div>                    </div>
                  <!--col-xs-6 -->
                  <div class="col-xs-6">
                    <div class="vd_status-widget vd_bg-blue widget">
    <div class="vd_panel-menu">
  <div data-action="refresh" data-original-title="Actualizar" data-rel="tooltip" class=" menu entypo-icon smaller-font"> <i class="icon-cycle"></i> </div>
</div>
<!-- vd_panel-menu -->                             
                 </div>                   
                  </div>
                  <!--col-xs-6 --> 
                </div>
                <!-- .row -->
                <div class="row">
                  <div class="col-xs-6">
                    <div class="vd_status-widget vd_bg-yellow widget">
    <div class="vd_panel-menu">
  <div data-action="refresh" data-original-title="Actualizar" data-rel="tooltip" class=" menu entypo-icon smaller-font"> <i class="icon-cycle"></i> </div>
</div>
<!-- vd_panel-menu --> 
                                
                                                                
</div>                    
                  </div>
                  <!--col-xs-6 -->
                  <div class="col-xs-6">
                    <div class="vd_status-widget vd_bg-grey widget">
    <div class="vd_panel-menu">
  <div data-action="refresh" data-original-title="Actualizar" data-rel="tooltip" class=" menu entypo-icon smaller-font"> <i class="icon-cycle"></i> </div>
</div>
<!-- vd_panel-menu -->                                   
                                                                    
                    </div>                   
                  </div>
                  <!--col-md-xs-6 --> 
                </div>
                <!-- .row --> 
                
              </div>
              <!-- .col-md-5 --> 
            </div>
            <div class="row">
              <div class="col-md-12">                

<!-- Panel Widget -->
              </div>
              <!--col-md-5-->
              <div class="col-md-4">
                

<!-- vd_panel-menu --> 

<!-- Panel Widget -->              
              </div>
              <!--col-md-4--> 

              
            </div>
            <!-- row --> 
            
          </div>
          <!-- .vd_content-section --> 
          
        </div>
        <!-- .vd_content --> 
      </div>
      <!-- .vd_container --> 
    </div>
    <!-- .vd_content-wrapper --> 
    
    <!-- Middle Content End --> 
    
  </div>
  <!-- .container --> 
</div>
<!-- .content -->

<!-- Footer Start -->
  <footer class="footer-1"  id="footer">      
    <div class="vd_bottom ">
        <div class="container">
            <div class="row">
              <div class=" col-xs-12">
                <div class="copyright">
                    Copyright &copy;2016 Universidad Lasalle. All Rights Reserved 
                </div>
              </div>
            </div><!-- row -->
        </div><!-- container -->
    </div>
  </footer>
<!-- Footer END -->

<!-- .vd_body END  -->
<a id="back-top" href="#" data-action="backtop" class="vd_back-top visible"> <i class="fa  fa-angle-up"> </i> </a>

<div id="scripts"></div>
<!--
<a class="back-top" href="#" id="back-top"> <i class="icon-chevron-up icon-white"> </i> </a> -->

<!-- Javascript =============================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script type="text/javascript" src="js/jquery.js"></script> 
<!--[if lt IE 9]>
  <script type="text/javascript" src="js/excanvas.js"></script>      
<![endif]-->
<script type="text/javascript" src="js/bootstrap.min.js"></script> 
<script type="text/javascript" src='plugins/jquery-ui/jquery-ui.custom.min.js'></script>
<script type="text/javascript" src="plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

<script type="text/javascript" src="js/caroufredsel.js"></script> 
<script type="text/javascript" src="js/plugins.js"></script>

<script type="text/javascript" src="plugins/breakpoints/breakpoints.js"></script>
<script type="text/javascript" src="plugins/dataTables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="plugins/prettyPhoto-plugin/js/jquery.prettyPhoto.js"></script> 

<script type="text/javascript" src="plugins/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="plugins/tagsInput/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="plugins/bootstrap-switch/bootstrap-switch.min.js"></script>
<script type="text/javascript" src="plugins/blockUI/jquery.blockUI.js"></script>
<script type="text/javascript" src="plugins/pnotify/js/jquery.pnotify.min.js"></script>

<script type="text/javascript" src="js/theme.js"></script>
<script type="text/javascript" src="custom/custom.js"></script>
 
<!-- Specific Page Scripts Put Here -->
<!-- Flot Chart  -->
<script type="text/javascript" src="plugins/flot/jquery.flot.min.js"></script>
<script type="text/javascript" src="plugins/flot/jquery.flot.resize.min.js"></script>
<script type="text/javascript" src="plugins/flot/jquery.flot.pie.min.js"></script>
<script type="text/javascript" src="plugins/flot/jquery.flot.categories.min.js"></script>
<script type="text/javascript" src="plugins/flot/jquery.flot.time.min.js"></script>
<script type="text/javascript" src="plugins/flot/jquery.flot.animator.min.js"></script>

<!-- Vector Map -->
<script type="text/javascript" src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script type="text/javascript" src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>

<!-- Calendar -->
<script type="text/javascript" src='plugins/moment/moment.min.js'></script>
<script type="text/javascript" src='plugins/jquery-ui/jquery-ui.custom.min.js'></script>
<script type="text/javascript" src='plugins/fullcalendar/fullcalendar.min.js'></script>

<!-- Intro JS (Tour) -->
<script type="text/javascript" src='plugins/introjs/js/intro.min.js'></script>

<!-- Sky Icons -->
<script type="text/javascript" src='plugins/skycons/skycons.js'></script>
<!--Texto enriquezido-->
<script type="text/javascript" src="js/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="js/bootstrap-wysihtml5.js"></script>
<script src="js/summernote.min.js"></script>

<script type="text/javascript">
// Guardar comentario
	function GuardarComentario(id){
		var autor = $('#autor-'+id).val();
		var email = $('#email-'+id).val();
		var comentario = $('#comentario-'+id).val();
		$.ajax({
			url: "ajax.php",
			type: "POST",
			data: { id:id,autor: autor, email: email,comentario:comentario,editarcomentario: 1},
			cache: false,
			success: function(html){
				// alert success
				alert('El comentario fue editado exitosamente.');
			}
		})
	}
// Eliminar Comentario
	function EliminarComentario(id){
		var mensaje = confirm("Estas seguro de eliminar el comentario?");
		if (mensaje == true) {
			$.ajax({
				url: "ajax.php",
				type: "POST",
				data: { id:id,eliminarcomentario: 1},
				cache: false,
				success: function(html){
					// alert success
					$('.row-comentario-'+id).fadeOut('slow');
					alert('El comentario fue eliminado exitosamente.');
				}
			})
		} else {
			return false;
		}
	}
// Texto enriquezido
	$(document).ready(function(){
		$('.wysihtml5').wysihtml5();
		$('textarea').summernote({
			height: 200,                 // set editor height

			minHeight: null,             // set minimum height of editor
			maxHeight: null,             // set maximum height of editor

			focus: true                 // set focus to editable area after initializing summernote
		});

	});


// Guardar nuevo submenu
$('#nuevo-guardar').click(function(){
	var datos = new FormData();   // FormData (Conjunto de datos) llamado "datos"
	var titulo = $('#nuevo-titulo').val();
	var categoria = $('#nuevo-categoria').val();
	var subtitulo = $('#nuevo-subtitulo').val();
	var autor = $('#nuevo-autor').val();
	
	var fecha = $('#nuevo-fecha').val();
	var selMulti = $.map($("#nuevo-tags option:selected"), function (el, i) { return $(el).val(); });
    var tags = selMulti.join(",");
	
	var parrafo = $('#nuevo-parrafo').val();
	var imagenes = document.getElementById("nuevo-imagen"); //Creamos un objeto con el elemento que contiene los archivos: el campo input file, que tiene el id = 'archivos'
	var imagen = imagenes.files; //Obtenemos los archivos seleccionados en el imput
	for (i = 0; i < imagen.length; i++) {
		datos.append('imagen' + i, imagen[i]); //Añadimos cada archivo a el arreglo con un indice direfente
	}
	if (imagen.length != 0) {
		var siimagen = 1;
		datos.append('siimagen', siimagen);
		datos.append('nuevanoticia', 1);
		datos.append('titulo', titulo);
		datos.append('subtitulo', subtitulo);
		datos.append('parrafo', parrafo);
		datos.append('categoria', categoria);
		datos.append('fecha', fecha);
		datos.append('tags', tags);
		datos.append('autor', autor);
		$.ajax({
			url: "ajax.php",
			type: 'POST',
			contentType: false,
			data: datos,
			processData: false,
			cache: false,
			success: function(html) {
				window.location = 'noticias.php?categoria='+categoria;
				//console.log(html);
			}
		})
	}else{
		alert('La imagen es necesaria'); return false;
	}
});
<?php if(isset($_GET["edicion"])){ ?>
// Guardar nuevo subsubmenu
$('#editar-guardar').click(function(){
	var datos = new FormData();   // FormData (Conjunto de datos) llamado "datos"
	var id = <?php echo $_GET["id"]; ?>;
	var titulo = $('#editar-titulo').val();
	var categoria = $('#editar-categoria').val();
	var subtitulo = $('#editar-subtitulo').val();
	var parrafo = $('#editar-parrafo').val();
	var autor = $('#editar-autor').val();
	
	var fecha = $('#editar-fecha').val();
	var selMulti = $.map($("#editar-tags option:selected"), function (el, i) { return $(el).val(); });
    var tags = selMulti.join(",");
	
	var imagenes = document.getElementById("editar-imagen"); //Creamos un objeto con el elemento que contiene los archivos: el campo input file, que tiene el id = 'archivos'
	var imagen = imagenes.files; //Obtenemos los archivos seleccionados en el imput
	for (i = 0; i < imagen.length; i++) {
		datos.append('imagen' + i, imagen[i]); //Añadimos cada archivo a el arreglo con un indice direfente
	}
	if (imagen.length != 0) {
		var siimagen = 1;
		datos.append('siimagen', siimagen);
	}
	datos.append('id', id);
	datos.append('edicionnoticia', 1);
	datos.append('titulo', titulo);
	datos.append('subtitulo', subtitulo);
	datos.append('parrafo', parrafo);
	datos.append('categoria', categoria);
	datos.append('fecha', fecha);
	datos.append('tags', tags);
	datos.append('autor', autor);
	$.ajax({
		url: "ajax.php",
		type: 'POST',
		contentType: false,
		data: datos,
		processData: false,
		cache: false,
		success: function(html) {
			//console.log(html);
			window.location = 'noticias.php?categoria='+categoria;
		}
	})
});
<?php } ?>
$(window).load(function () 
	{
		$.fn.UseTooltip = function () {
			var previousPoint = null;
			 
			$(this).bind("plothover", function (event, pos, item) {        
					if (item) {
						if (previousPoint != item.dataIndex) {
		
							previousPoint = item.dataIndex;
		
							$("#tooltip").remove();
							var x = item.datapoint[0].toFixed(2),
							y = item.datapoint[1].toFixed(2);
		
							showTooltip(item.pageX, item.pageY,
								"<p class='vd_bg-green'><strong class='mgr-10 mgl-10'>" + Math.round(x)  + " ENE 2015 </strong></p>" +
								"<div style='padding: 0 10px 10px;'>" +
								"<div>" + item.series.label +": <strong>"+ Math.round(y)  +"</strong></div>" +
//								"<div> Profit: <strong>$"+ Math.round(y)*7  +"</strong></div>" +
								"</div>"
							);
						}
					} else {
						$("#tooltip").remove();
						previousPoint = null;            
					}
			});
		};
		 
		function showTooltip(x, y, contents) {
			$('<div id="tooltip">' + contents + '</div>').css({
				position: 'absolute',
				display: 'none',
				top: y + 5,
				left: x + 20,    
				size: '10',  
//				'border-top' : '3px solid #1FAE66',
				'background-color': '#111111',
				color: "#FFFFFF",
				opacity: 0.85
			}).appendTo("body").fadeIn(200);
		}


/* REVENUE LINE CHART */

	var d2 = [ [1, 250],
            [2, 150],
            [3, 50],
            [4, 200],
            [5,50],
            [6, 150],
            [7, 150],
            [8, 200],
            [9, 100],
            [10, 250],
            [11,250],
            [12, 200],
            [13, 300]			

];
	var d1 = [
			[1, 650],
            [2, 550],
            [3, 450],
            [4, 550],
            [5, 350],
            [6, 500],
            [7, 600],
            [8, 450],
            [9, 300],
            [10, 600],
            [11, 400],
            [12, 500],
            [13, 700]					
			
];
	var plot = $.plotAnimator($("#revenue-line-chart"), [
			{  	label: "Usuarios",
				data: d2, 	
				lines: {				
					fill: 0.4,
					lineWidth: 0,				
				},
				color:['#f2be3e']
			},{ 
				data: d1, 
				animator: {steps: 60, duration: 1000, start:0}, 		
				lines: {lineWidth:2},	
				shadowSize:0,
				color: '#F85D2C'
			},{
				label: "Usuarios",
				data: d1, 
				points: { show: true, fill: true, radius:6,fillColor:"#F85D2C",lineWidth:3 },	
				color: '#fff',				
				shadowSize:0
			},
			{	label: "Marcas",
				data: d2, 
				points: { show: true, fill: true, radius:6,fillColor:"#f2be3e",lineWidth:3 },	
				color: '#fff',				
				shadowSize:0
			}
		],{	xaxis: {
		tickLength: 0,
		tickDecimals: 0,
		min:2,

				font :{
					lineHeight: 13,
					style: "normal",
					weight: "bold",
					family: "sans-serif",
					variant: "small-caps",
					color: "#6F7B8A"
				}
			},
			yaxis: {
				ticks: 3,
                tickDecimals: 0,
				tickColor: "#f0f0f0",
				font :{
					lineHeight: 13,
					style: "normal",
					weight: "bold",
					family: "sans-serif",
					variant: "small-caps",
					color: "#6F7B8A"
				}
			},
			grid: {
				backgroundColor: { colors: [ "#fff", "#fff" ] },
				borderWidth:1,borderColor:"#f0f0f0",
				margin:0,
				minBorderMargin:0,							
				labelMargin:20,
				hoverable: true,
				clickable: true,
				mouseActiveRadius:6
			},
			legend: { show: false}
		});

 		$("#revenue-line-chart").UseTooltip();		

		$(window).on("resize", function(){
			plot.resize();
			plot.setupGrid();
			plot.draw();
		});
				


		



/* FULL CALENDAR  */
		
		var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();
		
		$('#calendar').fullCalendar({
			header: {
				left:   'title',
				center: '',
				right:  'today prev,next'
			},
			editable: true,
			events: [
				{
					title: 'All Day Event',
					start: new Date(y, m, 1)
				},
				{
					title: 'Long Event',
					start: new Date(y, m, d-5),
					end: new Date(y, m, d-2)
				},
				{
					id: 999,
					title: 'Repeating Event',
					start: new Date(y, m, d-3, 16, 0),
					allDay: false
				},
				{
					id: 999,
					title: 'Repeating Event',
					start: new Date(y, m, d+4, 16, 0),
					allDay: false
				},
				{
					title: 'Meeting',
					start: new Date(y, m, d, 10, 30),
					allDay: false
				},
				{
					title: 'Lunch',
					start: new Date(y, m, d, 12, 0),
					end: new Date(y, m, d, 14, 0),
					allDay: false
				},
				{
					title: 'Birthday Party',
					start: new Date(y, m, d+1, 19, 0),
					end: new Date(y, m, d+1, 22, 30),
					allDay: false
				},
				{
					title: 'Click for Google',
					start: new Date(y, m, 28),
					end: new Date(y, m, 29),
					url: 'http://google.com/'
				}
			]
		});
		

// Skycons

      var icons = new Skycons({"color": "white","resizeClear": true}),
	  	  icons_btm = new Skycons({"color": "#F89C2C","resizeClear": true}),
          list  = "clear-day",
		  livd_btm = ["rain", "wind"
		  ];
		  icons.set(list,list)
      for(var i = livd_btm.length; i--; )
        icons_btm.set(livd_btm[i], livd_btm[i]);

      icons.play();
	  icons_btm.play();

/* News Widget */
	   $(".vd_news-widget .vd_carousel").carouFredSel({
			prev:{
				button : function()
				{
					return $(this).parent().parent().children('.vd_carousel-control').children('a:first-child')
				}
			},
			next:{
				button : function()
				{
					return $(this).parent().parent().children('.vd_carousel-control').children('a:last-child')
				}
			},		
			scroll: {
				fx: "crossfade",
				onBefore: function(){
						var target = "#front-1-clients";
						$(target).css("transition","all .5s ease-in-out 0s");				
					if ($(target).hasClass("vd_bg-soft-yellow")){						
						$(target).removeClass("vd_bg-soft-yellow");
						$(target).addClass("vd_bg-soft-red");		
					} else
					if ($(target).hasClass("vd_bg-soft-red")){						
						$(target).removeClass("vd_bg-soft-red");
						$(target).addClass("vd_bg-soft-blue");		
					} else
					if ($(target).hasClass("vd_bg-soft-blue")){						
						$(target).removeClass("vd_bg-soft-blue");
						$(target).addClass("vd_bg-soft-green");		
					} else
					if ($(target).hasClass("vd_bg-soft-green")){						
						$(target).removeClass("vd_bg-soft-green");
						$(target).addClass("vd_bg-soft-yellow");		
					} 					
				}
			},
			width: "auto",
			height: "responsive",
			responsive: true,
			auto:3000
			
		});



});
</script>
<!-- Specific Page Scripts END -->




<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information. -->

<script>
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-XXXXX-X']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script> 

</body>
</html>
<?php
// Unset para todas las variables declaradas
$variables = array_keys(get_defined_vars());
for ($i = 0; $i < count($variables); $i++) {
    unset($variables[$i]);
}
unset($variables,$i);
//echo memory_get_usage();
		mysql_close($conx);
	}	// Cierre del ELSE
?>