<?php
ob_start("ob_gzhandler");
define('TIMEZONE', 'America/Mexico_City');
date_default_timezone_set(TIMEZONE);
setlocale(LC_ALL,"es_ES");
// Conexion a BD
	require_once("conexion.php"); // conexion a BD
	session_start(); // iniciar variables de sesion
	if(isset($_GET["cerrar"])){	// Si cerro sesion el usuario, ser&Aacute; redireccionado al login
		session_destroy();	// Destruir variables de seson
		header("Location:index.php");	// redireccion al login
	}
	if(@$_SESSION["iniciada"]==NULL){	// Si el usuario no ha iniciado sesion sera redireciconado al login
		header("Location:index.php");	// redireccion al login
	}else{								// Si el usuario ya inicio sesion ser&Aacute; redireccionado al index.php
?>
<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html><!--<![endif]-->

<!-- Specific Page Data -->

<!-- End of Data -->

<head>
    <meta charset="utf-8" />
    <title>Universidad Lasalle | Administrador Maestro</title>
    <meta name="keywords" content="CMS Maestro, Universidad Lasalle" />
    <meta name="description" content="CMS Maestro, Universidad Lasalle">
    <meta name="author" content="Universidad Lasalle">
    
    <!-- Set the viewport width to device width for mobile -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">    
    
    
    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="img/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="img/ico/favicon.png">
    
    
    <!-- CSS -->
       
    <!-- Bootstrap & FontAwesome & Entypo CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if IE 7]><link type="text/css" rel="stylesheet" href="css/font-awesome-ie7.min.css"><![endif]-->
    <link href="css/font-entypo.css" rel="stylesheet" type="text/css">    

    <!-- Fonts CSS -->
    <link href="css/fonts.css"  rel="stylesheet" type="text/css">
               
    <!-- Plugin CSS -->
    <link href="plugins/jquery-ui/jquery-ui.custom.min.css" rel="stylesheet" type="text/css">    
    <link href="plugins/prettyPhoto-plugin/css/prettyPhoto.css" rel="stylesheet" type="text/css">
    <link href="plugins/isotope/css/isotope.css" rel="stylesheet" type="text/css">
    <link href="plugins/pnotify/css/jquery.pnotify.css" media="screen" rel="stylesheet" type="text/css">    
	<link href="plugins/google-code-prettify/prettify.css" rel="stylesheet" type="text/css"> 
   
         
    <link href="plugins/mCustomScrollbar/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css">
    <link href="plugins/tagsInput/jquery.tagsinput.css" rel="stylesheet" type="text/css">
    <link href="plugins/bootstrap-switch/bootstrap-switch.css" rel="stylesheet" type="text/css">    
    <link href="plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css">    
    <link href="plugins/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css">
    <link href="plugins/colorpicker/css/colorpicker.css" rel="stylesheet" type="text/css">            

	<!-- Specific CSS -->
	<link href="plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"><link href="plugins/fullcalendar/fullcalendar.print.css" rel="stylesheet" type="text/css"><link href="plugins/introjs/css/introjs.min.css" rel="stylesheet" type="text/css">    
     
    <!-- Theme CSS -->
    <link href="css/theme.min.css" rel="stylesheet" type="text/css">
    <!--[if IE]> <link href="css/ie.css" rel="stylesheet" > <![endif]-->
    <link href="css/chrome.css" rel="stylesheet" type="text/chrome"> <!-- chrome only css -->    

	<!-- Texto enriquezido -->
	<link rel="stylesheet" type="text/css" href="css/bootstrap-wysihtml5.css">
    <link href="css/summernote.css" rel="stylesheet">
        
    <!-- Responsive CSS -->
    <link href="css/theme-responsive.min.css" rel="stylesheet" type="text/css"> 

	  
 
 
    <!-- for specific page in style css -->
        
    <!-- for specific page responsive in style css -->
        
    
    <!-- Custom CSS -->
    <link href="custom/custom.css" rel="stylesheet" type="text/css">



    <!-- Head SCRIPTS -->
    <script type="text/javascript" src="js/modernizr.js"></script> 
    <script type="text/javascript" src="js/mobile-detect.min.js"></script> 
    <script type="text/javascript" src="js/mobile-detect-modernizr.js"></script> 
 
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script type="text/javascript" src="js/html5shiv.js"></script>
      <script type="text/javascript" src="js/respond.min.js"></script>     
    <![endif]-->
    <style>
		#objetivo,#Perfil,#Cuotas,#estudios,#Becas,#contacto,#adicionales-nuevo,.addicc{display:none}
	</style>
</head>    

<body id="dashboard" class="full-layout nav-left-medium nav-right-hide nav-right-start-hide  nav-top-fixed      responsive    clearfix" data-active="dashboard "  data-smooth-scrolling="1">     
<div class="vd_body">
<!-- Header Start -->
  <header class="header-1" id="header">
      <div class="vd_top-menu-wrapper">
        <div class="container ">
          <div class="vd_top-nav vd_nav-width  ">
          <div class="vd_panel-header">
          	<div class="logo">
            	<a href="index.html"><img alt="logo" src="img/logo.png"></a>
            </div>
            <!-- logo
            <div class="vd_panel-menu  hidden-sm hidden-xs" data-intro="<strong>Mininzar menú izquierda</strong><br/>Puede ampliar o reducir el menu." data-step=1>
            		                	<span class="nav-medium-button menu" data-toggle="tooltip" data-placement="bottom" data-original-title="Menú Bloques" data-action="nav-left-medium">
	                    <i class="fa fa-bars"></i>
                    </span>
                                       
            </div>
            <div class="vd_panel-menu left-pos visible-sm visible-xs">
                                 
                        <span class="menu" data-action="toggle-navbar-left">
                            <i class="fa fa-ellipsis-v"></i>
                        </span>  
                            
                              
            </div>
            -->
            <div class="vd_panel-menu visible-sm visible-xs">
                	<span class="menu visible-xs" data-action="submenu">
	                    <i class="fa fa-bars"></i>
                    </span>        
                          
                    <!--<span class="menu visible-sm visible-xs" data-action="toggle-navbar-right">
                            <i class="fa fa-comments"></i>
                        </span> -->                  
                   	 
                </div>
          

            <!-- vd_panel-menu -->
          </div>
          <!-- vd_panel-header -->
            
          </div>    
          <div class="vd_container">
          	<div class="row">
            	<div class="col-sm-5 col-xs-12">
                </div>
                <div class="col-sm-7 col-xs-12">
              		<div class="vd_mega-menu-wrapper">
                    	<div class="vd_mega-menu pull-right">
            				<ul class="mega-ul">
    <li id="top-menu-1" class="one-icon mega-li"> 


    </li>
      </a>
      
     <!-- perfil administrador maestro --> 
    <li id="top-menu-profile" class="profile mega-li"> 
        <a href="#" class="mega-link"  data-action="click-trigger"> 
            <span  class="mega-image">
                <img src="img/avatar/avatar0.jpg" alt="example image" />               
            </span>
            <span class="mega-name">
                Universidad Lasalle / Admin Master <i class="fa fa-caret-down fa-fw"></i> 
            </span>
        </a> 
      <div class="vd_mega-menu-content  width-xs-3  left-xs left-sm" data-action="click-target">
        <div class="child-menu"> 
        	<div class="content-list content-menu">
                <ul class="list-wrapper pd-lr-10">
                    <li> <a href="#"> <div class="menu-icon"><i class=" fa fa-user"></i></div> <div class="menu-text">Perfil Administrador</div> </a> </li>
                    <li> <a href="lockscreen.html"> <div class="menu-icon"><i class="  fa fa-key"></i></div> <div class="menu-text">Bloquear Cuenta</div> </a> </li>
                    <li class="line"></li>                
                    <li> <a href="logout.html"> <div class="menu-icon"><i class=" fa fa-sign-out"></i></div> <div class="menu-text">Salirme</div> </a> </li> 
                </ul>
            </div> 
        </div> 
      </div>     
  
    </li>               
       

	</ul>
<!-- Head menu search form ends -->                         
                        </div>
                    </div>
                </div>

            </div>
          </div>
        </div>
        <!-- container --> 
      </div>
      <!-- vd_primary-menu-wrapper --> 

  </header>
  <!-- Header Ends --> 
<div class="content">
  <div class="container">
    <div class="vd_navbar vd_nav-width vd_navbar-tabs-menu vd_navbar-left ">
	<div class="navbar-tabs-menu clearfix">
			<span class="expand-menu" data-action="expand-navbar-tabs-menu">
            	<span class="menu-icon menu-icon-left">
            		<i class="fa fa-ellipsis-h"></i>
                 
                </span>
            	<span class="menu-icon menu-icon-right">
            		<i class="fa fa-ellipsis-h"></i>
                    <span class="badge vd_bg-red">
                        20
                    </span>                    
                </span>                
            </span>
    </div>
	<div class="navbar-menu clearfix">
        <div class="vd_menu">
			<!-- Head menu search form ends -->  
			<?php include "sidebar.php"; ?>
		</div>             
    </div>
    <div class="navbar-spacing clearfix">
    </div>
    <div class="vd_menu vd_navbar-bottom-widget">
        <ul>
            <li>
                <a href="pages-logout.html">
                    <span class="menu-icon"><i class="fa fa-sign-out"></i></span>          
                    <span class="menu-text">Salirme</span>             
                </a>
                
            </li>
        </ul>
    </div>     
</div>       
    <!-- Middle Content Start -->
    
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <div class="vd_head-section clearfix">
            <div class="vd_panel-header">
              <ul class="breadcrumb">
                <li><a href="index.html">Home</a> </li>
                <li class="active">Dashboard</li>
              </ul>
              <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step=5  data-position="left">
    <div data-action="remove-navbar" data-original-title="Quitar Barra de navegación" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
      <div data-action="remove-header" data-original-title="Quitar Menú superior " data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
      <div data-action="fullscreen" data-original-title="Quitar Barra de navegación y Menú superior" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>
      
</div>
 
            </div>
          </div>
          <!-- vd_head-section -->
          
          <div class="vd_title-section clearfix">
            <div class="vd_panel-header">
              <div class="vd_panel-header">
                <h1>Dashboard</h1>
                <small class="subtitle">Bienvenido(a) al administrador de Universidad Lasalle.</small>

 <!-- vd_panel-menu -->
              </div>
              <h1>&nbsp;</h1>
            </div>
            <!-- vd_panel-header --> 
          </div>
          <!-- vd_title-section -->   
          
          <div class="vd_content-section clearfix">
            <div class="row">              
                        
            <div class="clearfix"></div>
            
            <div class="col-md-12">
				<div class="panel widget" style="margin: 0 auto;">
					<div class="panel-heading vd_bg-grey">
						<h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-pencil-square-o"></i> </span> Items de oferta educativa </h3>
					</div>
					<?php if(isset($_GET["listado"])){ ?>
						<div class="panel-body table-responsive">
								<div class="row">
									<div class="col-md-12">
										<button class="btn btn-info" id="agregar" data-toggle="modal" data-target="#modal-componente">+ Nuevo item</button>
									</div>
								</div>
								<div class="row item-componente">
								<?php 
									function items(){
										$sql= "select * from oferta_educativa_items where submenu_id=".$_GET["submenu"]." and subsubmenu_id=".$_GET["subsubmenu"]." order by id asc";
										$query=mysql_query($sql) or die(mysql_error());
										while ($resultados = mysql_fetch_assoc($query)){
											$id[] = $resultados["id"];
											$imagen[] = $resultados["imagen"];
											$parrafo1[] = $resultados["parrafo_1"];
											$duracion[] = $resultados["duracion"];
											$creditos[] = $resultados["creditos"];
											$semestres[] = $resultados["semestres"];
											$plan[] = $resultados["plan"];
											$objetivo_parrafo[] = $resultados["objetivo_parrafo"];
											$perfil_parrafo_1[] = $resultados["perfil_parrafo_1"];
											$perfil_parrafo_2[] = $resultados["perfil_parrafo_2"];
											$titulo[] = $resultados["titulo"];
											$subtitulo[] = $resultados["subtitulo"];
										}mysql_free_result($query);
										$i=0; while($i < count(@$id)){ ?>
									<div class="col-md-2">
										<a href="submenu-oferta-educativa.php?menu=<?php echo $_GET["menu"]; ?>&submenu=<?php echo $_GET["submenu"]; ?>&subsubmenu=<?php echo $_GET["subsubmenu"]; ?>&item=<?php echo $id[$i]; ?>&edicion=1">
											<img src="../lasalle/oferta/img/<?php echo $imagen[$i]; ?>" class="imagen-componente">
											<div class="titulo-item-oferta"><?php echo $titulo[$i]; ?></div>
											<a href="ajax.php?id=<?php echo $id[$i]; ?>&tabla=oferta_educativa_items&eliminar=1" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></a>
											<button data-toggle="modal" data-target="#modal-componente-editando" class="btn btn-success btn-xs" onclick="EditandoItem(<?php echo $id[$i]; ?>);"><i class="fa fa-pencil"></i></button>
										</a>
									</div>
								<?php $i++; } } items(); ?>
								</div>
						</div>
					<?php } if(isset($_GET["edicion"])){ 
								// Seleccionamos los datos del item de oferta educativa
									$sql= "select * from oferta_educativa_items where id=".$_GET["item"]."";
									$query=mysql_query($sql) or die(mysql_error());
									while ($resultados = mysql_fetch_assoc($query)){
										$titulo = $resultados["titulo"];
										$subtitulo = $resultados["subtitulo"];
										$imagen = $resultados["imagen"];
										$parrafo1 = $resultados["parrafo_1"];
										$duracion = $resultados["duracion"];
										$creditos = $resultados["creditos"];
										$semestres = $resultados["semestres"];
										$plan_ano = $resultados["plan"];
										$objetivo_parrafo = $resultados["objetivo_parrafo"];	// OBJETIVO
										$perfil_parrafo_1 = $resultados["perfil_parrafo_1"];	// PERFIL
										$perfil_parrafo_2 = $resultados["perfil_parrafo_2"];	
										$contacto_parrafo = $resultados["contacto_parrafo"];	// CONTACTO
										$contacto_imagen = $resultados["contacto_imagen"];	// CONTACTO
									}mysql_free_result($query);
								// Seleccionamos los datos de cuotas
									$sql= "select * from oferta_educativa_items_cuotas where oferta_educativa_item_id=".$_GET["item"]."";
									$query=mysql_query($sql) or die(mysql_error());
									while ($resultados = mysql_fetch_assoc($query)){
										$cuota_id[] = $resultados["id"];						// CUOTAS
										$cuota_semestre[] = $resultados["semestre"];
										$cuota_parrafo[] = $resultados["parrafo"];
									}mysql_free_result($query);
								// Seleccionamos los datos de plan de estudios
									$sql= "select * from oferta_educativa_items_plan_becas where oferta_educativa_item_id=".$_GET["item"]." and tipo='plan'";
									$query=mysql_query($sql) or die(mysql_error());
									while ($resultados = mysql_fetch_assoc($query)){
										$plan_id[] = $resultados["id"];							// PLAN DE ESTUDIOS
										$plan_titulo[] = $resultados["titulo"];
										$plan_texto[] = $resultados["texto"];
									}mysql_free_result($query);
								// Seleccionamos los datos de becas
									$sql= "select * from oferta_educativa_items_plan_becas where oferta_educativa_item_id=".$_GET["item"]." and tipo='becas'";
									$query=mysql_query($sql) or die(mysql_error());
									while ($resultados = mysql_fetch_assoc($query)){
										$becas_id[] = $resultados["id"];						// BECAS
										$becas_titulo[] = $resultados["titulo"];
										$becas_texto[] = $resultados["texto"];
									}mysql_free_result($query);
								// imagen de contacto
									if($contacto_imagen == NULL){$contacto_imagen = '123contacto.png';}
								// Seleccionamos items adicionales
									$sql= "select * from oferta_educativa_items_adicionales where id_item_oferta=".$_GET["item"]."";
									$query=mysql_query($sql) or die(mysql_error());
									while ($resultados = mysql_fetch_assoc($query)){
										$adicional_id[] = $resultados["id"];						
										$adicional_imagen[] = $resultados["imagen"];
										$adicional_contenido[] = $resultados["contenido"];
									}mysql_free_result($query);
								// Deshabilitados	
									$sql= "select * from iconos_oferta where id_item=".$_GET["item"]."";
									$query=mysql_query($sql) or die(mysql_error());
									while ($resultados = mysql_fetch_assoc($query)){
										$status_objetivo = $resultados["objetivo"];						
										$estatus_perfil = $resultados["perfil"];
										$estatus_cuotas = $resultados["cuotas"];
										$estatus_plan = $resultados["plan"];
										$estatus_becas = $resultados["becas"];
										$estatus_contacto = $resultados["contacto"];
									}mysql_free_result($query);
									if($status_objetivo == 1){$status_objetivo = 'green';}else{$status_objetivo = 'red';}
									if($estatus_perfil == 1){$estatus_perfil = 'green';}else{$estatus_perfil = 'red';}
									if($estatus_cuotas == 1){$estatus_cuotas = 'green';}else{$estatus_cuotas = 'red';}
									if($estatus_plan == 1){$estatus_plan = 'green';}else{$estatus_plan = 'red';}
									if($estatus_becas == 1){$estatus_becas = 'green';}else{$estatus_becas = 'red';}
									if($estatus_contacto == 1){$estatus_contacto = 'green';}else{$estatus_contacto = 'red';}
					?>
						<div class="row">
							<div class="col-md-12">
								<a href="javascript:void(0);" onclick="mostrar('objetivo');">Objetivo</a>
								<button class="btn btn-success btn-xs"  style="background-color:<?php echo $status_objetivo; ?>" onclick="IconosOferta('objetivo',<?php echo $_GET["item"]; ?>,this);">Habilitar</button>
							</div>
						</div>
						<div class="row" id="objetivo">
							<div class="col-md-12">
								<label for="editar-objetivo-parrafo">Párrafo</label>
								<textarea id="edicion-objetivo-parrafo"><?php echo $objetivo_parrafo; ?></textarea>
							</div>
							<div class="col-md-4">
								<button class="btn btn-success" id="edicion-guardar-objetivo">GUARDAR OBJETIVO</button>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<a href="javascript:void(0);" onclick="mostrar('Perfil');">Perfil</a>
								<button class="btn btn-success btn-xs" style="background-color:<?php echo $estatus_perfil; ?>" onclick="IconosOferta('perfil',<?php echo $_GET["item"]; ?>,this);">Habilitar</button>
							</div>
						</div>
						<div class="row" id="Perfil">
							<div class="col-md-12">
								<label for="edicion-perfil-parrafo1">Párrafo 1</label>
								<textarea id="edicion-perfil-parrafo1"><?php echo $perfil_parrafo_1; ?></textarea>
							</div>
							<div class="col-md-12">
								<label for="edicion-perfil-parrafo2">Párrafo 2</label>
								<textarea id="edicion-perfil-parrafo2"><?php echo $perfil_parrafo_2; ?></textarea>
							</div>
							<div class="col-md-4">
								<button class="btn btn-success" id="edicion-guardar-perfil">GUARDAR PERFIL</button>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<a href="javascript:void(0);" onclick="mostrar('Cuotas');">Cuotas</a>
								<button class="btn btn-success btn-xs" style="background-color:<?php echo $estatus_cuotas; ?>" onclick="IconosOferta('cuotas',<?php echo $_GET["item"]; ?>,this);">Habilitar</button>
							</div>
						</div>
						<div class="row" id="Cuotas">
							<div class="col-md-12">
								<button type="button" id="agregar-cuota" data-toggle="modal" data-target="#modal-agregar-cuota" style="float:left">+ semestre</button>
								<form method="post" enctype="multipart/form-data" action="ajax.php" onsubmit="alert('CSV importado correctamente.');return false;">
									<button type="submit" name="importar" class="btn btn-success btn-xs" style="float:right;margin: 5px;">Importar Excel <i class="fa fa-download"></i></button>
									<input type="file" name="excel" style="float:right;width:20%" accept=".csv">
									<input type="hidden" value="oferta_educativa_items_cuotas" name="tabla">
									<input type="hidden" value="<?php echo $_GET["item"]; ?>" name="item">
								</form>
							</div>
							<div class="col-md-12" class="cuota-items">
								<?php $i=0; while($i < count(@$cuota_id)){ ?>
									<div class="col-md-4 padre-cuota-<?php echo $cuota_id[$i]; ?>" style="background: rgba(85, 85, 85, 0.34);">
										<div class="col-md-12">
											<label for="edicion-cuotas-semestre">Semestre</label>
											<input type="text" data-cuota="<?php echo $cuota_id[$i]; ?>" id="edicion-cuotas-semestre" value="<?php echo $cuota_semestre[$i]; ?>">
										</div>
										<div class="col-md-12">
											<label for="edicion-cuotas-semestre">Párrafo</label>
											<textarea id="edicion-cuotas-parrafo" data-cuota="<?php echo $cuota_id[$i]; ?>"><?php echo $cuota_parrafo[$i]; ?></textarea>
										</div>
										<div class="col-md-12">
											<button class="btn btn-danger" onclick="EliminarItemOferta(<?php echo $cuota_id[$i]; ?>,'oferta_educativa_items_cuotas')">ELIMINAR</button>
											<button class="btn btn-success" onclick="EditarItemOferta(<?php echo $cuota_id[$i]; ?>,'oferta_educativa_items_cuotas')">GUARDAR</button>
										</div>
									</div>
								<?php $i++; } ?> 
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<a href="javascript:void(0);" onclick="mostrar('estudios');">Plan de estudios</a>
								<button class="btn btn-success btn-xs" style="background-color:<?php echo $estatus_plan; ?>" onclick="IconosOferta('plan',<?php echo $_GET["item"]; ?>,this);">Habilitar</button>
							</div>
						</div>
						<div class="row" id="estudios">
							<div class="col-md-4">
								<button type="button" id="agregar-plan" data-toggle="modal" data-target="#modal-agregar-plan">+ cuadro gris</button>
							</div>
							<div class="col-md-12" class="plan-items">
								<?php $i=0; while($i < count(@$plan_id)){ ?>
									<div class="col-md-4 padre-planbeca-<?php echo $plan_id[$i]; ?>" style="background: rgba(85, 85, 85, 0.34);">
										<div class="col-md-12">
											<label for="edicion-plan-titulo">Título</label>
											<input type="text" data-plan="<?php echo $plan_id[$i]; ?>" data-planbeca="<?php echo $plan_id[$i]; ?>" id="edicion-plan-titulo" value="<?php echo $plan_titulo[$i]; ?>">
										</div>
										<div class="col-md-12">
											<label for="edicion-plan-parrafo">Párrafo</label>
											<textarea id="edicion-plan-parrafo" data-plan="<?php echo $plan_id[$i]; ?>" data-planbeca="<?php echo $plan_id[$i]; ?>"><?php echo $plan_texto[$i]; ?></textarea>
										</div>
										<div class="col-md-12">
											<button class="btn btn-danger" onclick="EliminarItemOferta(<?php echo $plan_id[$i]; ?>,'oferta_educativa_items_plan_becas')">ELIMINAR</button>
											<button class="btn btn-success" onclick="EditarItemOferta(<?php echo $plan_id[$i]; ?>,'oferta_educativa_items_plan_becas')">GUARDAR</button>
										</div>
									</div>
								<?php $i++; } ?> 
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<a href="javascript:void(0);" onclick="mostrar('Becas');">Becas</a>
								<button class="btn btn-success btn-xs" style="background-color:<?php echo $estatus_becas; ?>" onclick="IconosOferta('becas',<?php echo $_GET["item"]; ?>,this);">Habilitar</button>
							</div>
						</div>
						<div class="row" id="Becas">
							<div class="col-md-4">
								<button type="button" id="agregar-beca" data-toggle="modal" data-target="#modal-agregar-becas">+ cuadro gris</button>
							</div>
							<div class="col-md-12" class="becas-items">
								<?php $i=0; while($i < count(@$becas_id)){ ?>
								<div class="col-md-4 padre-planbeca-<?php echo $becas_id[$i]; ?>" style="background: rgba(85, 85, 85, 0.34);">
									<div class="col-md-12">
										<label for="edicion-beca-titulo">Título</label>
										<input type="text" data-beca="<?php echo $becas_id[$i]; ?>" data-planbeca="<?php echo $becas_id[$i]; ?>" id="edicion-beca-titulo" value="<?php echo $becas_titulo[$i]; ?>">
									</div>
									<div class="col-md-12">
										<label for="edicion-beca-parrafo">Párrafo</label>
										<textarea id="edicion-beca-parrafo" data-beca="<?php echo $becas_id[$i]; ?>" data-planbeca="<?php echo $becas_id[$i]; ?>"><?php echo $becas_texto[$i]; ?></textarea>
									</div>
									<div class="col-md-12">
										<button class="btn btn-danger" onclick="EliminarItemOferta(<?php echo $becas_id[$i]; ?>,'oferta_educativa_items_plan_becas')">ELIMINAR</button>
										<button class="btn btn-success" onclick="EditarItemOferta(<?php echo $becas_id[$i]; ?>,'oferta_educativa_items_plan_becas')">GUARDAR</button>
									</div>
								</div>
								<?php $i++; } ?> 
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<a href="javascript:void(0);" onclick="mostrar('contacto');">Contacto</a>
								<button class="btn btn-success btn-xs" style="background-color:<?php echo $estatus_contacto; ?>" onclick="IconosOferta('contacto',<?php echo $_GET["item"]; ?>,this);">Habilitar</button>
							</div>
						</div>
						<div class="row" id="contacto">
							<div class="col-md-6">
								<img src="../lasalle/oferta/img/<?php echo $contacto_imagen; ?>" style="display:block">
								<label for="edicion-contacto-imagen">Imágen</label>
								<input type="file" id="edicion-contacto-imagen" accept="image/*">
							</div>
							<div class="col-md-12">
								<label for="edicion-contacto-parrafo">Párrafo</label>
								<textarea id="edicion-contacto-parrafo"><?php echo $contacto_parrafo; ?></textarea>
							</div>
							<div class="col-md-12">
								<button class="btn btn-success" id="edicion-guardar-contacto">GUARDAR CONTACTO</button>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<a href="javascript:void(0);" class="btn btn-success btn-xs" onclick="mostrar('adicionales-nuevo');">+ item</a>
							</div>
						</div>
						<div class="row" id="adicionales-nuevo">
							<div class="col-md-6">
								<label for="nuevo-adicionales-imagen">Imágen (Medidas de 166 x 35 y Formato .PNG)</label>
								<input type="file" id="nuevo-adicionales-imagen" accept=".png">
							</div>
							<div class="col-md-12">
								<label for="nuevo-adicionales-parrafo">Contenido</label>
								<textarea id="nuevo-adicionales-parrafo"><?php echo $adicional_contenido[$j]; ?></textarea>
							</div>
							<div class="col-md-12">
								<button class="btn btn-success" id="nuevo-guardar-adicionales" onclick="NuevoAdicional();">GUARDAR</button>
							</div>
						</div>
						<?php $j=0; while($j < count(@$adicional_id)){ ?>
						<div class="row">
							<div class="col-md-12">
								<a href="javascript:void(0);" onclick="mostrar('adicionales-<?php echo $adicional_id[$j]; ?>');">Adicionales no. <?php echo $j + 1; ?></a>
								<a class="btn btn-danger btn-xs" href="ajax.php?id=<?php echo $adicional_id[$j]; ?>&tabla=oferta_educativa_items_adicionales&eliminar=1">eliminar</a>
							</div>
						</div>
						<div class="row addicc" id="adicionales-<?php echo $adicional_id[$j]; ?>">
							<div class="col-md-6">
								<img src="../lasalle/oferta/adicionales/<?php echo $adicional_imagen[$j]; ?>" style="display:block">
								<label for="edicion-adicionales-imagen-<?php echo $adicional_id[$j]; ?>">Imágen (Medidas de 166 x 35 y Formato .PNG)</label>
								<input type="file" id="edicion-adicionales-imagen-<?php echo $adicional_id[$j]; ?>" accept=".png">
							</div>
							<div class="col-md-12">
								<label for="edicion-adicionales-parrafo-<?php echo $adicional_id[$j]; ?>">Contenido</label>
								<textarea id="edicion-adicionales-parrafo-<?php echo $adicional_id[$j]; ?>"><?php echo $adicional_contenido[$j]; ?></textarea>
							</div>
							<div class="col-md-12">
								<button class="btn btn-success" id="edicion-guardar-adicionales" onclick="EditarAdicional(<?php echo $adicional_id[$j]; ?>)">GUARDAR</button>
							</div>
						</div>
						<?php $j++; } ?>
					<?php } ?>
				</div>
            </div>     
            
              <div class="col-md-5">
                <div class="row">
                  <div class="col-md-12">
                    <div class="vd_status-widget vd_bg-green widget">
    <div class="vd_panel-menu">
  <div data-action="refresh" data-original-title="Actualizar" data-rel="tooltip" class=" menu entypo-icon smaller-font"> <i class="icon-cycle"></i> </div>
</div>
<!-- vd_panel-menu --> </div>                    
                  </div>
                  <!--col-md-12 --> 
                </div>
                <!-- .row -->
                <div class="row">
                  <div class="col-xs-6">
                    <div class="vd_status-widget vd_bg-red  widget">
    <div class="vd_panel-menu">
  <div data-action="refresh" data-original-title="Actualizar" data-rel="tooltip" class=" menu entypo-icon smaller-font"> <i class="icon-cycle"></i> </div>
</div>
<!-- vd_panel-menu -->                                  
                                                                 
</div>                    </div>
                  <!--col-xs-6 -->
                  <div class="col-xs-6">
                    <div class="vd_status-widget vd_bg-blue widget">
    <div class="vd_panel-menu">
  <div data-action="refresh" data-original-title="Actualizar" data-rel="tooltip" class=" menu entypo-icon smaller-font"> <i class="icon-cycle"></i> </div>
</div>
<!-- vd_panel-menu -->                             
                 </div>                   
                  </div>
                  <!--col-xs-6 --> 
                </div>
                <!-- .row -->
                <div class="row">
                  <div class="col-xs-6">
                    <div class="vd_status-widget vd_bg-yellow widget">
    <div class="vd_panel-menu">
  <div data-action="refresh" data-original-title="Actualizar" data-rel="tooltip" class=" menu entypo-icon smaller-font"> <i class="icon-cycle"></i> </div>
</div>
<!-- vd_panel-menu --> 
                                
                                                                
</div>                    
                  </div>
                  <!--col-xs-6 -->
                  <div class="col-xs-6">
                    <div class="vd_status-widget vd_bg-grey widget">
    <div class="vd_panel-menu">
  <div data-action="refresh" data-original-title="Actualizar" data-rel="tooltip" class=" menu entypo-icon smaller-font"> <i class="icon-cycle"></i> </div>
</div>
<!-- vd_panel-menu -->                                   
                                                                    
                    </div>                   
                  </div>
                  <!--col-md-xs-6 --> 
                </div>
                <!-- .row --> 
                
              </div>
              <!-- .col-md-5 --> 
            </div>
            <div class="row">
              <div class="col-md-12">                

<!-- Panel Widget -->
              </div>
              <!--col-md-5-->
              <div class="col-md-4">
                

<!-- vd_panel-menu --> 

<!-- Panel Widget -->              
              </div>
              <!--col-md-4--> 

              
            </div>
            <!-- row --> 
            
          </div>
          <!-- .vd_content-section --> 
          
        </div>
        <!-- .vd_content --> 
      </div>
      <!-- .vd_container --> 
    </div>
    <!-- .vd_content-wrapper --> 
    
    <!-- Middle Content End --> 
    
  </div>
  <!-- .container --> 
</div>
<!-- .content -->

<!-- Footer Start -->
  <footer class="footer-1"  id="footer">      
    <div class="vd_bottom ">
        <div class="container">
            <div class="row">
              <div class=" col-xs-12">
                <div class="copyright">
                    Copyright &copy;2016 Universidad Lasalle. All Rights Reserved 
                </div>
              </div>
            </div><!-- row -->
        </div><!-- container -->
    </div>
  </footer>
<!-- Footer END -->

<!-- Modal -->
  <div class="modal fade" id="modal-componente" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Item de oferta educativa</h4>
        </div>
        <div class="modal-body">
			<div class="row">
				<div class="col-md-6">
					<label for="nuevo-imagen">Imágen</label>
					<input type="file" id="nuevo-imagen" name="imagen" accept="image/*">
				</div>
				<div class="col-md-12">
					<label for="nuevo-titulo">Título</label>
					<input type="text" id="nuevo-titulo">
				</div>
				<div class="col-md-12">
					<label for="nuevo-subtitulo">Subtítulo</label>
					<input type="text" id="nuevo-subtitulo">
				</div>
				<div class="col-md-12">
					<label for="nuevo-parrafo">Párrafo</label>
					<textarea id="nuevo-parrafo"></textarea>
				</div>
				<div class="col-md-12">
					<label for="nuevo-tabla">Muestra tabla de antecedentes</label>
					<input type="checkbox" id="nuevo-tabla" checked>
				</div>
				<div id="nuevo-div-tabla">
					<div class="col-md-3">
						<label for="nuevo-duracion">Duración</label>
						<input type="text" id="nuevo-duracion">
					</div>
					<div class="col-md-3">
						<label for="nuevo-creditos">Créditos</label>
						<input type="text" id="nuevo-creditos">
					</div>
					<div class="col-md-3">
						<label for="nuevo-semestres">Semestres</label>
						<input type="text" id="nuevo-semestres">
					</div>
					<div class="col-md-3">
						<label for="nuevo-plan-ano">Plan de estudios</label>
						<input type="text" id="nuevo-plan-ano">
					</div>
					
					<div class="col-md-3">
						<label for="nuevo-modalidad">Modalidad</label>
						<input type="text" id="nuevo-modalidad">
					</div>
					<div class="col-md-3">
						<label for="nuevo-tipo">Tipo de ciclo</label>
						<input type="text" id="nuevo-tipo">
					</div>
					<div class="col-md-6">
						<label for="nuevo-antecedentes">Antecedentes académicos de ingreso</label>
						<input type="text" id="nuevo-antecedentes">
					</div>
				</div>
			</div>
        </div>
        <div class="modal-footer">
			<button type="button" class="btn btn-successs" id="nuevo-oferta">Guardar</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      
    </div>
  </div>
  
  <!-- Modal -->
  <div class="modal fade" id="modal-componente-editando" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Item de oferta educativa</h4>
        </div>
        <div class="modal-body">
			<div class="row">
				<div class="col-md-6">
					<img src="" class="imagen-item-componente">
					<label for="editando-imagen">Imágen</label>
					<input type="file" id="editando-imagen" name="imagen" accept="image/*">
				</div>
				<div class="col-md-12">
					<label for="editando-titulo">Título</label>
					<input type="text" id="editando-titulo">
				</div>
				<div class="col-md-12">
					<label for="editando-subtitulo">Subtítulo</label>
					<input type="text" id="editando-subtitulo">
				</div>
				<div class="col-md-12">
					<label for="editando-parrafo">Párrafo</label>
					<textarea id="editando-parrafo"></textarea>
				</div>
				<div class="col-md-12">
					<label for="editando-tabla">Muestra tabla de antecedentes</label>
					<input type="checkbox" id="editando-tabla" checked>
				</div>
				<div id="editando-div-tabla">
					<div class="col-md-3">
						<label for="editando-duracion">Duración</label>
						<input type="text" id="editando-duracion">
					</div>
					<div class="col-md-3">
						<label for="editando-creditos">Créditos</label>
						<input type="text" id="editando-creditos">
					</div>
					<div class="col-md-3">
						<label for="editando-semestres">Semestres</label>
						<input type="text" id="editando-semestres">
					</div>
					<div class="col-md-3">
						<label for="editando-plan-ano">Plan de estudios</label>
						<input type="text" id="editando-plan-ano">
					</div>
					
					<div class="col-md-3">
						<label for="editando-modalidad">Modalidad</label>
						<input type="text" id="editando-modalidad">
					</div>
					<div class="col-md-3">
						<label for="editando-tipo">Tipo de ciclo</label>
						<input type="text" id="editando-tipo">
					</div>
					<div class="col-md-6">
						<label for="editando-antecedentes">Antecedentes académicos de ingreso</label>
						<input type="text" id="editando-antecedentes">
					</div>
				</div>
					<input type="hidden" id="editando-id">
			</div>
        </div>
        <div class="modal-footer">
			<button type="button" class="btn btn-successs" id="editando-oferta">Guardar</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      
    </div>
  </div>
  
<!-- Modal -->
  <div class="modal fade" id="modal-componente-item" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Item de componente</h4>
        </div>
        <div class="modal-body">
			<div class="row">
				<div class="col-md-6">
					<label for="imagen">Imágen</label>
					<input type="file" id="editar-imagen" name="editar-imagen" accept="image/*">
					<img src="" id="imagen-edicion">
				</div>
				<div class="col-md-6">
					<label for="archivo">Archivo</label>
					<input type="file" id="editar-archivo" name="editar-archivo" accept=".pdf">
					<a href="" target="_blank" id="descarga-preview">Descargar</a>
				</div>
				<div class="col-md-12">
					<label for="parrafo">Párrafo</label>
					<textarea id="editar-parrafo"></textarea>
				</div>
				<input type="hidden" id="editar-item-id" value="">
			</div>
        </div>
        <div class="modal-footer">
			<button type="button" class="btn btn-successs" id="editar-item" onclick="GuardarEditarItem();">Guardar</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      
    </div>
  </div>
  
  <!-- Modal grises nuevo -->
  <div class="modal fade" id="modal-componente-item-grises" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Item de componente</h4>
        </div>
        <div class="modal-body">
			<div class="row">
				<div class="col-md-12">
					<label for="grises-nuevo-titulo">Título</label>
					<input type="text" id="grises-nuevo-titulo">
				</div>
				<div class="col-md-12">
					<label for="grises-nuevo-parrafo">Párrafo</label>
					<textarea id="grises-nuevo-parrafo"></textarea>
				</div>
				<div class="col-md-12">
					<label for="grises-nuevo-link">Link</label>
					<input type="text" id="grises-nuevo-link">
				</div>
			</div>
        </div>
        <div class="modal-footer">
			<button type="button" class="btn btn-successs" id="editar-item" onclick="GuardarEditarItem();">Guardar</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      
    </div>
  </div>
 <!-- Modal cuadros grises edicion-->
  <div class="modal fade" id="modal-componente-item-grises" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Item de componente</h4>
        </div>
        <div class="modal-body">
			<div class="row">
				<div class="col-md-12">
					<label for="grises-titulo">Título</label>
					<input type="text" id="grises-titulo">
				</div>
				<div class="col-md-12">
					<label for="grises-parrafo">Párrafo</label>
					<textarea id="grises-parrafo"></textarea>
				</div>
				<div class="col-md-12">
					<label for="grises-link">Link</label>
					<input type="text" id="grises-link">
				</div>
				<input type="hidden" id="grises-item-id" value="">
			</div>
        </div>
        <div class="modal-footer">
			<button type="button" class="btn btn-successs" id="editar-item" onclick="GuardarEditarItem();">Guardar</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      
    </div>
  </div>
  
<!-- **********************		MODALES PARA AGREGAR		***************************** -->
  <div class="modal fade" id="modal-agregar-cuota" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Cuota</h4>
        </div>
        <div class="modal-body">
			<div class="row">
				<div class="col-md-12">
					<label for="cuota-semestre">Semestre</label>
					<input type="text" id="cuota-semestre">
				</div>
				<div class="col-md-12">
					<label for="cuota-parrafo">Párrafo</label>
					<textarea id="cuota-parrafo"></textarea>
				</div>
			</div>
        </div>
        <div class="modal-footer">
			<button type="button" class="btn btn-successs" id="editar-item" onclick="GuardarCuota();">Guardar</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      
    </div>
  </div>
  
  <div class="modal fade" id="modal-agregar-plan" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Plan de estudios</h4>
        </div>
        <div class="modal-body">
			<div class="row">
				<div class="col-md-12">
					<label for="plan-titulo">Título</label>
					<input type="text" id="plan-titulo">
				</div>
				<div class="col-md-12">
					<label for="plan-parrafo">Párrafo</label>
					<textarea id="plan-parrafo"></textarea>
				</div>
			</div>
        </div>
        <div class="modal-footer">
			<button type="button" class="btn btn-successs" id="editar-item" onclick="GuardarPlan();">Guardar</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      
    </div>
  </div>
  
  <div class="modal fade" id="modal-agregar-becas" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Becas</h4>
        </div>
        <div class="modal-body">
			<div class="row">
				<div class="col-md-12">
					<label for="becas-titulo">Título</label>
					<input type="text" id="becas-titulo">
				</div>
				<div class="col-md-12">
					<label for="becas-parrafo">Párrafo</label>
					<textarea id="becas-parrafo"></textarea>
				</div>
			</div>
        </div>
        <div class="modal-footer">
			<button type="button" class="btn btn-successs" id="editar-item" onclick="GuardarBecas();">Guardar</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      
    </div>
  </div>
	<?php if(isset($_GET["edicion"])){ ?>
		<input type="hidden" id="id-edicion" value="<?php echo $_GET["item"]; ?>">
	<?php } ?>
<!-- .vd_body END  -->
<a id="back-top" href="#" data-action="backtop" class="vd_back-top visible"> <i class="fa  fa-angle-up"> </i> </a>
<div id="scripts"></div>
<!--
<a class="back-top" href="#" id="back-top"> <i class="icon-chevron-up icon-white"> </i> </a> -->

<!-- Javascript =============================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script type="text/javascript" src="js/jquery.js"></script> 
<!--[if lt IE 9]>
  <script type="text/javascript" src="js/excanvas.js"></script>      
<![endif]-->
<script type="text/javascript" src="js/bootstrap.min.js"></script> 
<script type="text/javascript" src='plugins/jquery-ui/jquery-ui.custom.min.js'></script>
<script type="text/javascript" src="plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

<script type="text/javascript" src="js/caroufredsel.js"></script> 
<script type="text/javascript" src="js/plugins.js"></script>

<script type="text/javascript" src="plugins/breakpoints/breakpoints.js"></script>
<script type="text/javascript" src="plugins/dataTables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="plugins/prettyPhoto-plugin/js/jquery.prettyPhoto.js"></script> 

<script type="text/javascript" src="plugins/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="plugins/tagsInput/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="plugins/bootstrap-switch/bootstrap-switch.min.js"></script>
<script type="text/javascript" src="plugins/blockUI/jquery.blockUI.js"></script>
<script type="text/javascript" src="plugins/pnotify/js/jquery.pnotify.min.js"></script>

<script type="text/javascript" src="js/theme.js"></script>
<script type="text/javascript" src="custom/custom.js"></script>
 
<!-- Specific Page Scripts Put Here -->
<!-- Flot Chart  -->
<script type="text/javascript" src="plugins/flot/jquery.flot.min.js"></script>
<script type="text/javascript" src="plugins/flot/jquery.flot.resize.min.js"></script>
<script type="text/javascript" src="plugins/flot/jquery.flot.pie.min.js"></script>
<script type="text/javascript" src="plugins/flot/jquery.flot.categories.min.js"></script>
<script type="text/javascript" src="plugins/flot/jquery.flot.time.min.js"></script>
<script type="text/javascript" src="plugins/flot/jquery.flot.animator.min.js"></script>

<!-- Vector Map -->
<script type="text/javascript" src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script type="text/javascript" src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>

<!-- Calendar -->
<script type="text/javascript" src='plugins/moment/moment.min.js'></script>
<script type="text/javascript" src='plugins/jquery-ui/jquery-ui.custom.min.js'></script>
<script type="text/javascript" src='plugins/fullcalendar/fullcalendar.min.js'></script>

<!-- Intro JS (Tour) -->
<script type="text/javascript" src='plugins/introjs/js/intro.min.js'></script>

<!-- Sky Icons -->
<script type="text/javascript" src='plugins/skycons/skycons.js'></script>

<!--Texto enriquezido-->
<script type="text/javascript" src="js/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="js/bootstrap-wysihtml5.js"></script>
<script src="js/summernote.min.js"></script>

<script type="text/javascript">
// Mostrar u ocultar tabla de antecedentes
	$( "#editando-tabla,#nuevo-tabla" ).click(function() {
		var id = $(this).attr('id');
		if(id == 'editando-tabla'){
			$("#editando-div-tabla").toggle('slow');
		}else{
			$("#nuevo-div-tabla").toggle('slow');
		}
	});

// Editar item adicional
function EditarAdicional(id){
	var datos = new FormData();   // FormData (Conjunto de datos) llamado "datos"
	var imagenes = document.getElementById("edicion-adicionales-imagen-"+id); //Creamos un objeto con el elemento que contiene los archivos: el campo input file, que tiene el id = 'archivos'
	var imagen = imagenes.files; //Obtenemos los archivos seleccionados en el imput
	for (i = 0; i < imagen.length; i++) {
		datos.append('imagen' + i, imagen[i]); //Añadimos cada archivo a el arreglo con un indice direfente
	}
	var contenido = $('#edicion-adicionales-parrafo-'+id).val();
	if (imagen.length != 0) {
		var siimagen = 1;
		datos.append('siimagen', siimagen);
	}
		datos.append('contenido', contenido);
		datos.append('id', id);
		datos.append('editaradicional', 1);
		
		$.ajax({
			url: "ajax.php",
			type: 'POST',
			contentType: false,
			data: datos,
			processData: false,
			cache: false,
			success: function(html) {
				//console.log(html);
				window.location = window.location;
			}
		})
}

// Nuuevo item adicional
function NuevoAdicional(){
	var datos = new FormData();   // FormData (Conjunto de datos) llamado "datos"
	var imagenes = document.getElementById("nuevo-adicionales-imagen"); //Creamos un objeto con el elemento que contiene los archivos: el campo input file, que tiene el id = 'archivos'
	var imagen = imagenes.files; //Obtenemos los archivos seleccionados en el imput
	for (i = 0; i < imagen.length; i++) {
		datos.append('imagen' + i, imagen[i]); //Añadimos cada archivo a el arreglo con un indice direfente
	}
	var contenido = $('#nuevo-adicionales-parrafo').val();
	var ofertaitem = $('#id-edicion').val();
	if (imagen.length != 0) {
		var siimagen = 1;
		datos.append('siimagen', siimagen);
	}else{
		alert('La imagen es necesaria');
		return false;
	}
		datos.append('contenido', contenido);
		datos.append('ofertaitem', ofertaitem);
		datos.append('nuevoadicional', 1);
		
		$.ajax({
			url: "ajax.php",
			type: 'POST',
			contentType: false,
			data: datos,
			processData: false,
			cache: false,
			success: function(html) {
				//console.log(html);
				window.location = window.location;
			}
		})
}

// Edita un item de cuotas, plan y becas
function CSV(){
	
}

function IconosOferta(icono,item,dato){
	if(dato.style.backgroundColor == 'red'){
		$.ajax({
			url: "ajax.php",
			type: "POST",
			data: { icono:icono,item:item,status:1,iconosoferta: 1},
			cache: false,
			success: function(html){
				console.log(html);
			}
		})
		dato.style.backgroundColor = 'green';
	}else{
		$.ajax({
			url: "ajax.php",
			type: "POST",
			data: { icono:icono,item:item,status:0,iconosoferta: 1},
			cache: false,
			success: function(html){
				console.log(html);
			}
		})
		dato.style.backgroundColor = 'red';
	}
}

function EditarItemOferta(id,tabla){
	var CampoTitulo = "";
	var CampoTexto = "";
	var ValorTitulo = "";
	var ValorTexto = "";
	// Si la tabla es de cuotas
	if(tabla == "oferta_educativa_items_cuotas"){
		CampoTitulo = "semestre";
		CampoTexto = "parrafo";
		ValorTitulo = $("input[data-cuota='" + id +"']").val();
		ValorTexto = $("textarea[data-cuota='" + id +"']").val();
	}
	// Si la tabla es de plan o becas
	if(tabla == "oferta_educativa_items_plan_becas"){
		CampoTitulo = "titulo";
		CampoTexto = "texto";
		ValorTitulo = $("input[data-planbeca='" + id +"']").val();
		ValorTexto = $("textarea[data-planbeca='" + id +"']").val();
	}
	$.ajax({
		url: "ajax.php",
		type: "POST",
		data: { campotitulo:CampoTitulo,campotexto:CampoTexto,valortitulo:ValorTitulo,valortexto:ValorTexto,id:id,tabla:tabla,edicionitemsseparado: 1},
		cache: false,
		success: function(html){
			console.log(html);
			alert("El item ha sido editado exitosamente."); return false;
			//window.location = window.location;
		}
	})
}

// Elimina un item de cuotas, plan y becas
function EliminarItemOferta(id,tabla){
	$.ajax({
		url: "ajax.php",
		type: "POST",
		data: {id:id,tabla:tabla,eliminaritemsseparado: 1},
		cache: false,
		success: function(html){
			console.log(html);
			if(tabla == "oferta_educativa_items_cuotas"){
				$('.padre-cuota-'+id).hide('slow');
			}else{
				$('.padre-planbeca-'+id).hide('slow');
			}			
			alert("El item ha sido eliminado exitosamente."); 
			return false;
		}
	})
}

// Texto enriquezido
	$(document).ready(function(){
		$('.wysihtml5').wysihtml5();
		$('textarea').summernote({
			height: 200,                 // set editor height

			minHeight: null,             // set minimum height of editor
			maxHeight: null,             // set maximum height of editor

			focus: true                 // set focus to editable area after initializing summernote
		});

	});

// Guardar Objetivo
$('#edicion-guardar-objetivo').click(function(){
	var texto = $('#edicion-objetivo-parrafo').val();
	var ofertaitem = $('#id-edicion').val();
	$.ajax({
		url: "ajax.php",
		type: "POST",
		data: { ofertaitem:ofertaitem,texto:texto,edicionobjetivooferta: 1},
		cache: false,
		success: function(html){
			//window.location = window.location;
		}
	})
});
// Guardar Perfil
$('#edicion-guardar-perfil').click(function(){
	var parrafo1 = $('#edicion-perfil-parrafo1').val();
	var parrafo2 = $('#edicion-perfil-parrafo2').val();
	var ofertaitem = $('#id-edicion').val();
	$.ajax({
		url: "ajax.php",
		type: "POST",
		data: { ofertaitem:ofertaitem,parrafo1:parrafo1,parrafo2:parrafo2,edicionperfiloferta: 1},
		cache: false,
		success: function(html){
			//window.location = window.location;
		}
	})
});
// Guardar Contacto
$('#edicion-guardar-contacto').click(function(){
	var datos = new FormData();   // FormData (Conjunto de datos) llamado "datos"
	var imagenes = document.getElementById("edicion-contacto-imagen"); //Creamos un objeto con el elemento que contiene los archivos: el campo input file, que tiene el id = 'archivos'
	var imagen = imagenes.files; //Obtenemos los archivos seleccionados en el imput
	for (i = 0; i < imagen.length; i++) {
		datos.append('imagen' + i, imagen[i]); //Añadimos cada archivo a el arreglo con un indice direfente
	}
	var parrafo1 = $('#edicion-contacto-parrafo').val();
	var ofertaitem = $('#id-edicion').val();
	if (imagen.length != 0) {
		var siimagen = 1;
		datos.append('siimagen', siimagen);
	}
		
		datos.append('parrafo1', parrafo1);
		datos.append('ofertaitem', ofertaitem);
		datos.append('edicioncontactooferta', 1);
		
		$.ajax({
			url: "ajax.php",
			type: 'POST',
			contentType: false,
			data: datos,
			processData: false,
			cache: false,
			success: function(html) {
				//window.location = window.location;
			}
		})
});
function GuardarCuota(){
	var semestres = $('#cuota-semestre').val();
	var parrafo = $('#cuota-parrafo').val();
	var ofertaitem = $('#id-edicion').val();
	$.ajax({
		url: "ajax.php",
		type: "POST",
		data: { ofertaitem:ofertaitem,semestres:semestres,parrafo:parrafo,edicioncuotaoferta: 1},
		cache: false,
		success: function(html){
			window.location = window.location;
		}
	})
}
function GuardarPlan(){
	var titulo = $('#plan-titulo').val();
	var parrafo = $('#plan-parrafo').val();
	var ofertaitem = $('#id-edicion').val();
	$.ajax({
		url: "ajax.php",
		type: "POST",
		data: { ofertaitem:ofertaitem,titulo:titulo,parrafo:parrafo,edicionplanoferta: 1},
		cache: false,
		success: function(html){
			window.location = window.location;
		}
	})
}
function GuardarBecas(){
	var titulo = $('#becas-titulo').val();
	var parrafo = $('#becas-parrafo').val();
	var ofertaitem = $('#id-edicion').val();
	$.ajax({
		url: "ajax.php",
		type: "POST",
		data: { ofertaitem:ofertaitem,titulo:titulo,parrafo:parrafo,edicionbecasoferta: 1},
		cache: false,
		success: function(html){
			window.location = window.location;
		}
	})
}
function mostrar(div){
	//$('#'+div).fadeIn('slow');
	$('#'+div).toggle( "slow", function() {
	});
}
// Guardar nuevo item en el componente
$('#nuevo-oferta').click(function(){
	var datos = new FormData();   // FormData (Conjunto de datos) llamado "datos"
	var titulo = $('#nuevo-titulo').val();
	var subtitulo = $('#nuevo-subtitulo').val();
	var parrafo = $('#nuevo-parrafo').val();
	var duracion = $('#nuevo-duracion').val();
	var creditos = $('#nuevo-creditos').val();
	var semestres = $('#nuevo-semestres').val();
	var PlanAno = $('#nuevo-plan-ano').val();
	var modalidad = $('#nuevo-modalidad').val();
	var tipo = $('#nuevo-tipo').val();
	var antecedentes = $('#nuevo-antecedentes').val();
	var muestratabla = 1;
	if($("#nuevo-tabla").is(':checked')){
		 muestratabla = 1;
	} else {
		 muestratabla = 0;
	}
	var imagenes = document.getElementById("nuevo-imagen"); //Creamos un objeto con el elemento que contiene los archivos: el campo input file, que tiene el id = 'archivos'
	var imagen = imagenes.files; //Obtenemos los archivos seleccionados en el imput
	for (i = 0; i < imagen.length; i++) {
		datos.append('imagen' + i, imagen[i]); //Añadimos cada archivo a el arreglo con un indice direfente
	}
	if (imagen.length != 0) {
		var siimagen = 1;
		datos.append('submenu', <?php echo $_GET["submenu"]; ?>);
		datos.append('subsubmenu', <?php echo $_GET["subsubmenu"]; ?>);
		datos.append('siimagen', siimagen);
		datos.append('nuevoitemoferta', 1);
		datos.append('titulo', titulo);
		datos.append('subtitulo', subtitulo);
		datos.append('parrafo', parrafo);
		datos.append('duracion', duracion);
		datos.append('creditos', creditos);
		datos.append('semestres', semestres);
		datos.append('planano', PlanAno);
		datos.append('modalidad', modalidad);
		datos.append('tipo', tipo);
		datos.append('antecedentes', antecedentes);
		datos.append('muestratabla', muestratabla);
		$.ajax({
			url: "ajax.php",
			type: 'POST',
			contentType: false,
			data: datos,
			processData: false,
			cache: false,
			success: function(iditem) {
				window.location = 'submenu-oferta-educativa.php?menu=<?php echo $_GET["menu"]; ?>&submenu=<?php echo $_GET["submenu"]; ?>&subsubmenu=<?php echo $_GET["subsubmenu"]; ?>&item='+iditem+'&edicion=1';
			}
		})
	}else{
		alert('La imagen es necesaria'); return false;
	}
});
// Poner id en modal para editar
function EditandoItem(id){
	$.ajax({
		url: "ajax.php",
		type: "POST",
		data: { id:id,vistaeditandoofertaitem: 1},
		cache: false,
		success: function(html){
			$('#scripts').html(html);
		}
	})
}
// EDITANDO ITEM
$('#editando-oferta').click(function(){
	var datos = new FormData();   // FormData (Conjunto de datos) llamado "datos"
	var id = $('#editando-id').val();
	var titulo = $('#editando-titulo').val();
	var subtitulo = $('#editando-subtitulo').val();
	var parrafo = $('#editando-parrafo').val();
	var duracion = $('#editando-duracion').val();
	var creditos = $('#editando-creditos').val();
	var semestres = $('#editando-semestres').val();
	var PlanAno = $('#editando-plan-ano').val();
	var muestratabla = 1;
	if($("#editando-tabla").is(':checked')){
		 muestratabla = 1;
	} else {
		 muestratabla = 0;
	}
	
	var modalidad = $('#editando-modalidad').val();
	var tipo = $('#editando-tipo').val();
	var antecedentes = $('#editando-antecedentes').val();
	
	var imagenes = document.getElementById("editando-imagen"); //Creamos un objeto con el elemento que contiene los archivos: el campo input file, que tiene el id = 'archivos'
	var imagen = imagenes.files; //Obtenemos los archivos seleccionados en el imput
	for (i = 0; i < imagen.length; i++) {
		datos.append('imagen' + i, imagen[i]); //Añadimos cada archivo a el arreglo con un indice direfente
	}
	if (imagen.length != 0) {
		var siimagen = 1;
		datos.append('siimagen', siimagen);
	}
		datos.append('submenu', <?php echo $_GET["submenu"]; ?>);
		datos.append('subsubmenu', <?php echo $_GET["subsubmenu"]; ?>);
		datos.append('editandoitemoferta', 1);
		datos.append('titulo', titulo);
		datos.append('subtitulo', subtitulo);
		datos.append('parrafo', parrafo);
		datos.append('duracion', duracion);
		datos.append('creditos', creditos);
		datos.append('semestres', semestres);
		datos.append('planano', PlanAno);
		datos.append('modalidad', modalidad);
		datos.append('tipo', tipo);
		datos.append('antecedentes', antecedentes);
		datos.append('muestratabla', muestratabla);
		datos.append('id', id);
		$.ajax({
			url: "ajax.php",
			type: 'POST',
			contentType: false,
			data: datos,
			processData: false,
			cache: false,
			success: function(html) {
				window.location = window.location;
				//console.log();
			}
		})
});
// Vista de edicion del item
function EditarItem(item){
	$.ajax({
		url: "ajax.php",
		type: "POST",
		data: { item:item,vistaitemcomponente: 1},
		cache: false,
		success: function(html){
			$('#scripts').html(html);
		}
	})
}
// Guardar edicion
function GuardarEditarItem(){
	var idcomponente = $('input#id-componente').val();
	
	var datos = new FormData();   // FormData (Conjunto de datos) llamado "datos"
	var iditem = $('input#editar-item-id').val();
	var parrafo = $('#editar-parrafo').val();
	var imagenes = document.getElementById("editar-imagen"); //Creamos un objeto con el elemento que contiene los archivos: el campo input file, que tiene el id = 'archivos'
	var imagen = imagenes.files; //Obtenemos los archivos seleccionados en el imput
	for (i = 0; i < imagen.length; i++) {
		datos.append('imagen' + i, imagen[i]); //Añadimos cada archivo a el arreglo con un indice direfente
	}
	datos.append('editaritemcomponente', 1);
	datos.append('parrafo', parrafo);
	datos.append('iditem', iditem);
	if (imagen.length != 0) {
		var siimagen = 1;
		datos.append('siimagen', siimagen);
	}	
		
		$.ajax({
			url: "ajax.php",
			type: 'POST',
			contentType: false,
			data: datos,
			processData: false,
			cache: false,
			success: function(html) {
				// Archivo de descarga
				adjunto2 = new FormData();
				var archivos = document.getElementById("editar-archivo"); //Creamos un objeto con el elemento que contiene los archivos: el campo input file, que tiene el id = 'archivos'
				var archivo = archivos.files; //Obtenemos los archivos seleccionados en el imput
				for (i = 0; i < archivo.length; i++) {
					adjunto2.append('archivo' + i, archivo[i]); //Añadimos cada archivo a el arreglo con un indice direfente
				}
				if (archivo.length != 0) {
					var siarchivo = 1;
					adjunto2.append('siarchivo', siarchivo);
					adjunto2.append('iditem', iditem);
					adjunto2.append('archivoeditaritemcomponente', 1);
					$.ajax({
						url: "ajax.php",
						type: 'POST',
						contentType: false,
						data: adjunto2,
						processData: false,
						cache: false,
						success: function(html2) {
							
						}
					})
				}
				//console.log(html);
				//window.location = 'componente.php?componente='+idcomponente+'&edicion=1';
				window.location = window.location;
			}
		})
}
///// Cajas de texto
// Guardar edicion
function EditarItemCaja(){
	var parrafo = $('#parrafo-caja').val();
	var item = $('#id-componente').val();
	$.ajax({
		url: "ajax.php",
		type: "POST",
		data: { item:item,parrafo:parrafo,guardarcaja: 1},
		cache: false,
		success: function(html){
			$('#scripts').html(html);
		}
	})
}
// Nuevo item grises
///////////////////////////////////////////////////////// PENDIENTE PODER GUARDAR UN NUEVO ITEM GRIS
$('#agregar-item-grises').click(function(){
	var idcomponente = $('#grises-nuevo-titulo').val();
	var parrafo = $('#parrafo').val();
	$.ajax({
		url: "ajax.php",
		type: "POST",
		data: { item:item,vistaitemcomponentegris: 1},
		cache: false,
		success: function(html){
			$('#scripts').html(html);
		}
	})
});
// Vista de edicion del item tipo Cuadros Grises
function EditarItemGris(item){
	$.ajax({
		url: "ajax.php",
		type: "POST",
		data: { item:item,vistaitemcomponentegris: 1},
		cache: false,
		success: function(html){
			$('#scripts').html(html);
		}
	})
}
$(window).load(function () 
	{




		$.fn.UseTooltip = function () {
			var previousPoint = null;
			 
			$(this).bind("plothover", function (event, pos, item) {        
					if (item) {
						if (previousPoint != item.dataIndex) {
		
							previousPoint = item.dataIndex;
		
							$("#tooltip").remove();
							var x = item.datapoint[0].toFixed(2),
							y = item.datapoint[1].toFixed(2);
		
							showTooltip(item.pageX, item.pageY,
								"<p class='vd_bg-green'><strong class='mgr-10 mgl-10'>" + Math.round(x)  + " ENE 2015 </strong></p>" +
								"<div style='padding: 0 10px 10px;'>" +
								"<div>" + item.series.label +": <strong>"+ Math.round(y)  +"</strong></div>" +
//								"<div> Profit: <strong>$"+ Math.round(y)*7  +"</strong></div>" +
								"</div>"
							);
						}
					} else {
						$("#tooltip").remove();
						previousPoint = null;            
					}
			});
		};
		 
		function showTooltip(x, y, contents) {
			$('<div id="tooltip">' + contents + '</div>').css({
				position: 'absolute',
				display: 'none',
				top: y + 5,
				left: x + 20,    
				size: '10',  
//				'border-top' : '3px solid #1FAE66',
				'background-color': '#111111',
				color: "#FFFFFF",
				opacity: 0.85
			}).appendTo("body").fadeIn(200);
		}


/* REVENUE LINE CHART */

	var d2 = [ [1, 250],
            [2, 150],
            [3, 50],
            [4, 200],
            [5,50],
            [6, 150],
            [7, 150],
            [8, 200],
            [9, 100],
            [10, 250],
            [11,250],
            [12, 200],
            [13, 300]			

];
	var d1 = [
			[1, 650],
            [2, 550],
            [3, 450],
            [4, 550],
            [5, 350],
            [6, 500],
            [7, 600],
            [8, 450],
            [9, 300],
            [10, 600],
            [11, 400],
            [12, 500],
            [13, 700]					
			
];
	var plot = $.plotAnimator($("#revenue-line-chart"), [
			{  	label: "Usuarios",
				data: d2, 	
				lines: {				
					fill: 0.4,
					lineWidth: 0,				
				},
				color:['#f2be3e']
			},{ 
				data: d1, 
				animator: {steps: 60, duration: 1000, start:0}, 		
				lines: {lineWidth:2},	
				shadowSize:0,
				color: '#F85D2C'
			},{
				label: "Usuarios",
				data: d1, 
				points: { show: true, fill: true, radius:6,fillColor:"#F85D2C",lineWidth:3 },	
				color: '#fff',				
				shadowSize:0
			},
			{	label: "Marcas",
				data: d2, 
				points: { show: true, fill: true, radius:6,fillColor:"#f2be3e",lineWidth:3 },	
				color: '#fff',				
				shadowSize:0
			}
		],{	xaxis: {
		tickLength: 0,
		tickDecimals: 0,
		min:2,

				font :{
					lineHeight: 13,
					style: "normal",
					weight: "bold",
					family: "sans-serif",
					variant: "small-caps",
					color: "#6F7B8A"
				}
			},
			yaxis: {
				ticks: 3,
                tickDecimals: 0,
				tickColor: "#f0f0f0",
				font :{
					lineHeight: 13,
					style: "normal",
					weight: "bold",
					family: "sans-serif",
					variant: "small-caps",
					color: "#6F7B8A"
				}
			},
			grid: {
				backgroundColor: { colors: [ "#fff", "#fff" ] },
				borderWidth:1,borderColor:"#f0f0f0",
				margin:0,
				minBorderMargin:0,							
				labelMargin:20,
				hoverable: true,
				clickable: true,
				mouseActiveRadius:6
			},
			legend: { show: false}
		});

 		$("#revenue-line-chart").UseTooltip();		

		$(window).on("resize", function(){
			plot.resize();
			plot.setupGrid();
			plot.draw();
		});
				


		



/* FULL CALENDAR  */
		
		var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();
		
		$('#calendar').fullCalendar({
			header: {
				left:   'title',
				center: '',
				right:  'today prev,next'
			},
			editable: true,
			events: [
				{
					title: 'All Day Event',
					start: new Date(y, m, 1)
				},
				{
					title: 'Long Event',
					start: new Date(y, m, d-5),
					end: new Date(y, m, d-2)
				},
				{
					id: 999,
					title: 'Repeating Event',
					start: new Date(y, m, d-3, 16, 0),
					allDay: false
				},
				{
					id: 999,
					title: 'Repeating Event',
					start: new Date(y, m, d+4, 16, 0),
					allDay: false
				},
				{
					title: 'Meeting',
					start: new Date(y, m, d, 10, 30),
					allDay: false
				},
				{
					title: 'Lunch',
					start: new Date(y, m, d, 12, 0),
					end: new Date(y, m, d, 14, 0),
					allDay: false
				},
				{
					title: 'Birthday Party',
					start: new Date(y, m, d+1, 19, 0),
					end: new Date(y, m, d+1, 22, 30),
					allDay: false
				},
				{
					title: 'Click for Google',
					start: new Date(y, m, 28),
					end: new Date(y, m, 29),
					url: 'http://google.com/'
				}
			]
		});
		

// Skycons

      var icons = new Skycons({"color": "white","resizeClear": true}),
	  	  icons_btm = new Skycons({"color": "#F89C2C","resizeClear": true}),
          list  = "clear-day",
		  livd_btm = ["rain", "wind"
		  ];
		  icons.set(list,list)
      for(var i = livd_btm.length; i--; )
        icons_btm.set(livd_btm[i], livd_btm[i]);

      icons.play();
	  icons_btm.play();

/* News Widget */
	   $(".vd_news-widget .vd_carousel").carouFredSel({
			prev:{
				button : function()
				{
					return $(this).parent().parent().children('.vd_carousel-control').children('a:first-child')
				}
			},
			next:{
				button : function()
				{
					return $(this).parent().parent().children('.vd_carousel-control').children('a:last-child')
				}
			},		
			scroll: {
				fx: "crossfade",
				onBefore: function(){
						var target = "#front-1-clients";
						$(target).css("transition","all .5s ease-in-out 0s");				
					if ($(target).hasClass("vd_bg-soft-yellow")){						
						$(target).removeClass("vd_bg-soft-yellow");
						$(target).addClass("vd_bg-soft-red");		
					} else
					if ($(target).hasClass("vd_bg-soft-red")){						
						$(target).removeClass("vd_bg-soft-red");
						$(target).addClass("vd_bg-soft-blue");		
					} else
					if ($(target).hasClass("vd_bg-soft-blue")){						
						$(target).removeClass("vd_bg-soft-blue");
						$(target).addClass("vd_bg-soft-green");		
					} else
					if ($(target).hasClass("vd_bg-soft-green")){						
						$(target).removeClass("vd_bg-soft-green");
						$(target).addClass("vd_bg-soft-yellow");		
					} 					
				}
			},
			width: "auto",
			height: "responsive",
			responsive: true,
			auto:3000
			
		});



});
</script>
<!-- Specific Page Scripts END -->




<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information. -->

<script>
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-XXXXX-X']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script> 

</body>
</html>
<?php
// Unset para todas las variables declaradas
$variables = array_keys(get_defined_vars());
for ($i = 0; $i < count($variables); $i++) {
    unset($variables[$i]);
}
unset($variables,$i);
//echo memory_get_usage();
		mysql_close($conx);
	}	// Cierre del ELSE
?>