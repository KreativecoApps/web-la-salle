<?php
ob_start("ob_gzhandler");
define('TIMEZONE', 'America/Mexico_City');
date_default_timezone_set(TIMEZONE);
setlocale(LC_ALL,"es_ES");
error_reporting(0);

// Conexion a BD
	require_once("conexion.php"); // conexion a BD
	require_once("funciones-micrositios.php"); // funciones
	$micrositio = $_GET["micrositio"];
?>
<!DOCTYPE html>
<html lang="es" class="no-js">
<head>
<!-- Prueba de index.html -->
<!-- Jose -->



  <!-- Create:hdzlruben.
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>La Salle</title>
  <meta name="description" content="">
  <meta name="author" content="Kreativeco">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Number Safari
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="format-detection" content="telephone=no">

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  
  <?php
	$query = mysql_query("select titulo from micrositios where id=".$_GET["micrositio"]."");
	$results = mysql_fetch_array($query); mysql_free_result($query);
	$tituloo = $results['titulo'];
	
	$query = mysql_query("select color from micrositios where id=".$_GET["micrositio"]."");
	$results = mysql_fetch_array($query); mysql_free_result($query);
	$color = $results['color'];
	$tono = null;
	if($color == 1){$tono = '#083a81';}
	if($color == 2){$tono = '#2a7fb8';}
	if($color == 3){$tono = '#818d8d';}
	if($color == 4){$tono = '#7537c3';}
	if($color == 5){$tono = '#eb740f';}
	if($color == 6){$tono = '#292929';}
	if($color == 7){$tono = '#bf3a29';}
	if($color == 8){$tono = '#019a34';}
  ?>

  

  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">
  <link rel="stylesheet" href="css/facultades/facu.css">
  <link rel="stylesheet" href="css/noticias/noticias-micrositios.css">
  <link rel="stylesheet" href="css/modal/remodal.css">
  <link rel="stylesheet" href="css/sliderPrincipal/sliderPrincipal-micrositios.css"/>
  <link rel="stylesheet" href="css/modal/remodal-default-theme.css">
  <link rel="stylesheet" href="css/calendario/responsive-calendar.css">
  <link rel="stylesheet" href="css/calendario/sliderNoticias.css" />
  <link rel="stylesheet" href="css/menuFullScreen/menuFullScreen.css" />
  <link rel="stylesheet" href="css/buttons/button-facu.css">
  <link rel="stylesheet" href="css/slider_menu_oferta/jquery.bxslider.css" />

  <link rel="stylesheet" href="css/bootstrap/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="css/component.css" />
  <link href="css/mosaico.css" rel="stylesheet">


  <style>
      .carousel-indicators .active { background-image: url("") !important; }  
	  
	  form.formContacto input,textarea{margin:0px 0px 3px 0px}
	  .contenedorContacto .cuadrosSubmenu:hover{cursor:pointer}
	  .contenedorDiagonalContacto p{text-align:justify;line-height:1.77778em;font-family:'HelveticaLight',arial !important;font-size:14px}
	  td.img-noticia {padding-right:8px}
	  div#textogeneral{ overflow-y: scroll; height: 272px; text-align: justify;font-family:'HelveticaLight',arial}
	  .day-headers{margin-top:11px !important;}
	  .titulo-modal{font-family:'HelveticaBold',arial}
	  .containerMapa{background:none !important;height:100%}
	  .grisesito{background:#e4e4e4;}
		@media screen and (max-width: 480px) {
			.CambioToSelect{display:none}
			.SelectCampus{display:inherit;text-align: center;}
			.lista-opciones{width:100% !important}
			.img-mos{max-width:100%}
		}
		@media screen and (min-width: 481px) {
			.CambioToSelect{display:inherit}
			.SelectCampus{display:none}
		}
		
		
.carousel-indicators {
    margin: 10px 0 0;
    overflow: auto;
    position: static;
    text-align: left;
    white-space: nowrap;
    width: 11%;
	position:absolute;
	left:20px;
}
.carousel-indicators li {
    background-color: transparent;
    -webkit-border-radius: 0;
    border-radius: 0;
    display: block;
    height: auto;
    margin: 0 !important;
    width: auto;
}
.carousel-indicators li img {
    display: block;
    opacity: 0.5;
}
.carousel-indicators li.active img {
    opacity: 1;
}
.carousel-indicators li:hover img {
    opacity: 0.75;
}
.carousel-outer {
    position: relative;
}
.carousel-indicators .active{
	width:100%;
	height:100%;
}


/* Estilos a aplicar */
		.w3-opacity,.w3-hover-opacity:hover{opacity:0.60;filter:alpha(opacity=60);-webkit-backface-visibility:hidden}
		.w3-opacity-off,.w3-hover-opacity-off:hover{opacity:1;filter:alpha(opacity=100);-webkit-backface-visibility:hidden}
		.w3-opacity-max{opacity:0.25;filter:alpha(opacity=25);-webkit-backface-visibility:hidden}
		.w3-opacity-min{opacity:0.75;filter:alpha(opacity=75);-webkit-backface-visibility:hidden}
		.miniaturas{padding: 12px 0px 0 0 !important;}
		.w3-left{float:left!important}.w3-right{float:right!important}
		.w3-tiny{font-size:10px!important}.w3-small{font-size:12px!important}
		.w3-medium{font-size:15px!important}.w3-large{font-size:18px!important}
		.w3-xlarge{font-size:24px!important}.w3-xxlarge{font-size:36px!important}
		.w3-xxxlarge{font-size:48px!important}.w3-jumbo{font-size:64px!important}
		.w3-vertical{word-break:break-all;line-height:1;text-align:center;width:0.6em}
		.w3-left-align{text-align:left!important}.w3-right-align{text-align:right!important}
		.w3-leftbar{border-left:6px solid #ccc!important}.w3-rightbar{border-right:6px solid #ccc!important}
		.w3-padding-left{padding-left:16px!important;bottom: 45%;position: absolute;font-size: 33px;cursor: pointer;}.w3-padding-right{padding-right:16px!important;position: absolute;bottom: 45%;font-size: 33px;right: 16px;cursor: pointer;}
		.w3-text-khaki,.w3-hover-text-khaki:hover{color:#b4aa50!important}
		
		.li-cat h5{
			color: #fff;
			display: block;
			height: 60px;
			background: yellow;
			border-radius: 1px;
			-moz-transform: scale(1) rotate(10deg) translateX(0px) translateY(0px) skewX(0deg) skewY(0deg);
			-webkit-transform: scale(1) rotate(10deg) translateX(0px) translateY(0px) skewX(0deg) skewY(0deg);
			-o-transform: scale(1) rotate(10deg) translateX(0px) translateY(0px) skewX(0deg) skewY(0deg);
			-ms-transform: scale(1) rotate(10deg) translateX(0px) translateY(0px) skewX(0deg) skewY(0deg);
			transform: scale(1) rotate(10deg) translateX(0px) translateY(0px) skewX(0deg) skewY(0deg);
			
			-ms-transform: matrix(1, -0.3, 0, 1, 0, 0); /* IE 9 */
			-webkit-transform: matrix(1, -0.3, 0, 1, 0, 0); /* Safari */
			transform: matrix(1, 0.3, 0, 1, 0, 0); /* Standard syntax */
		}
		.li-cat span{
			padding: 23px 0px 0px 14px;
			cursor:pointer;
			 position: absolute;
			-moz-transform: scale(1) rotate(-10deg) translateX(0px) translateY(0px) skewX(0deg) skewY(0deg);
			-webkit-transform: scale(1) rotate(-10deg) translateX(0px) translateY(0px) skewX(0deg) skewY(0deg);
			-o-transform: scale(1) rotate(-10deg) translateX(0px) translateY(0px) skewX(0deg) skewY(0deg);
			-ms-transform: scale(1) rotate(-10deg) translateX(0px) translateY(0px) skewX(0deg) skewY(0deg);
			transform: scale(1) rotate(-10deg) translateX(0px) translateY(0px) skewX(0deg) skewY(0deg);
			
			-ms-transform: matrix(1, -0.3, 0, 1, 0, 0); /* IE 9 */
			-webkit-transform: matrix(1, -0.3, 0, 1, 0, 0); /* Safari */
			transform: matrix(1, -0.3, 0, 1, 0, 0); /* Standard syntax */
		}
		.ul-subs{
			display:none;
			cursor:pointer;
			margin-left:0
		}
		.li-sub{
			padding: 5px;
			background-color: #ededed;
			height: 60px;
			color: #000;
			display: block;
			border-radius: 1px;
			-moz-transform: scale(1) rotate(10deg) translateX(0px) translateY(0px) skewX(0deg) skewY(0deg);
			-webkit-transform: scale(1) rotate(10deg) translateX(0px) translateY(0px) skewX(0deg) skewY(0deg);
			-o-transform: scale(1) rotate(10deg) translateX(0px) translateY(0px) skewX(0deg) skewY(0deg);
			-ms-transform: scale(1) rotate(10deg) translateX(0px) translateY(0px) skewX(0deg) skewY(0deg);
			transform: scale(1) rotate(10deg) translateX(0px) translateY(0px) skewX(0deg) skewY(0deg);
			
			-ms-transform: matrix(1, -0.3, 0, 1, 0, 0); /* IE 9 */
			-webkit-transform: matrix(1, -0.3, 0, 1, 0, 0); /* Safari */
			transform: matrix(1, 0.3, 0, 1, 0, 0); /* Standard syntax */
		}
		.li-sub span{
			padding: 10% 0 0 0;
			cursor:pointer;
			 position: absolute;
			-moz-transform: scale(1) rotate(-10deg) translateX(0px) translateY(0px) skewX(0deg) skewY(0deg);
			-webkit-transform: scale(1) rotate(-10deg) translateX(0px) translateY(0px) skewX(0deg) skewY(0deg);
			-o-transform: scale(1) rotate(-10deg) translateX(0px) translateY(0px) skewX(0deg) skewY(0deg);
			-ms-transform: scale(1) rotate(-10deg) translateX(0px) translateY(0px) skewX(0deg) skewY(0deg);
			transform: scale(1) rotate(-10deg) translateX(0px) translateY(0px) skewX(0deg) skewY(0deg);
			
			-ms-transform: matrix(1, -0.3, 0, 1, 0, 0); /* IE 9 */
			-webkit-transform: matrix(1, -0.3, 0, 1, 0, 0); /* Safari */
			transform: matrix(1, -0.3, 0, 1, 0, 0); /* Standard syntax */
		}
		.flechita-drop{
			width:12%;
		}
		.mySlides {display:none;width:100%;}
		.demo {cursor:pointer;width:100%}
		.parrafo-gris{background:#ededed;padding:10px;color:#000;margin-top:10px;}
		.padding-centrado{padding-top:14%}
		.barra-superior-slider{padding: 2%;text-align: center;background-color: <?php echo $tono; ?>;color: #fff;font-weight: bold;}
		.colorgris{color:#444}
  </style>
  



  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="http://www.lasalle.mx/wp-content/themes/lasalle/images/favicon.ico">
    <script>
        var tmp = new Date(),
            initial_date = tmp.getFullYear() + '-' + (tmp.getMonth()+1),
            initial_year = tmp.getFullYear(),
            initial_month = tmp.getMonth()+1
                ;

        //console.log(tmp);
        //console.log(initial_date);
    </script>

</head>
<body>
  
  <!-- BTN MENU PRINCIPAL -->
  <button id="nav_main" class="menuButtonScroll"></button> 
  <!-- FIN BTN MENU PRINCIPAL -->



  <!--  MENU HEADER SEARCH -->
  <div class="container" id="buttonsHeader">
    <div class="row">
      <header>      
        <div class="six columns logo">
          <a href="index.php"><img src="images/nuevosArtes/header/logo.png" alt="La Salle"></a>
        </div>    
        <div class="six columns searchBar">
          <div class="contenedorIdioma">
            <img class="" alt="En" src="images/nuevosArtes/idioma_botones/button_english_selected.png" usemap="#en">
            <map name="en" id="en">
              <area alt="" title="" href="index_en.html?id=2" shape="poly" coords="10,1,17,9,2,30,33,31,41,7,38,3,13,1" />
            </map>
            <img class="" alt="Es" src="images/nuevosArtes/idioma_botones/button_spanish_Selected.png" usemap="#es">
            <map name="es" id="es">
              <area alt="" title="" href="#" shape="poly" coords="10,1,17,9,2,30,33,31,41,7,38,3,13,1" />
            </map>
          </div>        
          <div class="contenedorFormulario">
            <form class="formSearch" method="get" action="busqueda.php">
              <input id="buscar"   name="buscar" value="" class="search" type="text" list="postdl" name="search" placeholder="Ingresa tu busqueda"/>

              <datalist id="postdl" name='postdl'>
                
              </datalist>
              <datalist id="postdls" name='postdls'>
                
              </datalist>
              <input id="buscarpost" class="submit" type="submit" value=""/>
            </form>
          </div>
        </div>
      </header>
    </div>
  </div>
  
  <!--  FIN HEADER SEARCH -->

<div id="tituloMicroSitioCienciasQuimicas" class="header-azul" style="background:<?php echo $tono; ?>">
    	<div class="container">
    		<div class="titleUnidadMedica ">
    			Bienvenid@s: <span><?php echo $tituloo; ?><span>
    		</div>
    	</div>
    </div>

  <!-- SECCION RESPONSIVE
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <!-- HEADER SEARCH RESPONSIVE -->

  <div class="container " id="responsiveMenuHeader">
    <div class="row">
      <header>

        <div class="six columns logo">
          <img src="images/nuevosArtes/header/logo.png" alt="La Salle">
        </div>

        <div class="six columns searchBar backgroundResponsive">
          <div class="contenedorIdioma">
            <img class="" alt="En" src="images/nuevosArtes/idioma_botones/button_english_selected.png" usemap="#en">
            <map name="en" id="en">
              <area alt="" title="" href="#" shape="poly" coords="10,1,17,9,2,30,33,31,41,7,38,3,13,1" />
            </map>
            <img class="" alt="Es" src="images/nuevosArtes/idioma_botones/button_spanish_Selected.png" usemap="#es">
            <map name="es" id="es">
              <area alt="" title="" href="#" shape="poly" coords="10,1,17,9,2,30,33,31,41,7,38,3,13,1" />
            </map>
          </div>
            
          <div class="contenedorFormulario">
            <form class="formSearch" method="get" action="busqueda.php">
              <input class="search" type="text" name="buscar" placeholder="Ingresa tu palabra..." />
              <input class="submit" type="submit" value="" />
            </form>
          </div>

          <div class="menuPrincipal">
            <button id="open_menu_principal" class="btn_menu_principal_responsive"></button>
           </div>
         </div>

      </header>
    </div>
  </div>


  <!-- FIN SECCION RESPONSIVE
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <!-- FIN HEADER SEARCH RESPONSIVE -->


  <!-- SECCION RESPONSIVE
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <!-- MENU PRINCIPAL -->

  <div class="container">
    <div class="opc_btn_menu_responsive" id="opc_menu_principal">
      
      <div class="row">
        <button id="botonUnoResponsive">Universidad La Salle</button>
        <hr></hr>
      </div>
      
      <div class="row">
        <button id="botonDosResponsive">Oferta Educativa</button>
        <hr></hr>
      </div>
      
      <div class="row">
        <button id="botonTresResponsive" >Vida en Estudiantil</button>
        <hr></hr>
      </div>
      
      <div class="row">
        <button id="botonCuatroResponsive">Servicios</button>
        <hr></hr>
      </div>
      
      <div class="row">
        <button id="botonCincoResponsive">Vinculación Empresarial</button>
        <hr></hr>
      </div>
      
      <div class="row">
        <button id="botonSeisResponsive">Acción Social</button>
        <hr></hr>
      </div>
      
      <div class="row">
        <button id="botonSieteResponsive">Investigación</button>
        <hr></hr>
      </div>
      
      <div class="row">
        <button id="botonOchoResponsive">Contacto</button>
        <hr></hr>
      </div>

    </div>
  </div>

  <!-- FIN SECCION RESPONSIVE
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <!-- FIN MENU PRINCIPAL -->



  <!--  SECCION RESPONSIVE
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <!--  MENU PERFILES Y BANNER -->



  <div class="container bannerPrincipalResponsive" id="bannerPrincipalResponsive">
    <div class="row">
      <img src="images/nuevosArtes/bannerPrincipal/home_banner.png">
    </div>
  </div>
    
  <div id="perfilesResponsive" class="container perfilesResponsive">
    
    <div class="row">
      <button id="Aspirante"class="unoDerecha" onclick="ItemsAspirante(1);">Aspirante</button>
      <button id="Estudiante" onclick="ItemsAspirante(2);">Estudiante</button>
      <button id="Docente" class="dosIzquierda" onclick="ItemsAspirante(3);">Docente</button>
    </div>
      
    <div class="row">
      <button id="Administrativo" class="unoDerecha" onclick="ItemsAspirante(4);">Administrativo</button>
      <button id="Egresado" onclick="ItemsAspirante(5);">Egresado</button>
      <button id="Padres" class="dosIzquierda" onclick="ItemsAspirante(6);">Padre de Familia</button>
    </div>
      
    <div class="row">
      <button id="International" onclick="ItemsAspirante(7);">International Student</button>
    </div>
  </div>


  <!--  FIN SECCION RESPONSIVE
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <!--  FIN MENU PERFILES Y BANNER -->



  <!--  SECCION VIDEO Y MENU PRINCIPAL
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->

  <div class="bannerPrincipals marginEntreSecciones">
    <?php
	// imagen principal
		$query = mysql_query("SELECT video FROM video where id=1");
		$results = mysql_fetch_array($query); mysql_free_result($query);
		$video = $results['video'];
	?>
    <div id="video-container" >
      <img src="images/nuevosArtes/facultades/home_imagen.png" class="img-principal">
        
      <div class="container" style="margin-top: -33%;z-index: 2;">
        <div class="botonesMenu" id="menuNavegacionPrincipal">
          <div class="contenedorBotonesMenu"> 
            
			<?php
			function Menuprinci(){
				$sql= "select * from micrositios_menus where id_micrositio=".$_GET["micrositio"]." order by orden asc";
				$query=mysql_query($sql) or die(mysql_error());
				while ($resultados = mysql_fetch_assoc($query)){
					$id[] = $resultados["id"];
					$nombre[] = $resultados["nombre"];
					$link[] = $resultados["link"];
				}mysql_free_result($query);
				
				$query = mysql_query("select color from micrositios where id=".$_GET["micrositio"]."");
				$results = mysql_fetch_array($query); mysql_free_result($query);
				$color = $results['color'];
				$tono = null;
				if($color == 1){$tono = '#083a81';}
				if($color == 2){$tono = '#2a7fb8';}
				if($color == 3){$tono = '#818d8d';}
				if($color == 4){$tono = '#7537c3';}
				if($color == 5){$tono = '#eb740f';}
				if($color == 6){$tono = '#292929';}
				if($color == 7){$tono = '#bf3a29';}
				if($color == 8){$tono = '#019a34';}
				
				$i=0; while($i < count(@$id)){
				$numero = null;
				if($i == 0){$numero = 'Uno';}
				if($i == 1){$numero = 'Dos';}
				if($i == 2){$numero = 'Tres';}
				if($i == 3){$numero = 'Cuatro';}
				if($i == 4){$numero = 'Cinco';}
				if($i == 5){$numero = 'Seis';}
				if($i == 6){$numero = 'Siete';}
				if($i == 7){$numero = 'Ocho';}
				if($i == 8){$numero = 'Nueve';}
				if($i == 9){$numero = 'Diez';}
				if($i == 10){$numero = 'Once';}
			?>
			
			
            <div class="botonPrueba boton<?php echo $numero; ?>Menu" onclick="primersubmenu(<?php echo $id[$i]; ?>);">
              <a href="<?php echo $link[$i]; ?>" class="btnEfecto skip" id="boton<?php echo $numero; ?>" style="text-align: center;width: 100%;">
                <span class="hover-bg reset" style="background:<?php echo $tono; ?>">
                  <span class="hover-text reset"></span>
                </span>
                <img src="images/nuevosArtes/menu/somos_la_salle.png" style="display:none">
				<span style="color: white;padding: 0px 8%"><?php echo $nombre[$i]; ?></span>
              </a>       
            </div>
  
			<?php $i++; }
			} Menuprinci(); ?>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!--  FIN SECCION VIDEO Y MENU PRINCIPAL
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->  

  


  <!--  SECCION MENU PERFILES
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->  

  <div id="menuPerfiles" class="container marginEntreSecciones segundo">
    <div class="row">
      <div class="menuPerfiles">
        <nav>
          <ul>
		  
            <?php MenusAspirante(); ?>
			
          </ul>
        </nav>
      </div>
    </div>
  </div>


  <!--  FIN SECCION MENU PERFILES
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->  



  <!-- PUNTERO DE PERFILES-->

  <div id="punteroPerfiles"></div>
  <!-- FIN PUNTERO DE PERFILES-->




  <!--  ASPIRANTES
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->  

  <div class="container marginInteriorBottom" id="aspirantes">
    <div class="row">
      <div class="contenedorCuadrosEstudiante">

        <div id="effect-1" class="effects clearfix contenedor-aspirante">
		
		</div>
      
      </div>
    </div>
  </div>


  <!--  FIN SECCION ASPIRANTES
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->  


  <!--  SECCION RESPONSIVE
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <!--  SECCION ASPIRANTES RESPONSIVE -->

  <div id="perfilAspiranteResponsive" class="investigacion_reponsive_container">
    <div class="container">
      <div class="row">

        <div class="investigacion_cuadro_responsive">   
          <p class="azulFuerte">ADMISIONES</p>
          <a href="#"><img src="images/nuevosArtes/images_responsive/ir.png"></a>
        </div>  

        <div class="investigacion_cuadro_responsive">
          <p class="azulClaro"><span style="font-size:0.8em;">Examen de <span> <span class="azulFuerte">ADMISION</span></p>
          <a href="#"><img src="images/nuevosArtes/images_responsive/ir.png"></a>
        </div>

      </div>
        
      <div class="row">

        <div class="investigacion_cuadro_responsive">
          <p class="azulFuerte">REQUISITOS</p>
          <a href="#"><img src="images/nuevosArtes/images_responsive/ir.png"></a>
        </div>

        <div class="investigacion_cuadro_responsive">
          <p class="azulFuerte">ADMITIDOS</p>
          <a href="#"><img src="images/nuevosArtes/images_responsive/ir.png"></a>
        </div>

      </div>
    </div> 
  </div>



  <!--  FIN SECCION STUDENT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <!--  FIN SECCION STUDENT RESPONSIVE -->

  

  <!--  SECCION UBICACION
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->

  <div id="MenuUbicacionPrincipal"></div>


  <div id="ubicacion" class="container">
    <div class="row">

      <div class="menuUbicacion">
        <nav>
          <ul>
            <li>
              <span class="colorGrisOscuro">Estas en:</span> Universidad La Salle, Ciudad de México > Universidad La Salle <span class="colorGrisOscuro">> <div id="changeTextTitulo"> Home</div></span>
            </li>
          </ul>
        </nav>
      </div> 

    </div>
  </div>

  <!--  FIN SECCION UBICACION
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->






  
  <!--  SECCION UNIVERSIDAD LA SALLE
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->

  <div id="universidadShow"> 
    <div class="container marginEntreSecciones">
    

      <div class="row">
        
        <div class="headerSubMenuUniversidad">
        
          <div class="two columns tituloUniversidad">
            <img src="images/nuevosArtes/btn_seccion/somos_la_salle.png"> 
          </div>
            
          <div class="ten columns subMenuUniversidad">
			
            <?php submenus(1); ?>
            
          </div>


            <div class="ten columns submenu" id="subsubmenus-menu1">
               <!-- <div class="cuadrosSubmenu">Contenido Uno</div> -->
            </div>
             

        </div>

         


        <div class="contenedorTituloUniversidad">
          <div class="tituloContenedorUniversidad">
            <div class="textoTituloUniversidad">
              <p id="subtitulo-subsubmenu-menu1"></p>
            </div>
          </div>
        </div>


      </div>

    </div>

    <div class="ocultarContenidoSubMenu">

        

      <div class="contenedorImagenTextoAccionSocial-" style="background: url(images/nuevosArtes/banner/auditorioimg.jpg)no-repeat;background-attachment: fixed;">
        <div class="container" id="componentes-menu1">
          <div class="contenedorDiagonalUniversidad2" id="scrollImagen2">
            <p class="descripcionTituloUniversidad- ws scroll-box-150">Vivir en la Republica es una experiencia compartida. Por este motivo, el respeto y el orden con indispensables. La consideracion hacia las personas y las instalaciones, la empatia y el consumo responsable de los recursos, conforman la base para poder garantizar un espacio agradable y familiar que tiene como eje principal el estudio y el desarrollo personal de los residentes.</p>

            <!--<div class="parrafoUniversidad ws scroll-box-300">
              <p>Las residentes pueden disfrutar de espacios creados especialmente para ello,pensando en sus necesidades y en su comodidad.</p>
              <p>Todas las instalaciones de la residencia has sido creadas pensando en la comodidad y las necesidades de sus residentes.</p>
              <h6>INSTALACIONES</h6>
              <p>La residencia es una casa que fue acondicionada para poder ofrecer un lugar de descanso y estudio para aquellos estudiantes que deseen estar cerca de las instalaciones de la Univeridad.</p>
              <p>Cuenta con una sala compartida para todos los residentes con televísor, un comedor con capacidad de 11 personas, área para la preparacion y conserva de alimentos, cuarto de servicio, terraza y un cuarto destinado para el personal de Limpieza y vigilancia que estará presente las 24 hrs del día.</p>
            </div>-->

          </div>
		  <!-- Pines -->
		  <div class="pines-mapa-mx" style="display:none">
			<div class="item-mapa-chihuahua">
				<img src="images/nuevosArtes/servicio/pin.png" class="chihuahua no-hover">
				<div class="texto-pin-chihuahua">
					<img src="images/nuevosArtes/servicio/pin2.png" class="img-texto-pin">
					<span>La Salle Campus<br>Chihuahua</span>
				</div>
			</div>
			<div class="item-mapa-noroeste">
				<img src="images/nuevosArtes/servicio/pin.png" class="noroeste no-hover">
				<div class="texto-pin-noroeste">
					<img src="images/nuevosArtes/servicio/pin2.png" class="img-texto-pin">
					<span>La Salle Campus<br>Noroeste</span>
				</div>
			</div>
			<div class="item-mapa-saltillo">
				<img src="images/nuevosArtes/servicio/pin.png" class="saltillo no-hover">
				<div class="texto-pin-saltillo">
					<img src="images/nuevosArtes/servicio/pin2.png" class="img-texto-pin">
					<span>La Salle Campus<br>Saltillo</span>
				</div>
			</div>
			<div class="item-mapa-laguna">
				<img src="images/nuevosArtes/servicio/pin.png" class="laguna no-hover">
				<div class="texto-pin-laguna">
					<img src="images/nuevosArtes/servicio/pin2.png" class="img-texto-pin">
					<span>La Salle Campus<br>Laguna</span>
				</div>
			</div>
			<div class="item-mapa-monterrey">
				<img src="images/nuevosArtes/servicio/pin.png" class="monterrey no-hover">
				<div class="texto-pin-monterrey">
					<img src="images/nuevosArtes/servicio/pin2.png" class="img-texto-pin">
					<span>La Salle Campus<br>Monterrey</span>
				</div>
			</div>
			<div class="item-mapa-victoria">
				<img src="images/nuevosArtes/servicio/pin.png" class="victoria no-hover">
				<div class="texto-pin-victoria">
					<img src="images/nuevosArtes/servicio/pin2.png" class="img-texto-pin">
					<span>La Salle Campus<br>Victoria</span>
				</div>
			</div>
			<div class="item-mapa-bajio">
				<img src="images/nuevosArtes/servicio/pin.png" class="bajio no-hover">
				<div class="texto-pin-bajio">
					<img src="images/nuevosArtes/servicio/pin2.png" class="img-texto-pin">
					<span>La Salle Campus<br>bajio</span>
				</div>
			</div>
			<div class="item-mapa-morelia">
				<img src="images/nuevosArtes/servicio/pin.png" class="morelia no-hover">
				<div class="texto-pin-morelia">
					<img src="images/nuevosArtes/servicio/pin2.png" class="img-texto-pin">
					<span>La Salle Campus<br>morelia</span>
				</div>
			</div>
			<div class="item-mapa-pachuca">
				<img src="images/nuevosArtes/servicio/pin.png" class="pachuca no-hover">
				<div class="texto-pin-pachuca">
					<img src="images/nuevosArtes/servicio/pin2.png" class="img-texto-pin">
					<span>La Salle Campus<br>Pachuca</span>
				</div>
			</div>
			<div class="item-mapa-neza">
				<img src="images/nuevosArtes/servicio/pin.png" class="neza no-hover">
				<div class="texto-pin-neza">
					<img src="images/nuevosArtes/servicio/pin2.png" class="img-texto-pin">
					<span>La Salle Campus<br>Nezahualcoyotl</span>
				</div>
			</div>
			<div class="item-mapa-puebla">
				<img src="images/nuevosArtes/servicio/pin.png" class="puebla no-hover">
				<div class="texto-pin-puebla">
					<img src="images/nuevosArtes/servicio/pin2.png" class="img-texto-pin">
					<span>La Salle Campus<br>Puebla</span>
				</div>
			</div>
			<div class="item-mapa-cuernavaca">
				<img src="images/nuevosArtes/servicio/pin.png" class="cuernavaca no-hover">
				<div class="texto-pin-cuernavaca">
					<img src="images/nuevosArtes/servicio/pin2.png" class="img-texto-pin">
					<span>La Salle Campus<br>Cuernavaca</span>
				</div>
			</div>
			<div class="item-mapa-oaxaca">
				<img src="images/nuevosArtes/servicio/pin.png" class="oaxaca no-hover">
				<div class="texto-pin-oaxaca">
					<img src="images/nuevosArtes/servicio/pin2.png" class="img-texto-pin">
					<span>La Salle Campus<br>Oaxaca</span>
				</div>
			</div>
			<div class="item-mapa-cancun">
				<img src="images/nuevosArtes/servicio/pin.png" class="cancun no-hover">
				<div class="texto-pin-cancun">
					<img src="images/nuevosArtes/servicio/pin2.png" class="img-texto-pin">
					<span>La Salle Campus<br>Cancún</span>
				</div>
			</div>
		  </div>
		  <div class="pines-mapa-mundo" style="display:none">
			<div class="item-mapa-mexico">
				<img src="images/nuevosArtes/servicio/pin.png" class="mexico no-hover">
				<div class="texto-pin-mexico">
					<img src="images/nuevosArtes/servicio/pin2.png" class="img-texto-pin">
					<span>Universidad La Salle<br>Cd. México</span>
				</div>
			</div>
			<div class="item-mapa-peru">
				<img src="images/nuevosArtes/servicio/pin.png" class="peru no-hover">
				<div class="texto-pin-peru">
					<img src="images/nuevosArtes/servicio/pin2.png" class="img-texto-pin">
					<span>Universidad La Salle<br>Cd. México</span>
				</div>
			</div>
			<div class="item-mapa-brasil">
				<img src="images/nuevosArtes/servicio/pin.png" class="brasil no-hover">
				<div class="texto-pin-brasil">
					<img src="images/nuevosArtes/servicio/pin2.png" class="img-texto-pin">
					<span>Universidad La Salle<br>Cd. México</span>
				</div>
			</div>
			<div class="item-mapa-espana">
				<img src="images/nuevosArtes/servicio/pin.png" class="espana no-hover">
				<div class="texto-pin-espana">
					<img src="images/nuevosArtes/servicio/pin2.png" class="img-texto-pin">
					<span>Universidad La Salle<br>Cd. México</span>
				</div>
			</div>
			<div class="item-mapa-turquia">
				<img src="images/nuevosArtes/servicio/pin.png" class="turquia no-hover">
				<div class="texto-pin-turquia">
					<img src="images/nuevosArtes/servicio/pin2.png" class="img-texto-pin">
					<span>Universidad La Salle<br>Cd. México</span>
				</div>
			</div>
			<div class="item-mapa-africa1">
				<img src="images/nuevosArtes/servicio/pin.png" class="africa1 no-hover">
				<div class="texto-pin-africa1">
					<img src="images/nuevosArtes/servicio/pin2.png" class="img-texto-pin">
					<span>Universidad La Salle<br>Cd. México</span>
				</div>
			</div>
			<div class="item-mapa-africa2">
				<img src="images/nuevosArtes/servicio/pin.png" class="africa2 no-hover">
				<div class="texto-pin-africa2">
					<img src="images/nuevosArtes/servicio/pin2.png" class="img-texto-pin">
					<span>Universidad La Salle<br>Cd. México</span>
				</div>
			</div>
			<div class="item-mapa-india">
				<img src="images/nuevosArtes/servicio/pin.png" class="india no-hover">
				<div class="texto-pin-india">
					<img src="images/nuevosArtes/servicio/pin2.png" class="img-texto-pin">
					<span>Universidad La Salle<br>Cd. México</span>
				</div>
			</div>
			<div class="item-mapa-indonesia">
				<img src="images/nuevosArtes/servicio/pin.png" class="indonesia no-hover">
				<div class="texto-pin-indonesia">
					<img src="images/nuevosArtes/servicio/pin2.png" class="img-texto-pin">
					<span>Universidad La Salle<br>Cd. México</span>
				</div>
			</div>
		  </div>
		<!-- Mosaico para acreditaciones -->
		<div class="acre-mosaico" style="display:none">
			<a href="#"><img alt="" src="http://www.lasalle.mx/wp-content/themes/lasalle/images/acre1h.png" data-hover="http://www.lasalle.mx/wp-content/themes/lasalle/images/acre1h.png"></a>
			<a href="#"><img alt="" src="http://www.lasalle.mx/wp-content/themes/lasalle/images/acre2h.png" data-hover="http://www.lasalle.mx/wp-content/themes/lasalle/images/acre2h.png"></a>
			<a href="#"><img alt="" src="http://www.lasalle.mx/wp-content/themes/lasalle/images/acre3h.png" data-hover="http://www.lasalle.mx/wp-content/themes/lasalle/images/acre3h.png"></a>
			<a href="#"><img alt="" src="http://www.lasalle.mx/wp-content/themes/lasalle/images/acre4h.png" data-hover="http://www.lasalle.mx/wp-content/themes/lasalle/images/acre4h.png"></a>
			<a href="#"><img alt="" src="http://www.lasalle.mx/wp-content/themes/lasalle/images/acre5h.png" data-hover="http://www.lasalle.mx/wp-content/themes/lasalle/images/acre5h.png"></a>
			<a href="#"><img alt="" src="http://www.lasalle.mx/wp-content/themes/lasalle/images/acre6h.png" data-hover="http://www.lasalle.mx/wp-content/themes/lasalle/images/acre6h.png"></a>
			<a href="#"><img alt="" src="http://www.lasalle.mx/wp-content/themes/lasalle/images/acre7h.png" data-hover="http://www.lasalle.mx/wp-content/themes/lasalle/images/acre8h.png"></a>
			<a href="#"><img alt="" src="http://www.lasalle.mx/wp-content/themes/lasalle/images/acre8h.png" data-hover="http://www.lasalle.mx/wp-content/themes/lasalle/images/acre8h.png"></a>
			<a href="#"><img alt="" src="http://www.lasalle.mx/wp-content/themes/lasalle/images/acre9h.png" data-hover="http://www.lasalle.mx/wp-content/themes/lasalle/images/acre9h.png"></a>
			<a href="#"><img alt="" src="http://www.lasalle.mx/wp-content/themes/lasalle/images/acre10h.png" data-hover="http://www.lasalle.mx/wp-content/themes/lasalle/images/acre10h.png"></a>
			<a href="#"><img alt="" src="http://www.lasalle.mx/wp-content/themes/lasalle/images/acre11h.png" data-hover="http://www.lasalle.mx/wp-content/themes/lasalle/images/acre11h.png"></a>
			<a href="#"><img alt="" src="http://www.lasalle.mx/wp-content/themes/lasalle/images/acre12h.png" data-hover="http://www.lasalle.mx/wp-content/themes/lasalle/images/acre12h.png"></a>
			<a href="#"><img alt="" src="http://www.lasalle.mx/wp-content/themes/lasalle/images/acre13h.png" data-hover="http://www.lasalle.mx/wp-content/themes/lasalle/images/acre13h.png"></a>
			<a href="#"><img alt="" src="http://www.lasalle.mx/wp-content/themes/lasalle/images/acre14h.png" data-hover="http://www.lasalle.mx/wp-content/themes/lasalle/images/acre14h.png"></a>
			<a href="#"><img alt="" src="http://www.lasalle.mx/wp-content/themes/lasalle/images/acre15h.png" data-hover="http://www.lasalle.mx/wp-content/themes/lasalle/images/acre15h.png"></a>
		</div>
			<table class="normal_table ta_center excel" id="tabla-acreditaciones" cellspacing="0" style="display:none">
				<thead>
					<tr>
						<th>Programa</th>
						<th>Organismo</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Administración</td>
						<td>CACECA</td>
					</tr>
					<tr>
						<td>Arquitectura</td>
						<td>ANPADEH</td>
					</tr>
					<tr>
						<td>Ciencias de la Comunicación</td>
						<td>CONAC</td>
					</tr>
					<tr>
						<td>Ciencias de la Educación</td>
						<td>CEPPE</td>
					</tr>
					<tr>
						<td>Ciencias Religiosas</td>
						<td>COAPEHUM</td>
					</tr>
					<tr>
						<td>Ciencias Religiosas (No escolarizado)</td>
						<td>COAPEHUM</td>
					</tr>
					<tr>
						<td>Comercio y Negocios Internacionales</td>
						<td>CACECA</td>
					</tr>
					<tr>
						<td>Contaduría y Finanzas</td>
						<td>CACECA</td>
					</tr>
					<tr>
						<td>Derecho</td>
						<td>CONAED</td>
					</tr>
					<tr>
						<td>Diseño Gráfico</td>
						<td>COMAPROD</td>
					</tr>
					<tr>
						<td>Educación Primaria</td>
						<td>CIEES</td>
					</tr>
					<tr>
						<td>Filosofía</td>
						<td>COAPEHUM</td>
					</tr>
					<tr>
						<td>Filosofía (No escolarizado)</td>
						<td>COAPEHUM</td>
					</tr>
					<tr>
						<td>Gestión de Negocios y Tecnologías de Información</td>
						<td>CACECA</td>
					</tr>
					<tr>
						<td>Ingeniería Ambiental</td>
						<td>CACEI</td>
					</tr>
					<tr>
						<td>Ingeniería Biomédica</td>
						<td>CACEI</td>
					</tr>
					<tr>
						<td>Ingeniería Cibernética y Sistemas Computacionales</td>
						<td>CACEI</td>
					</tr>
					<tr>
						<td>Ingeniería Civil</td>
						<td>CACEI</td>
					</tr>
					<tr>
						<td>Ingeniería Electrónica y Comunicaciones</td>
						<td>CACEI</td>
					</tr>
					<tr>
						<td>Ingeniería Industrial</td>
						<td>CACEI</td>
					</tr>
					<tr>
						<td>Ingeniería Mecánica</td>
						<td>CACEI</td>
					</tr>
					<tr>
						<td>Ingeniería Mecatrónica</td>
						<td>CACEI</td>
					</tr>
					<tr>
						<td>Ingeniería Química</td>
						<td>CACEI</td>
					</tr>
					<tr>
						<td>Médico – Cirujano</td>
						<td>COMAEM</td>
					</tr>
					<tr>
						<td>Psicología</td>
						<td>CNEIP</td>
					</tr>
					<tr>
						<td>Químico Farmacéutico Biólogo</td>
						<td>COMAEF</td>
					</tr>
					<tr>
						<td>Relaciones Internacionales</td>
						<td>ACCECISO</td>
					</tr>
					<tr>
						<td>Mercadotecnia </td>
						<td>en proceso </td>
					</tr>
					<tr>
						<td>Actuaría </td>
						<td>en proceso </td>
					</tr>
					<tr>
						<td>Química en Alimentos </td>
						<td>en proceso </td>
					</tr>
					<tr>
						<td>Educación Preescolar</td>
						<td>no aplica</td>
					</tr>
				</tbody>
			</table>
        </div>
		<!-- Contenedor de cuadros azules en el menu de red la salle -->
		<div class="container" style="display:none" id="azules-red">
			<div class="row row-mx">
				<a class="col-md-3 azul-col" href="http://www.ulsa.edu.mx">
					<h3>Universidad La Salle</h3>
					<h4>Ciudad de México, Distrito Federal</h4>
					<h5>www.ulsa.edu.mx</h5>
				</a>
				<a class="col-md-3 azul-col" href="http://www.lasallecancun.edu.mx">
					<h3>Universidad La Salle Cancún</h3>
					<h4>Cancún, Quintana Roo</h4>
					<h5>www.lasallecancun.edu.mx</h5>
				</a>
				<a class="col-md-3 azul-col" href="http://www.ulsac.edu.mx">
					<h3>Universidad La Salle Cuernavaca</h3>
					<h4>Cuernavaca, Morelos</h4>
					<h5>www.ulsac.edu.mx</h5>
				</a>
				<a class="col-md-3 azul-col" href="http://www.ceslas.mx">
					<h3>Centro de Estudios Superiores La Salle</h3>
					<h4>Monterrey, Nuevo León</h4>
					<h5>www.ceslas.mx</h5>
				</a>
				<a class="col-md-3 azul-col" href="http://ulsaneza.edu.mx">
					<h3>Universidad La Salle Nezahualcóyotl</h3>
					<h4>Nezahualcóyotl, Edo. de México</h4>
					<h5>ulsaneza.edu.mx</h5>
				</a>
				<a class="col-md-3 azul-col" href="http://www.lasallep.edu.mx">
					<h3>Universidad La Salle Pachuca</h3>
					<h4>Pachuca, Hidalgo</h4>
					<h5>www.lasallep.edu.mx</h5>
				</a>
				<a class="col-md-3 azul-col" href="http://www.ulsaoaxaca.edu.mx">
					<h3>Universidad La Salle Oaxaca</h3>
					<h4>Santa Cruz Xoxocotlán, Oaxaca</h4>
					<h5>www.ulsaoaxaca.edu.mx</h5>
				</a>
				<a class="col-md-3 azul-col" href="http://www.ulsavictoria.edu.mx">
					<h3>Universidad La Salle Victoria</h3>
					<h4>Ciudad Victoria, Tamaulipas</h4>
					<h5>www.ulsavictoria.edu.mx</h5>
				</a>
				<a class="col-md-3 azul-col" href="http://www.delasalle.edu.mx">
					<h3>Universidad De La Salle Bajío</h3>
					<h4>León, Guanajuato</h4>
					<h5>www.delasalle.edu.mx</h5>
				</a>
				<a class="col-md-3 azul-col" href="http://www.ulsachihuahua.edu.mx">
					<h3>Universidad La Salle Chihuahua</h3>
					<h4>Chihuahua, Chuhuahua</h4>
					<h5>www.ulsachihuahua.edu.mx</h5>
				</a>
				<a class="col-md-3 azul-col" href="http://www.ulsalaguna.edu.mx">
					<h3>Universidad La Salle Laguna</h3>
					<h4>Gomez Palacio, Durango</h4>
					<h5>www.ulsalaguna.edu.mx</h5>
				</a>
				<a class="col-md-3 azul-col" href="http://www.lasallemorelia.edu.mx">
					<h3>Universidad La Salle Morelia</h3>
					<h4>Morelia, Michoacán</h4>
					<h5>www.lasallemorelia.edu.mx</h5>
				</a>
				<a class="col-md-3 azul-col" href="http://www.ulsa-noroeste.edu.mx">
					<h3>Universidad La Salle Noroeste</h3>
					<h4>Ciudad Obregón, Sonora</h4>
					<h5>www.ulsa-noroeste.edu.mx</h5>
				</a>
				<a class="col-md-3 azul-col" href="http://www.universidadlasallebenavente.edu.mx">
					<h3>Universidad La Salle Benavente</h3>
					<h4>Puebla, Puebla</h4>
					<h5>www.universidadlasallebenavente.edu.mx</h5>
				</a>
				<a class="col-md-3 azul-col" href="http://www.ulsasaltillo.edu.mx">
					<h3>Universidad La Salle Saltillo</h3>
					<h4>Saltillo, Coahuila</h4>
					<h5>www.ulsasaltillo.edu.mx</h5>
				</a>
			</div>
			<div class="row row-mundo">
				<div class="two columns menuSecciones">  
					<ul class="ul-mundo" style="">
						<li><button class="seccion academicas tab" id="tab-todos" onclick="FiltroMapas('todos');">Todos</button></li>
						<li><button class="seccion academicas tab" id="tab-institucionales" onclick="FiltroMapas('north');">Norte América</button></li>
						<li><button class="seccion academicas tab" id="tab-institucionales" onclick="FiltroMapas('south');">Sudamérica</button></li>
						<li><button class="seccion academicas tab" id="tab-institucionales" onclick="FiltroMapas('asia');">Asia</button></li>
						<li><button class="seccion academicas tab" id="tab-institucionales" onclick="FiltroMapas('europe');">Europa</button></li>
					</ul>
				</div>
				<a class="col-md-3 azul-col asia" href="http://www.dlsau.edu.ph">
					<h3>De La Salle- Araneta University</h3>
					<h4>Malabon City, PHILIPPINES</h4>
					<h5>www.dlsau.edu.ph</h5>
				</a>
				<a class="col-md-3 azul-col asia" href="http://www.dlsc.edu.ph">
					<h3>De La Salle Canlubang</h3>
					<h4>Bian, PHILIPPINES</h4>
					<h5>www.dlsc.edu.ph</h5>
				</a>
				<a class="col-md-3 azul-col asia" href="http://www.dasma.dlsu.edu.ph">
					<h3>De La Salle Dasmarias</h3>
					<h4>Cavite, PHILIPPINES</h4>
					<h5>www.dasma.dlsu.edu.ph</h5>
				</a>
				<a class="col-md-3 azul-col asia" href="http://www.dlsl.edu.ph">
					<h3>De La Salle Lipa</h3>
					<h4>Lipa City, PHILIPPINES</h4>
					<h5>www.dlsl.edu.ph</h5>
				</a>
				<a class="col-md-3 azul-col asia" href="http://lasallel@mul.paknet.com.pk">
					<h3>La Salle High Secondary School</h3>
					<h4>Multan, PAKISTAN</h4>
					<h5>lasallel@mul.paknet.com.pk</h5>
				</a>
				<a class="col-md-3 azul-col asia" href="http://www.usls.edu.ph">
					<h3>University of St. La Salle</h3>
					<h4>Bacolod City, PHILIPPINES</h4>
					<h5>www.usls.edu.ph</h5>
				</a>
				<a class="col-md-3 azul-col asia" href="#">
					<h3>De La Salle-Professional Schools, Inc</h3>
					<h4>Manila, PHILIPPINES</h4>
					<h5></h5>
				</a>
				<a class="col-md-3 azul-col asia" href="http://www.dls-csb.edu.ph">
					<h3>De La Salle - College of Saint Benilde</h3>
					<h4>Manila, PHILIPPINES</h4>
					<h5>www.dls-csb.edu.ph</h5>
				</a>
				<a class="col-md-3 azul-col asia" href="#">
					<h3>De La Salle Catholic University</h3>
					<h4>Manado, INDONESIA</h4>
					<h5></h5>
				</a>
				<a class="col-md-3 azul-col asia" href="http://www.dlshsi.edu.ph">
					<h3>De La Salle Health Sciences Institute</h3>
					<h4>Cavite, PHILIPPINES</h4>
					<h5>www.dlshsi.edu.ph</h5>
				</a>
				<a class="col-md-3 azul-col asia" href="http://www.LSU.edu.ph">
					<h3>Immaculate Conception College-La Salle</h3>
					<h4>Ozamis City, PHILIPPINES</h4>
					<h5>www.LSU.edu.ph</h5>
				</a>
				<a class="col-md-3 azul-col asia" href="http://www.delasalle.phlsca">
					<h3>La Salle College Antipolo</h3>
					<h4>Antipolo City, PHILIPPINES</h4>
					<h5>www.delasalle.phlsca</h5>
				</a>
				<a class="col-md-3 azul-col asia" href="http://www.dlsu.edu.ph">
					<h3>De La Salle University</h3>
					<h4>Manila, PHILIPPINES</h4>
					<h5>www.dlsu.edu.ph</h5>
				</a>
				<a class="col-md-3 azul-col north" href="http://www.bethlehem.edu">
					<h3>Bethlehem University</h3>
					<h4>Bethlehem, PALESTINE</h4>
					<h5>www.bethlehem.edu</h5>
				</a>
				<a class="col-md-3 azul-col north" href="http://www.cbu.edu">
					<h3>Christian Brothers University</h3>
					<h4>Memphis, USA</h4>
					<h5>www.cbu.edu</h5>
				</a>
				
				<a class="col-md-3 azul-col north" href="http://www.lasalle.edu">
					<h3>La Salle University</h3>
					<h4>Philadelphia, USA</h4>
					<h5>www.lasalle.edu</h5>
				</a>
				<a class="col-md-3 azul-col north" href="http://www.manhattan.edu">
					<h3>Manhattan Collage</h3>
					<h4>New York, USA</h4>
					<h5>www.manhattan.edu</h5>
				</a>
				<a class="col-md-3 azul-col north" href="http://www.smumn.edu">
					<h3>Saint Mary's University of Minnesota</h3>
					<h4>Minnesota, USA</h4>
					<h5>www.smumn.edu</h5>
				</a>
				<a class="col-md-3 azul-col north" href="http://www.cbu.edu">
					<h3>Christ the Teacher Institute for Education</h3>
					<h4>Nairobi, KENYA</h4>
					<h5>www.tangaza.org</h5>
				</a>
				<a class="col-md-3 azul-col north" href="http://www.csf.educsf">
					<h3>College of Santa Fe</h3>
					<h4>Santa fe, USA</h4>
					<h5>www.csf.educsf</h5>
				</a>
				<a class="col-md-3 azul-col north" href="http://www.lewisu.edu">
					<h3>Lewis University</h3>
					<h4>Romeoville, IL, USA</h4>
					<h5>www.lewisu.edu</h5>
				</a>
				<a class="col-md-3 azul-col north" href="http://www.cbu.edu">
					<h3>Saint Mary's College of California</h3>
					<h4>California, USA</h4>
					<h5>www.stmarys-ca.edu</h5>
				</a>
				<a class="col-md-3 azul-col north" href="http://www.smumn.edu">
					<h3>Saint Mary's University of Minnesota</h3>
					<h4>Minnesota, USA</h4>
					<h5>www.smumn.edu</h5>
				</a>
				
				<a class="col-md-3 azul-col south" href="http://www.unilasalle.edu.br">
					<h3>Centro Universitário La Salle – UNILASALLE</h3>
					<h4>Canoas –RS-, BRASIL</h4>
					<h5>www.unilasalle.edu.br</h5>
				</a>
				<a class="col-md-3 azul-col south" href="http://www.delasalle.edu.co">
					<h3>Escuela Normal Superior San Pio X</h3>
					<h4>Choco, COLOMBIA</h4>
					<h5>www.delasalle.edu.co</h5>
				</a>
				<a class="col-md-3 azul-col south" href="http://www.fundacionlasalle.org.ve">
					<h3>Fundación La Salle de Ciencias Naturales</h3>
					<h4>Caracas, VENEZUELA</h4>
					<h5>www.fundacionlasalle.org.ve</h5>
				</a>
				<a class="col-md-3 azul-col south" href="http://www.lasalle.edu.co">
					<h3>La Salle – Bogotá</h3>
					<h4>Bogotá, COLOMBIA.</h4>
					<h5>www.lasalle.edu.co</h5>
				</a>
				<a class="col-md-3 azul-col south" href="http://www.ialu.net">
					<h3>Instituto La Crujía</h3>
					<h4>Buenos Aires, ARGENTINA</h4>
					<h5>www.ialu.net</h5>
				</a>
				<a class="col-md-3 azul-col south" href="http://www.unilasalle.edu.br">
					<h3>Instituto La Salle – Rosario</h3>
					<h4>Santa Fe, ARGENTINA</h4>
					<h5>www.lasallerosario.org.ar</h5>
				</a>
				
				<a class="col-md-3 azul-col south" href="http://www.lasalle.org">
					<h3>Instituto Superior Pedagógico La Salle</h3>
					<h4>Urubamba, Cuzco, PERU</h4>
					<h5>www.lasalle.org</h5>
				</a>
				<a class="col-md-3 azul-col south" href="#">
					<h3>Instituto Técnico Central</h3>
					<h4>Bogotá, COLOMBIA</h4>
					<h5></h5>
				</a>
				<a class="col-md-3 azul-col south" href="http://www.lasallerj.org">
					<h3>Institutos Superiores La Salle (UNILASALLE-RJ)</h3>
					<h4>Niteroi - Río de Janeiro, BRASIL</h4>
					<h5>www.lasallerj.org</h5>
				</a>
				<a class="col-md-3 azul-col south" href="http://www.lasallista.edu.co">
					<h3>Corporación Universitaria Lasallista</h3>
					<h4>Caldas, Antioquia, COLOMBIA</h4>
					<h5>www.lasallista.edu.co</h5>
				</a>
				<a class="col-md-3 azul-col south" href="http://www.ialu.net">
					<h3>Escuela Superior de Educación Integral Rural</h3>
					<h4>GUATEMALA</h4>
					<h5>www.ialu.net</h5>
				</a>
				<a class="col-md-3 azul-col south" href="http://www.ialu.net">
					<h3>ICCRE Instituto Centroamericano de Ciencias Religiosas</h3>
					<h4>Guatemala, GUATEMALA</h4>
					<h5>www.ialu.net</h5>
				</a>
				<a class="col-md-3 azul-col south" href="http://www.ialu.net">
					<h3>Instituto Catecheticum</h3>
					<h4>Santiago, CHILE</h4>
					<h5>www.ialu.net</h5>
				</a>
				<a class="col-md-3 azul-col south" href="http://www.ilasalle.com.ar">
					<h3>Instituto La Salle – Florida</h3>
					<h4>Florida, Buenos Aires, ARGENTINA</h4>
					<h5>www.ilasalle.com.ar</h5>
				</a>
				<a class="col-md-3 azul-col south" href="http://www.unilasalle.edu.br">
					<h3>Instituto Lasallano de Estudios Superiores (ILES) de Buenos Aires</h3>
					<h4>Buenos Aires, ARGENTINA</h4>
					<h5>www.delasallesuperior.edu.ar</h5>
				</a>
				<a class="col-md-3 azul-col south" href="http://www.unilasalle.edu.br">
					<h3>Instituto Superior Pedagógico</h3>
					<h4>Requeña, PERU</h4>
					<h5>www.lasalle.org</h5>
				</a>
				<a class="col-md-3 azul-col south" href="http://www.unilasalle.edu.br">
					<h3>Universidad Tecnológica La Salle, León</h3>
					<h4>León, NICARAGUA</h4>
					<h5>www.ulsa.edu.ni</h5>
				</a>
				<a class="col-md-3 azul-col south" href="http://www.ulasalle.ac.cr">
					<h3>Universidad De La Salle – San Jose</h3>
					<h4>San Jose, COSTA RICA</h4>
					<h5>www.ulasalle.ac.cr</h5>
				</a>
				
				<a class="col-md-3 azul-col south" href="http://www.ulasalle.ac.cr">
					<h3>Universidad De La Salle – San Jose</h3>
					<h4>San Jose, COSTA RICA</h4>
					<h5>www.ulasalle.ac.cr</h5>
				</a>
				<a class="col-md-3 azul-col south" href="http://www.ulasalle.ac.cr">
					<h3>Universidad De La Salle – San Jose</h3>
					<h4>San Jose, COSTA RICA</h4>
					<h5>www.ulasalle.ac.cr</h5>
				</a>
				<a class="col-md-3 azul-col south" href="http://www.ulasalle.ac.cr">
					<h3>Universidad De La Salle – San Jose</h3>
					<h4>San Jose, COSTA RICA</h4>
					<h5>www.ulasalle.ac.cr</h5>
				</a>
				<a class="col-md-3 azul-col south" href="http://www.ulasalle.ac.cr">
					<h3>Universidad De La Salle – San Jose</h3>
					<h4>San Jose, COSTA RICA</h4>
					<h5>www.ulasalle.ac.cr</h5>
				</a>
				<a class="col-md-3 azul-col south" href="http://www.ulasalle.ac.cr">
					<h3>Universidad De La Salle – San Jose</h3>
					<h4>San Jose, COSTA RICA</h4>
					<h5>www.ulasalle.ac.cr</h5>
				</a>
				
				<a class="col-md-3 azul-col europe" href="http://www.lasalle.esbenincelaflecentre.htm">
					<h3>Centre Lasallien African CELAF</h3>
					<h4>Abidjan - Riviera, AFRICA</h4>
					<h5>www.lasalle.esbenincelaflecentre.htm</h5>
				</a>
				<a class="col-md-3 azul-col europe" href="http://www.salleurl.edu">
					<h3>Enginyeria i Arquitectura La Salle</h3>
					<h4>Barcelona, ESPAÑAA</h4>
					<h5>www.salleurl.edu</h5>
				</a>
				<a class="col-md-3 azul-col europe" href="http://www.lasalle-beauvais.fr">
					<h3>Institut Polytechnique LaSalle Beauvais</h3>
					<h4>Beauvais, France</h4>
					<h5>www.lasalle-beauvais.fr</h5>
				</a>
				<a class="col-md-3 azul-col europe" href="http://www.st-luc-brussels-archi.be">
					<h3>Institut Suprieur D'Architecture St. Luc de Bruxelles</h3>
					<h4>Bruxelles, Belgique</h4>
					<h5>www.st-luc-brussels-archi.be</h5>
				</a>
				<a class="col-md-3 azul-col europe" href="http://www.isaip.org">
					<h3>ISAIP ESAIP</h3>
					<h4>Angers, FRANCE</h4>
					<h5>www.isaip.org</h5>
				</a>
				<a class="col-md-3 azul-col europe" href="http://www.ecam.fr">
					<h3>Ecole Catholique D Arts et Mtiers (ECAM)</h3>
					<h4>Lyon, France</h4>
					<h5>www.ecam.fr</h5>
				</a>
				<a class="col-md-3 azul-col europe" href="http://www.lasalle.org">
					<h3>Fratelli Delle Scuole Cristiane - LA SALLE ROMA</h3>
					<h4>Roma, ITALIA</h4>
					<h5>www.lasalle.org</h5>
				</a>
				<a class="col-md-3 azul-col europe" href="http://www.ialu.netportal">
					<h3>Institut Reine Astrid De Mons (IRAM )</h3>
					<h4>Mons, Belgique</h4>
					<h5>www.ialu.netportal</h5>
				</a>
				<a class="col-md-3 azul-col europe" href="http://ispx@lasalle.es">
					<h3>Instituto San Pio X</h3>
					<h4>Madrid, ESPAÑA</h4>
					<h5>ispx@lasalle.es</h5>
				</a>
				<a class="col-md-3 azul-col europe" href="http://www.lasallecampus.es">
					<h3>La Salle Campus Madrid</h3>
					<h4>Madrid, ESPAÑA</h4>
					<h5>www.lasallecampus.es</h5>
				</a>
			</div>
		</div>
		<!-- Links tipo Google -->
		<div class="container" style="display:none" id="mosaico-somos-salle">
			<div class="row">
				<section class="image-grid">
					<article class="image__cell is-collapsed">
						<div class="image--basic"><span class="titulos-cuadros-azulitos">Las Unidades principales</span>
							<a href="#expand-jump-0"><img id="expand-jump-0" class="basic__img" src="images/nuevosArtes/vida_estudiantil/1.jpg" alt="Fashion 8"></a>
							<div class="arrow--up"></div>
						</div>
						<div class="image--expand"><img class="image--large" src="images/nuevosArtes/vida_estudiantil/1.jpg" alt="Fashion 8">
							<div class="descripcion-cuadros-azules">Unidad I: unidad principal, acceso por Carlos B. Zetina y Francisco Murguía
								<br> Unidad II: acceso por Benjamín Hill
								<br> Unidad III: acceso por Francisco Murguía
								<br> Unidad IV: San Fernando, Tlalpan, D. F.
								<br> 
								<div class="enlaces-google border-google" id="imagenes-relacionadas" style="display:none">
									<img src="images/nuevosArtes/Noticias/image_news7.png" class="item-img-google">
									<img src="images/nuevosArtes/Noticias/image_news7.png" class="item-img-google">
									<img src="images/nuevosArtes/Noticias/image_news7.png" class="item-img-google">
									<img src="images/nuevosArtes/Noticias/image_news7.png" class="item-img-google">
								</div>
								<div class="enlaces-google" id="descargables" style="display:none">
									<button class="d-pdf">Descargar Archivo</button>
									<button class="d-pdf">Descargar Archivo</button>
								</div>
								<div class="enlaces-google" id="links-referencia" style="display:none">
									<button class="d-pdf link-rel">La Salle en el mundo >></button>
									<button class="d-pdf link-rel">Red de universidades en M&eacute;xico >></button>
								</div>
								<div class="enlaces-google" id="tabla-excel" style="display:none">
									<button class="d-pdf" id="ver-tabla">Ver Tabla</button>
								</div>
							</div>
						</div>
					</article>
					<article class="image__cell is-collapsed">
						<div class="image--basic"><span class="titulos-cuadros-azulitos">Instalaciones y servicios</span>
							<a href="#expand-jump-0"><img id="expand-jump-0" class="basic__img" src="images/nuevosArtes/vida_estudiantil/2.jpg" alt="Fashion 8"></a>
							<div class="arrow--up"></div>
						</div>
						<div class="image--expand"><img class="image--large" src="images/nuevosArtes/vida_estudiantil/2.jpg" alt="Fashion 8">
							<div class="descripcion-cuadros-azules">6 auditorios y salas
								<br> 1 Teatro
								<br> 3 Salones de Usos Múltiples
								<br> 10 salas de cómputo
								<br> 20 Laboratorios especializados
								<br> 
								<div class="enlaces-google border-google" id="imagenes-relacionadas" style="display:none">
									<img src="images/nuevosArtes/Noticias/image_news7.png" class="item-img-google">
									<img src="images/nuevosArtes/Noticias/image_news7.png" class="item-img-google">
									<img src="images/nuevosArtes/Noticias/image_news7.png" class="item-img-google">
									<img src="images/nuevosArtes/Noticias/image_news7.png" class="item-img-google">
								</div>
								<div class="enlaces-google" id="descargables" style="display:none">
									<button class="d-pdf">Descargar Archivo</button>
									<button class="d-pdf">Descargar Archivo</button>
								</div>
								<div class="enlaces-google" id="links-referencia" style="display:none">
									<button class="d-pdf link-rel">La Salle en el mundo >></button>
									<button class="d-pdf link-rel">Red de universidades en M&eacute;xico >></button>
								</div>
								<div class="enlaces-google" id="tabla-excel" style="display:none">
									<button class="d-pdf" id="ver-tabla">Ver Tabla</button>
								</div>
							</div>
						</div>
					</article>
					<article class="image__cell is-collapsed">
						<div class="image--basic"><span class="titulos-cuadros-azulitos">Instalaciones de formación</span>
							<a href="#expand-jump-0"><img id="expand-jump-0" class="basic__img" src="images/nuevosArtes/vida_estudiantil/3.jpg" alt="Fashion 8"></a>
							<div class="arrow--up"></div>
						</div>
						<div class="image--expand"><img class="image--large" src="images/nuevosArtes/vida_estudiantil/3.jpg" alt="Fashion 8">
							<div class="descripcion-cuadros-azules">Unidad Deportiva
								<br> Canchas de Fútbol Soccer y Rápido, Tenis, Voleibol, Basquetbol
								<br> Gimnasio de duela
								<br> Gimnasio de pesas
								<br> Alberca
								<br> 
								<div class="enlaces-google border-google" id="imagenes-relacionadas" style="display:none">
									<img src="images/nuevosArtes/Noticias/image_news7.png" class="item-img-google">
									<img src="images/nuevosArtes/Noticias/image_news7.png" class="item-img-google">
									<img src="images/nuevosArtes/Noticias/image_news7.png" class="item-img-google">
									<img src="images/nuevosArtes/Noticias/image_news7.png" class="item-img-google">
								</div>
								<div class="enlaces-google" id="descargables" style="display:none">
									<button class="d-pdf">Descargar Archivo</button>
									<button class="d-pdf">Descargar Archivo</button>
								</div>
								<div class="enlaces-google" id="links-referencia" style="display:none">
									<button class="d-pdf link-rel">La Salle en el mundo >></button>
									<button class="d-pdf link-rel">Red de universidades en M&eacute;xico >></button>
								</div>
								<div class="enlaces-google" id="tabla-excel" style="display:none">
									<button class="d-pdf" id="ver-tabla">Ver Tabla</button>
								</div>
							</div>
						</div>
					</article>
					<article class="image__cell is-collapsed">
						<div class="image--basic"><span class="titulos-cuadros-azulitos">Servicios complementarios</span>
							<a href="#expand-jump-0"><img id="expand-jump-0" class="basic__img" src="images/nuevosArtes/vida_estudiantil/4.jpg" alt="Fashion 8"></a>
							<div class="arrow--up"></div>
						</div>
						<div class="image--expand"><img class="image--large" src="images/nuevosArtes/vida_estudiantil/4.jpg" alt="Fashion 8">
							<div class="descripcion-cuadros-azules">2 librerías
								<br> 4 cafeterías
								<br> Ananá Frozen Yogurt
								<br> Banco Santander
								<br> Lumen
								<br> 
								<div class="enlaces-google border-google" id="imagenes-relacionadas" style="display:none">
									<img src="images/nuevosArtes/Noticias/image_news7.png" class="item-img-google">
									<img src="images/nuevosArtes/Noticias/image_news7.png" class="item-img-google">
									<img src="images/nuevosArtes/Noticias/image_news7.png" class="item-img-google">
									<img src="images/nuevosArtes/Noticias/image_news7.png" class="item-img-google">
								</div>
								<div class="enlaces-google" id="descargables" style="display:none">
									<button class="d-pdf">Descargar Archivo</button>
									<button class="d-pdf">Descargar Archivo</button>
								</div>
								<div class="enlaces-google" id="links-referencia" style="display:none">
									<button class="d-pdf link-rel">La Salle en el mundo >></button>
									<button class="d-pdf link-rel">Red de universidades en M&eacute;xico >></button>
								</div>
								<div class="enlaces-google" id="tabla-excel" style="display:none">
									<button class="d-pdf" id="ver-tabla">Ver Tabla</button>
								</div>
							</div>
						</div>
					</article>
					<article class="image__cell is-collapsed">
						<div class="image--basic"><span class="titulos-cuadros-azulitos">La Condesa, Barrio Universitario</span>
							<a href="#expand-jump-0"><img id="expand-jump-0" class="basic__img" src="images/nuevosArtes/vida_estudiantil/5.jpg" alt="Fashion 8"></a>
							<div class="arrow--up"></div>
						</div>
						<div class="image--expand"><img class="image--large" src="images/nuevosArtes/vida_estudiantil/5.jpg" alt="Fashion 8">
							<div class="descripcion-cuadros-azules">El campus universitario se encuentra en la Condesa, barrio en la zona centro, con gran cantidad de centros culturales, teatros, foros, cafés, librerías, restaurantes galerías y parques.
								<br> Forma parte del eje de mayor desarrollo de la capital, punto de encuentro para residentes y visitantes nacionales y extranjeros.
								<br> 
								<div class="enlaces-google border-google" id="imagenes-relacionadas" style="display:none">
									<img src="images/nuevosArtes/Noticias/image_news7.png" class="item-img-google">
									<img src="images/nuevosArtes/Noticias/image_news7.png" class="item-img-google">
									<img src="images/nuevosArtes/Noticias/image_news7.png" class="item-img-google">
									<img src="images/nuevosArtes/Noticias/image_news7.png" class="item-img-google">
								</div>
								<div class="enlaces-google" id="descargables" style="display:none">
									<button class="d-pdf">Descargar Archivo</button>
									<button class="d-pdf">Descargar Archivo</button>
								</div>
								<div class="enlaces-google" id="links-referencia" style="display:none">
									<button class="d-pdf link-rel">La Salle en el mundo >></button>
									<button class="d-pdf link-rel">Red de universidades en M&eacute;xico >></button>
								</div>
								<div class="enlaces-google" id="tabla-excel" style="display:none">
									<button class="d-pdf" id="ver-tabla">Ver Tabla</button>
								</div>
							</div>
						</div>
					</article>
				</section>
			</div>
		</div>
		<!-- Links tipo Google - Monumentos -->
		<div class="container" style="display:none" id="mosaico-somos-salle-monumentos">
			<div class="row">
				<section class="image-grid">
					<article class="image__cell is-collapsed">
						<div class="image--basic"><span class="titulos-cuadros-azulitos">Arbol de 100 años</span>
							<a href="#expand-jump-0"><img id="expand-jump-0" class="basic__img" src="http://www.lasalle.mx/wp-content/uploads/2014/03/arbol_100.jpg" alt="Fashion 8"></a>
							<div class="arrow--up"></div>
						</div>
						<div class="image--expand"><img class="image--large" src="http://www.lasalle.mx/wp-content/uploads/2014/03/arbol_100.jpg" alt="Fashion 8">
							<div class="descripcion-cuadros-azules">
								En nuestros jardines crece la semilla de la obra Lasallista sembrada hace más de tres siglos por nuestro Fundador Juan Bautista De La Salle. En el 2005, cada institución lasallista sembró un “Árbol de los Cien Años” para celebrar el primer centenario de presencia de los Hermanos Lasallistas en México. Hoy este árbol frondoso de más de 6 metros viste uno de nuestros corredores principales de la Universidad La Salle recordándonos día a día que nuestro servicio humilde, cotidiano y de entrega, da frutos.
								<br> 
							</div>
						</div>
					</article>
					<article class="image__cell is-collapsed">
						<div class="image--basic"><span class="titulos-cuadros-azulitos">Atrévete a SER</span>
							<a href="#expand-jump-0"><img id="expand-jump-0" class="basic__img" src="http://www.lasalle.mx/wp-content/uploads/2014/03/atrevete_a_ser.jpg" alt="Fashion 8"></a>
							<div class="arrow--up"></div>
						</div>
						<div class="image--expand"><img class="image--large" src="http://www.lasalle.mx/wp-content/uploads/2014/03/atrevete_a_ser.jpg" alt="Fashion 8">
							<div class="descripcion-cuadros-azules">
								El espacio en donde descansan los prismas del monumento Atrévete a Ser fue por más de dos décadas, el único punto de encuentro para la Comunidad Lasallista. Era un espacio multifuncional: área de recreación y actividad física, para honores a la bandera y guardar automóviles, era el único patio con el que contaba la Escuela Preparatoria y de Administración. Hoy la Plaza Bicentenario abraza al monumento Atrévete a Ser, un conjunto de cinco prismas verticales de diferentes alturas y planta triangular, que apuntan al cielo. Su autor, el Hno. José Cervantes Hernández detalla en un poema la obra: “Juntos y por asociación”. Así nos quiso La Salle. Comunidad, modelo nuestro universitario: cada uno diferente, y cada quien embonando en el conjunto. …En todas las estaciones, en todos los inviernos y veranos, en los turbiones o en las claridades inocentes, estas columnas aquí quedarán, testigos del encuentro con el amigo que, en la veracidad y en el amor, todos queremos ser, emblema, efigie, signo, como ellas, atrévete a serlo tú también.
								<br> 
							</div>
						</div>
					</article>
					<article class="image__cell is-collapsed">
						<div class="image--basic"><span class="titulos-cuadros-azulitos">Casa Rosa</span>
							<a href="#expand-jump-0"><img id="expand-jump-0" class="basic__img" src="http://www.lasalle.mx/wp-content/uploads/2014/03/casa_rosa.jpg" alt="Fashion 8"></a>
							<div class="arrow--up"></div>
						</div>
						<div class="image--expand"><img class="image--large" src="http://www.lasalle.mx/wp-content/uploads/2014/03/casa_rosa.jpg" alt="Fashion 8">
							<div class="descripcion-cuadros-azules">
								Construida en 1913-1914 por el ingeniero, arquitecto y geólogo Don Manuel de Anda y Barreda, integró las técnicas de construcción más modernas es esa época como vigas de acero, instalación eléctrica e hidráulica y fue decorada bellamente con artesonados de yeso y paneles enmarcados. Cuando el predio fue adquirido por la Universidad en la década de los ochenta, se encontraba en condiciones deplorables. Hoy este inmueble es Patrimonio Cultural de la Colonia Condesa y alberga las oficinas del Centro Internacional de Educación La Salle, órgano responsable de la internacionalización en esta Casa de Estudios. Su amplio comedor y sala de junta, pueden ser reservados para reuniones y actividades.
								<br> 
							</div>
						</div>
					</article>
					<article class="image__cell is-collapsed">
						<div class="image--basic"><span class="titulos-cuadros-azulitos">Estatua del fundador</span>
							<a href="#expand-jump-0"><img id="expand-jump-0" class="basic__img" src="http://www.lasalle.mx/wp-content/uploads/2014/03/Fundador.jpg" alt="Fashion 8"></a>
							<div class="arrow--up"></div>
						</div>
						<div class="image--expand"><img class="image--large" src="http://www.lasalle.mx/wp-content/uploads/2014/03/Fundador.jpg" alt="Fashion 8">
							<div class="descripcion-cuadros-azules">
								La estatua colocada en Plaza La Salle es la primer réplica realizada de un original hecho en mármol de carrara que se encuentra a un costado de la capilla de la Casa de Formación de los Hermanos, en Tlalpan al Sur de la Ciudad de México. Don Luis Fern
								<br> 
							</div>
						</div>
					</article>
					<article class="image__cell is-collapsed">
						<div class="image--basic"><span class="titulos-cuadros-azulitos">La Justicia</span>
							<a href="#expand-jump-0"><img id="expand-jump-0" class="basic__img" src="http://www.lasalle.mx/wp-content/uploads/2014/03/escultura_justicia.jpg" alt="Fashion 8"></a>
							<div class="arrow--up"></div>
						</div>
						<div class="image--expand"><img class="image--large" src="http://www.lasalle.mx/wp-content/uploads/2014/03/escultura_justicia.jpg" alt="Fashion 8">
							<div class="descripcion-cuadros-azules">
								Es una escultura de una mujer sosteniendo en equilibrio la balanza de la justicia. Pisa con el pie izquierdo la cabeza de una serpiente que se encuentra sobre el código de normas jurídicas en defensa de los ideales y las virtudes que persigue la profesión. La Justicia se develó cuando la Facultad de Derecho cumplió 30 años, solicitando al escultor Carlos Espino que reflejara en una escultura el recuerdo de que, a pesar de la vicisitudes de la vida, siempre tenemos que enaltecer la bandera de la Justicia, Verdad y la Honestidad, fieles y siempre unidos a la misión Lasallista.
								<br> 
							</div>
						</div>
					</article>
					<article class="image__cell is-collapsed">
						<div class="image--basic"><span class="titulos-cuadros-azulitos">La Verdad</span>
							<a href="#expand-jump-0"><img id="expand-jump-0" class="basic__img" src="http://www.lasalle.mx/wp-content/uploads/2014/03/escultura_belleza.jpg" alt="Fashion 8"></a>
							<div class="arrow--up"></div>
						</div>
						<div class="image--expand"><img class="image--large" src="http://www.lasalle.mx/wp-content/uploads/2014/03/escultura_belleza.jpg" alt="Fashion 8">
							<div class="descripcion-cuadros-azules">
								Espiral blanca de arte dinámico e innovador, al centro tiene una esfera roja que nos habla de verdad y unidad, representando el corazón que une a la Comunidad Lasallista. Se develó cuando la FAMADyC cumplió 35 años de existencia, convocando previamente a un concurso a alumnos de Arquitectura. Obtuvo el primer lugar, la pieza arquitectónica “La belleza y la verdad son mis pilares fundamentales” del equipo conformado por Álvaro Carrera y Sergio López, asesorados por el profesor Arq. Pedro Vázquez Estupiñán.
								<br> 
							</div>
						</div>
					</article>
					<article class="image__cell is-collapsed">
						<div class="image--basic"><span class="titulos-cuadros-azulitos">Los dones del Señor</span>
							<a href="#expand-jump-0"><img id="expand-jump-0" class="basic__img" src="http://www.lasalle.mx/wp-content/uploads/2014/03/fuente_dones.jpg" alt="Fashion 8"></a>
							<div class="arrow--up"></div>
						</div>
						<div class="image--expand"><img class="image--large" src="http://www.lasalle.mx/wp-content/uploads/2014/03/fuente_dones.jpg" alt="Fashion 8">
							<div class="descripcion-cuadros-azules">
								Con el propósito de crear una experiencia cordial al acceso a nuestras instalaciones, el corredor principal de entrada a la Unidad I, comenzó una etapa de remodelación que incluía una fuente que llenara de vida y energía el ambiente, y diera la bienvenida a nuestra comunidad Universitaria. La fuente es un muro levantado con mosaicos de bloques pequeños de fragmento de piedra; cantos trabajados y ensamblados, donde resalta la frase de Leonardo Da Vinci “Señor, tú nos das los dones, pero nos pides a cambio la fatiga”.
								<br> 
							</div>
						</div>
					</article>
					<article class="image__cell is-collapsed">
						<div class="image--basic"><span class="titulos-cuadros-azulitos">Mural Medicina</span>
							<a href="#expand-jump-0"><img id="expand-jump-0" class="basic__img" src="http://www.lasalle.mx/wp-content/uploads/2014/03/Mural_Medicina.jpg" alt="Fashion 8"></a>
							<div class="arrow--up"></div>
						</div>
						<div class="image--expand"><img class="image--large" src="http://www.lasalle.mx/wp-content/uploads/2014/03/Mural_Medicina.jpg" alt="Fashion 8">
							<div class="descripcion-cuadros-azules">
								Adornando el vestíbulo del Teatro De La Salle en la Facultad Mexicana de Medicina, se encuentra el mural “La Historia de la Medicina” del Hno. Antonio Carrillo. Trabajado por secciones y pintado en Puebla, el mural de 12 m de largo x 2.40 m de ancho evoca épocas, pasajes y personajes de la historia de la medicina, incluyendo a los griegos, egipcios, los alquimistas, la herbolaria, los microscopios, resonancia magnética y todos aquellos que aportaron algo en la evolución de la ciencia médica. La Historia de la Medicina da vida al espacio cultural, social y académico que ofrece el Teatro a toda la Comunidad Universitaria.<br> 
							</div>
						</div>
					</article>
					<article class="image__cell is-collapsed">
						<div class="image--basic"><span class="titulos-cuadros-azulitos">Mural Dios</span>
							<a href="#expand-jump-0"><img id="expand-jump-0" class="basic__img" src="http://www.lasalle.mx/wp-content/uploads/2014/03/Mural_Dios_Creador.jpg" alt="Fashion 8"></a>
							<div class="arrow--up"></div>
						</div>
						<div class="image--expand"><img class="image--large" src="http://www.lasalle.mx/wp-content/uploads/2014/03/Mural_Dios_Creador.jpg" alt="Fashion 8">
							<div class="descripcion-cuadros-azules">
								El edificio 1D albergó hasta 1974, en el sexto, séptimo y octavo piso, la Casa de los Hermanos Lasallistas. Después del terremoto de 1985 se sometió a remodelaciones, que incluían la restauración de un mural, creación del Hno. Antonio Carrillo. No fue posible contrarrestar el deterioro ocasionado por el paso del tiempo y la humedad, por lo que se remplazó por uno nuevo. El mural, también del Hno. Carrillo, incluye varios elementos como una escena de Dios y la creación del hombre, a la derecha los más destacados filósofos griegos y a la izquierda los intelectuales modernos signos de la humanidad, reflejando la unión de pensamientos en el campo de la educación, la filosofía, la teología y la poesía.
								<br> 
							</div>
						</div>
					</article>
					<article class="image__cell is-collapsed">
						<div class="image--basic"><span class="titulos-cuadros-azulitos">Mural de Iconos</span>
							<a href="#expand-jump-0"><img id="expand-jump-0" class="basic__img" src="http://www.lasalle.mx/wp-content/uploads/2014/03/Mural_Arquitectura.jpg" alt="Fashion 8"></a>
							<div class="arrow--up"></div>
						</div>
						<div class="image--expand"><img class="image--large" src="http://www.lasalle.mx/wp-content/uploads/2014/03/Mural_Arquitectura.jpg" alt="Fashion 8">
							<div class="descripcion-cuadros-azules">
								En el año 2002, la entonces Escuela de Arquitectura Diseño y Comunicación, lanzó una convocatoria para crear una propuesta plástica que representara los íconos de estas disciplinas. La propuesta ganadora fue presentada y desarrollada por el Arq. Fidel Meraz, quien logró sintetizar en esta obra de talla majestuosa dividida en 13 módulos, los hitos relevantes en la evolución de la arquitectura, el diseño y la comunicación. Puedes visitarlo en los pasillos de la FAMADyC , edificio 2C.
							</div>
						</div>
					</article>
					<article class="image__cell is-collapsed">
						<div class="image--basic"><span class="titulos-cuadros-azulitos">Mural de Felguerez</span>
							<a href="#expand-jump-0"><img id="expand-jump-0" class="basic__img" src="http://www.lasalle.mx/wp-content/uploads/2014/03/Mural_Felguerez.jpg" alt="Fashion 8"></a>
							<div class="arrow--up"></div>
						</div>
						<div class="image--expand"><img class="image--large" src="http://www.lasalle.mx/wp-content/uploads/2014/03/Mural_Felguerez.jpg" alt="Fashion 8">
							<div class="descripcion-cuadros-azules">
								Para conmemorar el 35 aniversario de la Universidad La Salle se solicitó al renombrado escultor y pintor Manuel Felguérez realizar un mural inspirado en la heráldica de La Salle. Curvas y relieves, vacíos, texturas y sombras rompen el símbolo de la Universidad, transformándose en libros. Este imponente mural viste la plaza central y corredores con nuestros colores institucionales.
								<br> 
							</div>
						</div>
					</article>
					<article class="image__cell is-collapsed">
						<div class="image--basic"><span class="titulos-cuadros-azulitos">Plaza la Raza</span>
							<a href="#expand-jump-0"><img id="expand-jump-0" class="basic__img" src="http://www.lasalle.mx/wp-content/uploads/2014/03/plazuela_raza.jpg" alt="Fashion 8"></a>
							<div class="arrow--up"></div>
						</div>
						<div class="image--expand"><img class="image--large" src="http://www.lasalle.mx/wp-content/uploads/2014/03/plazuela_raza.jpg" alt="Fashion 8">
							<div class="descripcion-cuadros-azules">
								Plaza vestida por una escultura en bronce, montada sobre una base cuadrada de piedra. La escena está compuesta por un hombre español, una mujer mexicana y una niña pequeña que surge como fusión de ambas culturas, a la que llamamos “el origen de una raza”: la raza mexicana. La Plaza ofrece una terraza con mesas y sillas para comer al aire libre cualquier tarde soleada.
							</div>
						</div>
					</article>
					<article class="image__cell is-collapsed">
						<div class="image--basic"><span class="titulos-cuadros-azulitos">Reloj del milenio</span>
							<a href="#expand-jump-0"><img id="expand-jump-0" class="basic__img" src="http://www.lasalle.mx/wp-content/uploads/2014/03/reloj_milenio.jpg" alt="Fashion 8"></a>
							<div class="arrow--up"></div>
						</div>
						<div class="image--expand"><img class="image--large" src="http://www.lasalle.mx/wp-content/uploads/2014/03/reloj_milenio.jpg" alt="Fashion 8">
							<div class="descripcion-cuadros-azules">
								Este reloj de cuatro caras dio la bienvenida al nuevo milenio a un costado de Plaza La Salle en enero del 2000. Construido en la cuna de los Relojes Monumentales, Zacatlán de las Manzanas, Puebla, muestra una cubierta de vidrio, descubriendo la belleza de su maquinaria. Sus engranes simbolizan el ir y venir de los estudiantes y docentes, su tic-tac simboliza el latido que da vida a nuestra comunidad, es por eso que en su interior cada semestre se guardan las cartas con las ilusiones de los alumnos de primer ingreso. En un momento muy emotivo, al finalizar su programa académico, recuperan sus cartas.
								<br> 
							</div>
						</div>
					</article>
					<article class="image__cell is-collapsed">
						<div class="image--basic"><span class="titulos-cuadros-azulitos">Reloj Solar</span>
							<a href="#expand-jump-0"><img id="expand-jump-0" class="basic__img" src="http://www.lasalle.mx/wp-content/uploads/2014/03/reloj_solar.jpg" alt="Fashion 8"></a>
							<div class="arrow--up"></div>
						</div>
						<div class="image--expand"><img class="image--large" src="http://www.lasalle.mx/wp-content/uploads/2014/03/reloj_solar.jpg" alt="Fashion 8">
							<div class="descripcion-cuadros-azules">
								Resultado del trabajo fraterno entre docentes y estudiantes, este reloj de aluminio y latón se elaboró en los talleres de ingeniería, para conmemorar treinta años en la formación de ingenieros profesionales con valor. Representa la exactitud y la clave de su funcionamiento es la geometría y la inclinación. Se orienta conforme a las estaciones del año, los ajustes son astronómicos de acuerdo con la geometría de círculos y ángulos. La flecha a la vista (gnom) es la que proyecta la sombra en los números e indica la hora.
							</div>
						</div>
					</article>
					<article class="image__cell is-collapsed">
						<div class="image--basic"><span class="titulos-cuadros-azulitos">Tenemos un compromiso</span>
							<a href="#expand-jump-0"><img id="expand-jump-0" class="basic__img" src="http://www.lasalle.mx/wp-content/uploads/2014/03/escultura_compromiso.jpg" alt="Fashion 8"></a>
							<div class="arrow--up"></div>
						</div>
						<div class="image--expand"><img class="image--large" src="http://www.lasalle.mx/wp-content/uploads/2014/03/escultura_compromiso.jpg" alt="Fashion 8">
							<div class="descripcion-cuadros-azules">
								Con motivo de la celebración del 50 Aniversario de la Facultad de Negocios, se seleccionó una escultura que simboliza la actividad principal que se desarrolla en el mundo de los negocios, eligiendo dos manos que amistosamente se estrechan cerrando un trato ético y honesto como compromiso. Una Cápsula del Tiempo fue colocada dentro del Monumento de cantera y bronce azul, junto a una placa con pensamientos escritos por San Juan Bautista de La Salle en sus Meditaciones para los Maestros.
								<br> 
							</div>
						</div>
					</article>
					<article class="image__cell is-collapsed">
						<div class="image--basic"><span class="titulos-cuadros-azulitos">Luz Milenio</span>
							<a href="#expand-jump-0"><img id="expand-jump-0" class="basic__img" src="http://www.lasalle.mx/wp-content/uploads/2013/10/Luz_del_Milenio-small.jpg" alt="Fashion 8"></a>
							<div class="arrow--up"></div>
						</div>
						<div class="image--expand"><img class="image--large" src="http://www.lasalle.mx/wp-content/uploads/2013/10/Luz_del_Milenio-small.jpg" alt="Fashion 8">
							<div class="descripcion-cuadros-azules">
								Tres piezas curvas, dispuestas casi entrelazadas, componen la escultura Luz Milenio, representando cada una 1000 años de historia. La terminación en punta de las piezas de forma helicoidal representa la proyección hacia el infinito, equivalentes a la asociación física del movimiento tal como ocurre en la naturaleza: crecimiento de especies de árboles, turbulencias y remolinos; disposición de los astros y reacciones químicas tales como el fuego, e incluso la forma helicoidal en estructuras moleculares del ADN. La escultura que da vida a la Facultad de química es el remate visual desde el acceso principal, sirviéndole de identidad y referencia, brindando unidad al conjunto de edificios que le rodean; representando al “punto de encuentro” de la Comunidad que los habita.
							</div>
						</div>
					</article>
				</section>
			</div>
		</div>
		<!-- Video -->
		<div id="video-container" style="position: relative;height: 467px !important;z-index: 4;display:none" class="container video-somos-salle">
			<video autoplay="" loop="" muted="" class="fillWidth">
				<source src="images/nuevosArtes/video/nuevos.mp4" type="video/mp4; codecs=&quot;avc1.42E01E, mp4a.40.2&quot;">
				<source src="video/trailer.webm" type="video/webm; codecs=&quot;vp8, vorbis&quot;"> Video not supported.
			</video>
		</div>
		<!-- Carousel -->
      </div>
    </div>
    
  <div class="container">    
      <div id="myCarouselLaSalle"   class="carousel slide" data-ride="carousel">
      </div> 
    </div> 
	
	<div class="container" id="carusel-somos-salle" style="width: 100%;display:none">
		  <div id="slider-somos-salle" class="carousel slide" data-ride="carousel">
			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
			  <div class="item active">
				<img src="images/nuevosArtes/Noticias/image_news5.png" width="100%" height="345">
				<div class="carousel-caption">
				  <h3>Chania</h3>
				  <p>The atmosphere in Chania has a touch of Florence and Venice.</p>
				</div>
			  </div>
			  <div class="item">
				<img src="images/nuevosArtes/Noticias/image_news5.png" width="100%" height="345">
				<div class="carousel-caption">
				  <h3>Chania</h3>
				  <p>The atmosphere in Chania has a touch of Florence and Venice.</p>
				</div>
			  </div>
			</div>
		   <!-- Left and right controls -->
			<a class="left carousel-control" href="#slider-somos-salle" role="button" data-slide="prev">
			  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			  <span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#slider-somos-salle" role="button" data-slide="next">
			  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			  <span class="sr-only">Next</span>
			</a>
		  </div>
	</div>
				
	<div class="container" id="links-historia" style="display:none">
		<div class="links-historia">
			<a href="#" class="numeritos">1<h5>Texto de ejemplo</h5></a>
			<a href="#" class="numeritos">2<h5>Texto de ejemplo</h5></a>
			<a href="#" class="numeritos">3<h5>Texto de ejemplo</h5></a>
			<a href="#" class="numeritos">4<h5>Texto de ejemplo</h5></a>
			<a href="#" class="numeritos">5<h5>Texto de ejemplo</h5></a>
			<a href="#" class="numeritos">6<h5>Texto de ejemplo</h5></a>
			<a href="#" class="numeritos">7<h5>Texto de ejemplo</h5></a>
			<a href="#" class="numeritos">8<h5>Texto de ejemplo</h5></a>
		</div>
	</div>
	
    <div  class="container marginEntreSecciones">
      <div class="row">
        <div class="slider4 slider-inner">
        </div>
      </div>
    </div>
    

    <div class="container">
      <div class="row seccion_1 ws ">
      </div>
    </div>

  </div>

  <!--  FIN SECCION UNIVERSIDAD LA SALLE
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->

  <!--  SECCION UNIVERSIDAD LA SALLE
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <!--  SECCION UNIVERSIDAD LA SALLE RESPONSIVE -->


  <div id="universidadShowResponsive" class="marginEntreSecciones" style=""> 
    <div class="container marginEntreSecciones">
      <div class="row">
            
        <div class="subMenuResponsiveUniversidad">
          <div class="flechita">
            <select>
              <option value="">La Salle hoy</option>
              <option value="">Mision, Vision e Ideano</option>
              <option value="">Modelo Educativo</option>
              <option value="">Acreditaciones</option>
              <option value="">Plan de Desarrollo Institucional</option>
              <option value="">Reglamentos</option>
              <option value="">Historia</option>
              <option value="">Identidad</option>
            </select>
          </div>
        </div>

        <div class="contenedorTituloUniversidadResponsive">
          
          <div class="tituloContenedorUniversidadResponsive">
            <div class="textoTituloUniversidadResponsive">
              <p>La Residencia Universitaria de la Universidad La Salle es un espacio donde el estudio y el desarrollo personal son una prioridad</p>
            </div>
          </div>

          <div class="imageTituloUniversidad">
            <div class="container_image_responsive">
                    
            </div>
          </div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="content_text_universidad_responsive">
        <h6>¿Pero como lo hacemos?</h6>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquad Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquad</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquad Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquad</p>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="video_universidad_responsive">
          <img src="images/nuevosArtes/video/video_universidad.png">
        </div>
      </div>
    </div>

  </div>



  <!-- FIN SECCION UNIVERSIDAD LA SALLE
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <!-- FIN SECCION UNIVERSIDAD LA SALLE RESPONSIVE -->




  <!-- SECCION OFERTA ACADEMICA
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->

  <div id="ofertaAcademicaShow">
		<div class="container marginEntreSecciones">
    

      <div class="row">
        
        <div class="headerSubMenuUniversidad">
        
          <div class="two columns tituloUniversidad">
            <img src="images/nuevosArtes/btn_seccion/somos_la_salle.png"> 
          </div>
            
          <div class="ten columns subMenuUniversidad">
			
            <?php submenus(2); ?>
            
          </div>


            <div class="ten columns submenu" id="subsubmenus-menu2">
               <!-- <div class="cuadrosSubmenu">Contenido Uno</div> -->
            </div>
             

        </div>

         


        <div class="contenedorTituloUniversidad">
          <div class="tituloContenedorUniversidad">
            <div class="textoTituloUniversidad">
              <p id="subtitulo-subsubmenu-menu2"></p>
            </div>
          </div>
        </div>


      </div>

    </div>
		
		
		<div class="ocultarContenidoSubMenu">
		  <div class="container" id="componentes-menu2">
			<div class="row">
			  
			  
			</div>
		  </div>
		</div>
		
		 <div class="contenedorImagenTextoAccionSocial-" style="background: url(images/nuevosArtes/banner/auditorioimg.jpg)no-repeat;background-attachment: fixed;">
			<div class="container" id="componentes-menu2">
			  <div class="contenedorDiagonalUniversidad2" id="scrollImagen2">
				<p class="descripcionTituloUniversidad- ws scroll-box-150">Vivir en la Republica es una experiencia compartida. Por este motivo, el respeto y el orden con indispensables. La consideracion hacia las personas y las instalaciones, la empatia y el consumo responsable de los recursos, conforman la base para poder garantizar un espacio agradable y familiar que tiene como eje principal el estudio y el desarrollo personal de los residentes.</p>

				</div>
			</div>
		  </div>
		
  </div>





  <!-- FIN SECCION OFERTA ACADEMICA
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <!-- FIN SECCION OFERTA ACADEMICA -->



  <!-- SECCIONES RESPONSIVAS OFERTA ACADEMICA
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->

  <div id="ofertaAcademicaResponsive">
    <div class="contenedorInvestigacion marginEntreSecciones"> 
      <div class="container">
        
        <div class="row">

          <div class="subMenuResponsiveUniversidad">
            <div class="flechita">
              <select>
                <option value="">Licenciaturas</option>
                <option value="">Campus</option>
                <option value="">Deportes</option>
                <option value="">Aguilas<br> La salle, grupos<br>representativos</option>
                <option value="">Grupos<br>Estudiates</option>
                <option value="">Desarrollo<br> Humano y<br>espiritual</option>
                <option value="">La salle por<br> la tierra</option>
                <option value="">Universidad<br> Segura</option>
                <option value="">Medios<br> Institucionales</option>
                <option value="">La condesa<br> barrio<br> Univeristario</option>
              </select>
            </div>
          </div>
          
          <div class="subMenuResponsiveUniversidad">
            <div class="flechita">
              <select>
                <option value="">Facultad Mexicana de Medicina</option>
                <option value="">Campus</option>
                <option value="">Deportes</option>
                <option value="">Aguilas<br> La salle, grupos<br>representativos</option>
                <option value="">Grupos<br>Estudiates</option>
                <option value="">Desarrollo<br> Humano y<br>espiritual</option>
                <option value="">La salle por<br> la tierra</option>
                <option value="">Universidad<br> Segura</option>
                <option value="">Medios<br> Institucionales</option>
                <option value="">La condesa<br> barrio<br> Univeristario</option>
              </select>
            </div>
          </div>

          <div class="subMenuResponsiveUniversidad">
            <div class="flechita">
              <select>
                <option value="">Licenciatura en Médico Cirujano</option>
                <option value="">Campus</option>
                <option value="">Deportes</option>
                <option value="">Aguilas<br> La salle, grupos<br>representativos</option>
                <option value="">Grupos<br>Estudiates</option>
                <option value="">Desarrollo<br> Humano y<br>espiritual</option>
                <option value="">La salle por<br> la tierra</option>
                <option value="">Universidad<br> Segura</option>
                <option value="">Medios<br> Institucionales</option>
                <option value="">La condesa<br> barrio<br> Univeristario</option>
              </select>
            </div>
          </div>


          <div class="container_menu_opciones_plan_estudio">
            <div id="menu_plan_estudios_1" class="square"><button id="btn_uno_plan_estudio" class="btn_opc_1"></button></div>
            <div id="menu_plan_estudios_2" class="square"><button id="btn_dos_plan_estudio" class="btn_opc_2"></button></div>
            <div id="menu_plan_estudios_3" class="square"><button id="btn_tres_plan_estudio" class="btn_opc_3"></button></div>
            <div id="menu_plan_estudios_4" class="square"><button id="btn_cuatro_plan_estudio" class="btn_opc_4"></button></div>
            <div id="menu_plan_estudios_5" class="square"><button id="btn_cinco_plan_estudio" class="btn_opc_5"></button></div>
            <div id="menu_plan_estudios_6" class="square"><button id="btn_seis_plan_estudio" class="btn_opc_6"></button></div>
          </div>
          

          <div class="container_info_gral" id="container_info_gral">
            
            <div class="title_seccion_container">
              <div class="container">
                <p class="textHelveticaBold"><br>Plan de Estudios</p>
              </div>
            </div>

            <div class="image_image_rsponsive_seccion">
              <img src="images/nuevosArtes/images_responsive/oferta_academica_banner.png">
            </div>

            <div class="container_skew_text">
              
              <div class="contenedor_text_responsive_skew_left_principal_menu_facultades">
                <div class="content_text_gray">
                  <p>Numero y fecha de acuerdo<br> RVCOEDecreto Presidecial del 29 de mayo<br>de<br> 1987 / 8 de septiembre de 2008<br><br>Turno<br>Mixto<br>
                  <span class="textHelveticaBold">Modalidad de titulación:<br></span>
                Examen General de Conocimientos</p>
                </div>
              </div>

            
              <div class="title_seccion_container_subtitle">
                <p>Licenciatura en <span class="textHelveticaBold">Médico Cirujano</span></p>
              </div>
            

              <div class="container_data_general_licenciatura">           
                <div class="table_data_left_licenciature">
                  <p class="title_licenciature">Modalidad</p>
                  <p class="desc_licenciature">Escolarizada</p>
                  <p class="title_licenciature">Duracion del ciclo</p>
                  <p class="desc_licenciature">18 semanas cada semestre</p>
                  <p class="title_licenciature">Creditos a cubrir</p>
                  <p class="desc_licenciature">611.43</p>
                </div>
                        
                <div class="table_data_right_licenciature">
                  <p class="title_licenciature">Número de semestres</p>
                  <p class="desc_licenciature">10</p>
                  <p class="title_licenciature">Tipo de ciclo</p>
                  <p class="desc_licenciature">Semestral los primeros 4 años Anueal el 5º año</p>
                  <p class="title_licenciature">Antecentes académicos de ingreso</p>
                  <p class="desc_licenciature">Bachillerato o equivalente</p>
                </div>
              </div>


              <div class="contanier_list_course">
                <h6 class="title_semestre">Primer <span class="textHelveticaBold">Semestre</span></h6>
                <p>ºLengua Extranjera I</p>
                <p>ºAnatomia</p>
                <p>ºEmbriología</p>
                <p>ºHistología</p>
                <p>ºInformatica Medica</p>
                <p>ºHistoria de la Medicina</p>
              </div>


              <div class="contanier_list_course">
                <h6 class="title_semestre">Segundo <span class="textHelveticaBold">Semestre</span></h6>
                <p>ºLengua Extranjera II</p>
                <p>ºLa Persona y su Interacción con los otros</p>
                <p>ºNeuroanatomia y Órganos de los Sentidos</p>
                <p>ºBioquimicas</p>
                <p>ºFisiología I</p>
                <p>ºPsicología Médica</p>
                <p>ºSalud Publica</p>
              </div>


              <div class="contanier_list_course">
                <h6 class="title_semestre">Tercer <span class="textHelveticaBold">Semestre</span></h6>
                <p>ºDialogo Intercultural</p>
                <p>ºFarmacología</p>
                <p>ºBiología Molecular</p>
                <p>ºFisiologia II</p>
                <p>ºMicrobiología</p>
                <p>ºBases de Cirugía</p>
              </div>


              <div class="contanier_list_course">
                <h6 class="title_semestre">Cuarto <span class="textHelveticaBold">Semestre</span></h6>
                <p>ºValores y Ética Profesional</p>
                <p>ºNosologia</p>
                <p>ºGenetica y Medicina Genómica</p>
                <p>ºAnatomía Patologíca</p>
                <p>ºPropedéutica Clínica </p>
                <p>ºSeminario de Integración Clínica Básica</p>
                <p>Advance Bu Life Support</p>
              </div>



              <div class="contanier_list_course">
                <h6 class="title_semestre">Quinto <span class="textHelveticaBold">Semestre</span></h6>
                <p>ºEmprendimiento</p>
                <p>ºFe y Desarrollo Espiritual</p>
                <p>ºHerramientas Clinicas Diagnósticas</p>
                <p>ºNefrología</p>
                <p>ºInmunología y Reumotología</p>
                <p>ºNeurología</p>
                <p>ºNeumología</p>
                <p>ºEndocrinología</p>
                <p>ºCardiología</p>
                <p>ºTaller "Harley" y Electrocardiografía</p>
              </div>


              <div class="contanier_list_course">
                <h6 class="title_semestre">Sexto <span class="textHelveticaBold">Semestre</span></h6>
                <p>ºTaller: Empleo, Autoempleo y Actividad Empresarial</p>
                <p>ºObligatoria Electiva</p>
                <p>ºDermatologia</p>
                <p>ºMedicina Forense</p>
                <p>ºHematología</p>
                <p>ºGastroenrerología</p>
                <p>ºInfectología</p>
                <p>ºPsiquiatria</p>
                <p>ºMetodología de la Investigación y Bioestadistica</p>
                <p>ºBasic Life Support y Advanced Cardio Life Support</p>
              </div>


              <div class="contanier_list_course">
                <h6 class="title_semestre">Septimo <span class="textHelveticaBold">Semestre</span></h6>
                <p>ºNutrición</p>
                <p>ºPatología Quirurgica</p>
                <p>Oftalmologia</p>
                <p>ºOtorrinolaringología</p>
                <p>ºGeriatría</p>
                <p>ºTraumatoligía y Ortopedia</p>
                <p>ºSeminario de Integracion Clinica Avanzada</p>
                <p>ºEpidemiologia y Trabajo Comunitario</p>
                <p>ºAspectos Legales de la Practica Medica</p>
                <p>Taller "I stan"</p>
              </div>


              <div class="contanier_list_course">
                <h6 class="title_semestre">Octavo <span class="textHelveticaBold">Semestre</span></h6>
                <p>ºTaller: Proyecto Profesional Ocupacional</p>
                <p>ºSeminario de Bioética</p>
                <p>ºUrologia</p>
                <p>ºSexología Humana</p>
                <p>ºPediatria</p>
                <p>ºGinecología y Obstetricia</p>
                <p>ºToxicologia y Medicina Complementaria</p>
                <p>ºAdministración de los Servicios de Salud</p>
                <p>ºReanimación Neonatal</p>
              </div>


              <div class="contanier_list_course">
                <h6 class="title_semestre">Noveno <span class="textHelveticaBold">Semestre</span></h6>
                <p>ºINTERNADO DE PREGRADO</p>
                <p>ºMedicina Interna</p>
                <p>ºCirugia</p>
                <p>ºPediatría</p>
                <p>ºGinecologia y Obstetricia</p>
                <p>ºUrgencias</p>
                <p>ºMedicina Familiar y Comunitaria</p>
              </div>


              <div class="contanier_list_course">
                <h6 class="title_semestre">Materias <span class="textHelveticaBold">Obligatorias</span></h6>
                <p>ºObligatoria Electiva 1</p>
                <p>ºEl Fenomeno Religioso</p>
                <p>ºFe Religiosa y Mundo Actual</p>
                <p>ºEl Mensaje Liberador de Jesús</p>
              </div>


              <div class="container_video_licenciature">
                <img src="images/nuevosArtes/images_responsive/video.png">
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div> 


  <!-- FIN SECCION RESPONSIVA OFERTA ACADEMICA
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->

  


  <!--  SECCION VIDA ESTUDIANTIL
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->

  <div id="vidaShow">
    <div class="contenedorInvestigacion marginEntreSecciones"> 
      <div class="container">       
        <div class="row">

          <div class="headerSubMenuInvestigacion">
            
            <div class="two columns tituloServicios">
              <img src="images/nuevosArtes/btn_seccion/vida_estudiantil.png"> 
            </div>
              
            <div class="eleven columns subMenuAccionSocial vidaestudiantil">
              <?php submenus(3); ?>
            </div>

          </div>

          <div class="contenedorTituloServicios">
            <div class="tituloContenedorVida">
              <div class="textoTituloVida">
                <p id="subtitulo-subsubmenu-menu3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquad</p>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>

    <div class="ocultarContenidoSubMenu">
      <div class="container" id="componentes-menu3">
        <div class="row">
          
          
        </div>
      </div>
    </div>
  </div> 



  <!-- FIN SECCION VIDA ESTUDIANTIL
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->


  <!-- SECCION RESPONSIVA VIDA ESTUDIANTIL
  ––––––––––––––––––––––––––––––––––––––––––––––––– -->


  <div id="vidaShowResponsive">
    <div class="contenedorInvestigacion marginEntreSecciones"> 
      <div class="container">
        
        <div class="row">

          <div class="subMenuResponsiveUniversidad">
            <div class="flechita">
              <select>
                <option value="">Arte y cultura</option>
                <option value="">Campus</option>
                <option value="">Deportes</option>
                <option value="">Aguilas<br> La salle, grupos<br>representativos</option>
                <option value="">Grupos<br>Estudiates</option>
                <option value="">Desarrollo<br> Humano y<br>espiritual</option>
                <option value="">La salle por<br> la tierra</option>
                <option value="">Universidad<br> Segura</option>
                <option value="">Medios<br> Institucionales</option>
                <option value="">La condesa<br> barrio<br> Univeristario</option>
              </select>
            </div>
          </div>

          <div class="container_skew_text">
              
            <div class="contenedor_text_responsive_skew_left_principal">
              <div class="content_text_red">
                <p>En la salle encontrarás un espacio para compartir, aprender, recordar, sentir, imaginar, pensar, leer, bailar.</p>
              </div>
            </div>

            <div class="contenedor_text_responsive_skew_right">
              <div class="content_text">
                <h6 class="azulClaro">Programas <span class="azulFuerte">y Talleres</span></h6>
                <p>En la salle encontrarás un espacio para compartir, aprender, recordar, sentir, imaginar, pensar, leer, bailar.</p>
              </div>
            </div>

            <div class="contenedor_text_responsive_skew_left">
              <div class="content_text_red">
                <h6 class="azulClaro">Talleres en <span class="azulFuerte">Línea</span></h6>
                <p>En la salle encontrarás un espacio para compartir, aprender, recordar, sentir, imaginar, pensar, leer, bailar.</p>
              </div>
            </div>

            <div class="contenedor_text_responsive_skew_right">
              <div class="content_text">
                <h6 class="azulClaro">Clases <span class="azulFuerte">Individuales</span></h6>
                <p>En la salle encontrarás un espacio para compartir, aprender, recordar, sentir, imaginar, pensar, leer, bailar.</p>
              </div>
            </div>

            <div class="contenedor_text_responsive_skew_left">
              <div class="content_text_red">
                <h6 class="azulClaro">Exhibiciones y<span class="azulFuerte">Actividades</span></h6>
                <p>En la salle encontrarás un espacio para compartir, aprender, recordar, sentir, imaginar, pensar, leer, bailar.</p>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div> 


  <!-- FIN  SECCION RESPONSIVA VIDA ESTUDIANTIL
  ––––––––––––––––––––––––––––––––––––––––––––––––– -->






  <!-- SECCION SERVICIOS
  ––––––––––––––––––––––––––––––––––––––––––––––––– -->


  <div id="serviciosShow">
    <div class="contenedorServicios marginEntreSecciones"> 
      <div class="container">
        
        <div class="row">
          <div class="headerSubMenuServicios">
            <div class="two columns tituloServicios">
              <img src="images/nuevosArtes/btn_seccion/servicios.png"> 
            </div>

            <div class="ten columns subMenuAccionSocial servicios">
              <?php submenus(4); ?>
            </div>
          </div>

            <div class="contenedorTituloServicios">
              <div class="tituloContenedorServicios">
                <div class="textoTituloServicios">
                  <p id="subtitulo-subsubmenu-menu4" >La Residencia Universitaria de la Universidad La Salle es un espacio donde el estudio y el desarrollo personal son una prioridad</p>
                </div>
              </div>
            </div>
          </div>
        </div>

      <div class="ocultarContenidoSubMenu">
        <div class="contenedorImagenTextoServicios" style="position:relative">
            <div class="container" id="componentes-menu4">
				<div class="contenedorDiagonaTextoServicios"> 
					<p class="descripcionTituloServicios ws">Vivir en la Republica es una experiencia compartida. Por este motivo, el respeto y el orden con indispensables. La consideracion hacia las personas y las instalaciones, la empatia y el consumo responsable de los recursos, conforman la base para poder garantizar un espacio agradable y familiar que tiene como eje principal el estudio y el desarrollo personal de los residentes.</p>
					<div class="parrafoServicios ws">
						  <p>Las residentes pueden disfrutar de espacios creados especialmente para ello,pensando en sus necesidades y en su comodidad.</p>
						  <p>Todas las instalaciones de la residencia has sido creadas pensando en la comodidad y las necesidades de sus residentes.</p>
						  <h6>INSTALACIONES</h6>
						  <p>La residencia es una casa que fue acondicionada para poder ofrecer un lugar de descanso y estudio para aquellos estudiantes que deseen estar cerca de las instalaciones de la Univeridad.</p>
						  <p>Cuenta con una sala compartida para todos los residentes con televísor, un comedor con capacidad de 11 personas, área para la preparacion y conserva de alimentos, cuarto de servicio, terraza y un cuarto destinado para el personal de Limpieza y vigilancia que estará presente las 24 hrs del día.</p>
					</div>
				</div>
				<div class="" style="display:none" id="pines-estacionamiento">
					<div class="item-estacionamiento-e1">
						<img src="images/nuevosArtes/servicio/pine.png" class="" id="e1" onclick="Estacionamiento(this);" onmouseover="OverPinEstacionamiento(this)" onmouseout="NoOverPinEstacionamiento(this)">
						<span>E1</span>
					</div>
					<div class="item-estacionamiento-e2">
						<img src="images/nuevosArtes/servicio/pine.png" class="" id="e2" onclick="Estacionamiento(this);" onmouseover="OverPinEstacionamiento(this)" onmouseout="NoOverPinEstacionamiento(this)">
						<span>E2</span>
					</div>
					<div class="item-estacionamiento-e3">
						<img src="images/nuevosArtes/servicio/pine.png" class="" id="e3" onclick="Estacionamiento(this);" onmouseover="OverPinEstacionamiento(this)" onmouseout="NoOverPinEstacionamiento(this)">
						<span>E3</span>
					</div>
					<div class="item-estacionamiento-e4">
						<img src="images/nuevosArtes/servicio/pine.png" class="" id="e4" onclick="Estacionamiento(this);" onmouseover="OverPinEstacionamiento(this)" onmouseout="NoOverPinEstacionamiento(this)">
						<span>E4</span>
					</div>
					<div class="item-estacionamiento-e5">
						<img src="images/nuevosArtes/servicio/pine.png" class="" id="e5-e6-e7" onclick="Estacionamiento(this);" onmouseover="OverPinEstacionamiento(this)" onmouseout="NoOverPinEstacionamiento(this)">
						<span>E5</span>
					</div>
					<div class="item-estacionamiento-e6">
						<img src="images/nuevosArtes/servicio/pine.png" class="" id="e5-e6-e7" onclick="Estacionamiento(this);" onmouseover="OverPinEstacionamiento(this)" onmouseout="NoOverPinEstacionamiento(this)">
						<span>E6</span>
					</div>
					<div class="item-estacionamiento-e7">
						<img src="images/nuevosArtes/servicio/pine.png" class="" id="e5-e6-e7" onclick="Estacionamiento(this);" onmouseover="OverPinEstacionamiento(this)" onmouseout="NoOverPinEstacionamiento(this)">
						<span>E7</span>
					</div>
					<div class="item-estacionamiento-e8">
						<img src="images/nuevosArtes/servicio/pine.png" class="" id="e8" onclick="Estacionamiento(this);" onmouseover="OverPinEstacionamiento(this)" onmouseout="NoOverPinEstacionamiento(this)">
						<span>E8</span>
					</div>
					<div class="item-estacionamiento-e9">
						<img src="images/nuevosArtes/servicio/pine.png" class="" id="e9" onclick="Estacionamiento(this);" onmouseover="OverPinEstacionamiento(this)" onmouseout="NoOverPinEstacionamiento(this)">
						<span>E9</span>
					</div>
					<div class="item-estacionamiento-e10">
						<img src="images/nuevosArtes/servicio/pine.png" class="" id="e10" onclick="Estacionamiento(this);" onmouseover="OverPinEstacionamiento(this)" onmouseout="NoOverPinEstacionamiento(this)">
						<span style="left: 7px;top: -53px;">E10</span>
					</div>
				</div>
			</div>
			
        </div>
      </div>


        <div class="container">
            <div class="row seccion_3  ws imagenes-servicios">

                <!--<img src="images/nuevosArtes/video/video_universidad.png">-->
            </div>
        </div>

    </div> 
		<div class="container" id="slide-hospedaje" style="width: 100%;display:none">
		  <div id="slider-hospedaje" class="carousel slide" data-ride="carousel">
			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
			  <div class="item active">
				<img src="images/nuevosArtes/Noticias/image_news5.png" width="100%" height="345">
				<div class="carousel-caption">
				  <h3>Chania</h3>
				  <p>The atmosphere in Chania has a touch of Florence and Venice.</p>
				</div>
			  </div>
			  <div class="item">
				<img src="images/nuevosArtes/Noticias/image_news5.png" width="100%" height="345">
				<div class="carousel-caption">
				  <h3>Chania</h3>
				  <p>The atmosphere in Chania has a touch of Florence and Venice.</p>
				</div>
			  </div>
			</div>
		   <!-- Left and right controls -->
			<a class="left carousel-control" href="#slider-hospedaje" role="button" data-slide="prev">
			  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			  <span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#slider-hospedaje" role="button" data-slide="next">
			  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			  <span class="sr-only">Next</span>
			</a>
		  </div>
		</div>
		<div class="container" style="display:none">
			<a href="http://biblioteca.lasalle.mx" target="_blank" class="link-servicios btn btn-primary">IR A BIBLIOTECA</a>
		</div>
		<div class="container" style="display:none">
			<center id="descargables-ulsabus">
				<a href="#"><img src="images/nuevosArtes/servicio/reglamentos.png"></a>
				<a href="#"><img src="images/nuevosArtes/servicio/folletos.png"></a>
				<a href="#"><img src="images/nuevosArtes/servicio/reglamentos.png"></a>
				<a href="#"><img src="images/nuevosArtes/servicio/folletos.png"></a>
			</center>
		</div>
		<div class="container" style="display:none" id="mosaico-servicios">
			<div class="row">
				<section class="image-grid">
					<article class="image__cell is-collapsed">
						<div class="image--basic"><span class="titulos-cuadros-azulitos">Las Unidades principales</span>
							<a href="#expand-jump-0"><img id="expand-jump-0" class="basic__img" src="images/nuevosArtes/vida_estudiantil/1.jpg" alt="Fashion 8"></a>
							<div class="arrow--up"></div>
						</div>
						<div class="image--expand"><img class="image--large" src="images/nuevosArtes/vida_estudiantil/1.jpg" alt="Fashion 8">
							<div class="descripcion-cuadros-azules">Unidad I: unidad principal, acceso por Carlos B. Zetina y Francisco Murguía
								<br> Unidad II: acceso por Benjamín Hill
								<br> Unidad III: acceso por Francisco Murguía
								<br> Unidad IV: San Fernando, Tlalpan, D. F.
								<br> 
								<br> 
								<br> <br> 
								<span>
									Servicios e Infraestructura <br>C.P.Marco Antonio Valero Trejo<br>marco.valero@ulsa.mx
								</span>
							</div>
						</div>
					</article>
					<article class="image__cell is-collapsed">
						<div class="image--basic"><span class="titulos-cuadros-azulitos">Instalaciones y servicios</span>
							<a href="#expand-jump-0"><img id="expand-jump-0" class="basic__img" src="images/nuevosArtes/vida_estudiantil/2.jpg" alt="Fashion 8"></a>
							<div class="arrow--up"></div>
						</div>
						<div class="image--expand"><img class="image--large" src="images/nuevosArtes/vida_estudiantil/2.jpg" alt="Fashion 8">
							<div class="descripcion-cuadros-azules">6 auditorios y salas
								<br> 1 Teatro
								<br> 3 Salones de Usos Múltiples
								<br> 10 salas de cómputo
								<br> 20 Laboratorios especializados
								<br> 
								<br> 
								<br> <br> 
									Servicios e Infraestructura <br>C.P.Marco Antonio Valero Trejo<br>marco.valero@ulsa.mx
							</div>
						</div>
					</article>
					<article class="image__cell is-collapsed">
						<div class="image--basic"><span class="titulos-cuadros-azulitos">Instalaciones de formación</span>
							<a href="#expand-jump-0"><img id="expand-jump-0" class="basic__img" src="images/nuevosArtes/vida_estudiantil/3.jpg" alt="Fashion 8"></a>
							<div class="arrow--up"></div>
						</div>
						<div class="image--expand"><img class="image--large" src="images/nuevosArtes/vida_estudiantil/3.jpg" alt="Fashion 8">
							<div class="descripcion-cuadros-azules">Unidad Deportiva
								<br> Canchas de Fútbol Soccer y Rápido, Tenis, Voleibol, Basquetbol
								<br> Gimnasio de duela
								<br> Gimnasio de pesas
								<br> Alberca
								<br> 
								<br> 
								<br> <br> 
									Servicios e Infraestructura <br>C.P.Marco Antonio Valero Trejo<br>marco.valero@ulsa.mx
							</div>
						</div>
					</article>
					<article class="image__cell is-collapsed">
						<div class="image--basic"><span class="titulos-cuadros-azulitos">Servicios complementarios</span>
							<a href="#expand-jump-0"><img id="expand-jump-0" class="basic__img" src="images/nuevosArtes/vida_estudiantil/4.jpg" alt="Fashion 8"></a>
							<div class="arrow--up"></div>
						</div>
						<div class="image--expand"><img class="image--large" src="images/nuevosArtes/vida_estudiantil/4.jpg" alt="Fashion 8">
							<div class="descripcion-cuadros-azules">2 librerías
								<br> 4 cafeterías
								<br> Ananá Frozen Yogurt
								<br> Banco Santander
								<br> Lumen
								<br> 
								<br> 
								<br> <br> 
									Servicios e Infraestructura <br>C.P.Marco Antonio Valero Trejo<br>marco.valero@ulsa.mx
							</div>
						</div>
					</article>
					<article class="image__cell is-collapsed">
						<div class="image--basic"><span class="titulos-cuadros-azulitos">La Condesa, Barrio Universitario</span>
							<a href="#expand-jump-0"><img id="expand-jump-0" class="basic__img" src="images/nuevosArtes/vida_estudiantil/5.jpg" alt="Fashion 8"></a>
							<div class="arrow--up"></div>
						</div>
						<div class="image--expand"><img class="image--large" src="images/nuevosArtes/vida_estudiantil/5.jpg" alt="Fashion 8">
							<div class="descripcion-cuadros-azules">2 librerías
								<br> 4 cafeterías
								<br> Ananá Frozen Yogurt
								<br> Banco Santander
								<br> Lumen
								<br> 
								<br> 
								<br> <br> 
									Servicios e Infraestructura <br>C.P.Marco Antonio Valero Trejo<br>marco.valero@ulsa.mx
							</div>
						</div>
					</article>
				</section>
			</div>
		</div>
		<!-- Video -->
		<div id="video-container" style="position: relative;height: 467px !important;z-index: 4;display:none" class="container video-servicios">
			<video autoplay="" loop="" muted="" class="fillWidth">
				<source src="images/nuevosArtes/video/nuevos.mp4" type="video/mp4; codecs=&quot;avc1.42E01E, mp4a.40.2&quot;">
				<source src="video/trailer.webm" type="video/webm; codecs=&quot;vp8, vorbis&quot;"> Video not supported.
			</video>
		</div>
  </div>



  <!-- FIN SECCION SERVICIOS
  ––––––––––––––––––––––––––––––––––––––––––––––––– -->





  <!-- FIN SECCION  REPONSIVES SERVICIOS
  ––––––––––––––––––––––––––––––––––––––––––––––––– -->

  <div id="serviciosShowResponsive">
    <div class="contenedorInvestigacion marginEntreSecciones"> 
      <div class="container">
        
        <div class="row">
          <div class="subMenuResponsiveUniversidad">
            <div class="flechita">
              <select>
                <option value="">Biblioteca</option>
                <option value="">Editorial</option>
                <option value="">ULSABus</option>
                <option value="">Hospedaje para estudiantes</option>
                <option value="">Renta de instalaciones</option>
                <option value="">Centro Empresarial Teteda</option>
              </select>
            </div>
          </div>
        </div>

      </div>

      <div class="container_skew_text">
        <div class="contenedor_text_responsive_skew_left_principal">
          
          <div class="content_text_red">
            <p>La Residencia Universitaria de la Universidad La Salle es un espacio donde el estudio y el desarrollo personal son una prioridad</p>
          </div>
        </div>
                
        <div class="banner_seccion_responsive">
          <img src="images/nuevosArtes/servicio/servicios.png">
        </div>

        <div class="text_info_seccion_responsive">
          <div class="container">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquad,Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquad</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquad</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquad</p>
            <h6>INSTALACIONES</h6>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquadLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquad</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquad Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquad</p>

          </div>
        </div>


        <div class="title_seccion_responsive">
          <div class="container">
            <p>SERVICIOS</p>
          </div>
        </div>

        <div class="contenedor_text_responsive_skew_left">
          <div class="content_text_servicios_responsive_left">
            <h6 class="azulFuerte">Cocina</h6>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquad</p>                  
          </div>
        </div>

        <div class="contenedor_text_responsive_skew_right">
          <div class="content_text_servicios_responsive">
            <h6 class="azulClaro">Cuarto de <br><span class="azulFuerte">Lavado</span></h6>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquad</p>
          </div>
        </div>

        <div class="contenedor_text_responsive_skew_left">
          <div class="content_text_servicios_responsive_left">
            <h6 class="azulClaro"><span class="azulFuerte">Limpieza</span></h6>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquad</p>
          </div>
        </div>

        <div class="contenedor_text_responsive_skew_right">
          <div class="content_text_servicios_responsive">
            <h6 class="azulClaro"><span class="azulFuerte">Vigilancia</span></h6>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquad</p>
          </div>
        </div>

        <div class="contenedor_text_responsive_skew_left">
          <div class="content_text_servicios_responsive_left">
            <h6 class="azulClaro"><span class="azulFuerte">Internet</span></h6>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquad</p>
          </div>
        </div>

        <div class="title_seccion_responsive">
          <div class="container">
            <p>INFORMES</p>
          </div>
        </div>

        <div class="text_info_seccion_responsive">
          <div class="container">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquad,Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquad</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquad</p>
          </div>
        </div>

        <div class="text_info_seccion_student_international_responsive">
          <div class="container">
            <h6>Hospedaje para estudiantes<br>internacionales</h6>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquad,Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquad</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquad</p>
            <img src="images/nuevosArtes/images_responsive/boton_mas_hover_reponsive.png">
          </div>
        </div>  

      </div>
    </div>
  </div>


  

  <!-- FIN SECCION  REPONSIVES SERVICIOS
  ––––––––––––––––––––––––––––––––––––––––––––––––– -->




  <!-- SECCION VINCULACION
  ––––––––––––––––––––––––––––––––––––––––––––––––– -->

  <div id="vinculacionEmpresarialShow">
    <div class="contenedorEmpresarial marginEntreSecciones"> 
      <div class="container">
        
          <div class="headerSubMenuVinculacion">
            <div class="two columns tituloVinculacion">
              <img src="images/nuevosArtes/btn_seccion/vinculacion_empresarial.png"> 
            </div>
          </div>

        <div class="row">
			<div class="contenedorFilaVinculacion">
				<?php submenus(5); ?>
			</div>
		</div>
      </div> 
    </div> 
  </div>


  <!-- FIN SECCION VINCULACION
  ––––––––––––––––––––––––––––––––––––––––––––––––– -->





  <!-- SECCION  RESONSIVE VINCULACION
  ––––––––––––––––––––––––––––––––––––––––––––––––– -->

  <div id="vinculacionEmpresarialShowResponsive">
    <div class="contenedorInvestigacion marginEntreSecciones"> 
        
      <div class="container_skew_text">
        
        <div class="contenedor_text_responsive_skew_left">
          <div class="content_text_servicios_responsive_left">
            <h6 class="azulClaro">Incubadora <br><span class="azulFuerte textHelveticaBold"> de Negocios</span></h6>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquad</p>
            <a class="more_vinculacion_responsive azulClaro" href="#">Leér mas<img src="images/nuevosArtes/images_responsive/ver_mas.png"></a>
          </div>
        </div>
          
        <div class="contenedor_text_responsive_skew_right">
          <div class="content_text_servicios_responsive">
            <h6 class="azulClaro">Negocios <br><span class="azulFuerte textHelveticaBold">Familiares</span></h6>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquad</p>
            <a class="more_vinculacion_responsive azulClaro" href="#">Leér mas<img src="images/nuevosArtes/images_responsive/ver_mas.png"></a>
          </div>
        </div>


        <div class="contenedor_text_responsive_skew_left">
          <div class="content_text_servicios_responsive_left">
            <h6 class="azulClaro">Centro<br><span class="azulFuerte textHelveticaBold">de Pantentamiento</span></h6>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquad</p>
            <a class="more_vinculacion_responsive azulClaro" href="#">Leér mas<img src="images/nuevosArtes/images_responsive/ver_mas.png"></a>
          </div>
        </div> 

        <div class="contenedor_text_responsive_skew_right">
          <div class="content_text_servicios_responsive">
            <h6 class="azulClaro">Capacitación<br><span class="azulFuerte textHelveticaBold">Empresarial</span></h6>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquad</p>
              <a class="more_vinculacion_responsive azulClaro" href="#">Leér mas<img src="images/nuevosArtes/images_responsive/ver_mas.png"></a>
          </div>
        </div>

        <div class="contenedor_text_responsive_skew_left">
          <div class="content_text_servicios_responsive_left">
            <h6 class="azulClaro">Convenios<br><span class="azulFuerte">Empresariales</span></h6>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquad</p>
            <a class="more_vinculacion_responsive azulClaro" href="#">Leér mas<img src="images/nuevosArtes/images_responsive/ver_mas.png"></a>
          </div>
        </div>


        <div class="contenedor_text_responsive_skew_right">
          <div class="content_text_servicios_responsive">
            <h6 class="azulClaro">Bolsa de <br><span class="azulFuerte">Trabajo</span></h6>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquad</p>
          </div>
        </div>

        <div class="contenedor_text_responsive_skew_left">
          <div class="content_text_servicios_responsive_left">
            <h6 class="azulClaro">Prácticas<br><span class="azulFuerte">Profesionales</span></h6>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquad</p>
            <a class="more_vinculacion_responsive azulClaro" href="#">Leér mas<img src="images/nuevosArtes/images_responsive/ver_mas.png"></a>
          </div>
        </div> 

        <div class="contenedor_text_responsive_skew_right">
          <div class="content_text_servicios_responsive">
            <h6 class="azulClaro">Especialistas y<br><span class="azulFuerte">Conferencistas</span></h6>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquad</p>
            <a class="more_vinculacion_responsive azulClaro" href="#">Leér mas<img src="images/nuevosArtes/images_responsive/ver_mas.png"></a>
          </div>
        </div>
      
      </div>
    </div>
  </div>


  <!-- FIN SECCION  RESONSIVE VINCULACION
  ––––––––––––––––––––––––––––––––––––––––––––––––– -->




  <!-- SECCION  ACCION SOCIAL
  ––––––––––––––––––––––––––––––––––––––––––––––––– -->


  <div id="accionsocialShow">
    <div class="contenedorAccionSocial"> 
      <div class="container marginEntreSecciones">
        
        <div class="row">
          <div class="headerSubMenuAccionSocial">           
            <div class="two columns tituloAccionSocial">
              <img src="images/nuevosArtes/btn_seccion/accion-social.png"> 
            </div>
            
            <div class="ten subMenuAccionSocial accion">
				<?php submenus(6); ?>  	
            </div>
          </div>
            
          <div class="ocultarContenidoSubMenu">
            <div class="contenedorTituloAccionSocial">
              <div class="tituloContenedorAccionSocial">
                <div class="textoTituloAccionSocial">


                  <!--<h6>Centro Poeta</h6>-->
                  <p id="subtitulo-subsubmenu-menu6"></p>
                </div>
              </div>
            </div>
          </div>
        </div>
		<div class="container" id="componentes-menu6">
			
		</div>
		<div id="videos-accion-social" style="display:none">
			
		</div>
      </div>

        <div class="container">
            <div class="row seccion_6  ws ">



                <!--<img src="images/nuevosArtes/video/video_universidad.png">-->
            </div>
        </div>


    </div> 
  </div>




  <!-- FIN SECCION  ACCION SOCIAL
  ––––––––––––––––––––––––––––––––––––––––––––––––– -->



  <!-- SECCION RESPONSIVE ACCION SOCIAL
  ––––––––––––––––––––––––––––––––––––––––––––––––– -->

    <!-- Accion Social Responsive-->

    <div id="accionsocialShowResponsive">
      <div class="contenedorInvestigacion marginEntreSecciones"> 
        <div class="container">
          
          <div class="row">
            <div class="subMenuResponsiveUniversidad">
              <div class="flechita">
                <select>
                  <option value="">CADES</option>
                  <option value="">Despacho Probono</option>
                  <option value="">Regularizacion Academica</option>
                  <option value="">Centro Poeta</option>
                  <option value="">Impresión en braille</option>
                  <option value="">Centros Comunitarios</option>
                  <option value="">Consultorio medico</option>
                  <option value="">Servicio Social</option>
                  <option value="">Misiones</option>
                </select>
              </div>
            </div>
          </div>

        </div>
        
        <div class="container_skew_text">
          
          <div class="contenedor_text_responsive_skew_left_principal">
            <div class="content_text_red">
              <p>Centro Poeta<br>La Residencia Universitaria de la Universidad La Salle es un espacio donde el estudio y el desarrollo personal son una prioridad</p>
            </div>
          </div>
          
          <div class="banner_seccion_responsive">
            <img src="images/nuevosArtes/servicio/servicios.png">
          </div>
          
          <div class="text_info_seccion_responsive">
            <div class="container">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquad,Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquad</p>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquad</p>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquad</p>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquadLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquad</p>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquad Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquad</p>

            </div>
          </div>
          
          <div class="text_info_credit_seccion_responsive">
            <div class="container textHelveticaBold" >
              <p>Lorem ipsum dolor sit amet,ng elit,</p>
              <p>Lorem dolor sit amet, consectetur adipiscing elit,</p>
              <p>Lorem ipsum madipiscing elit,</p>
            </div>
          </div>

        </div>
      </div>
    </div>

  
  <!-- FIN SECCION RESPONSIVE ACCION SOCIAL
  ––––––––––––––––––––––––––––––––––––––––––––––––– -->

  <!--  SECCION INVESTIGACION
  ––––––––––––––––––––––––––––––––––––––––––––––––– -->

  <div id="investigacionShow">
    <div class="contenedorInvestigacion marginEntreSecciones"> 
      <div class="container">
        
        <div class="row">
          <div class="headerSubMenuInvestigacion esta-investigacion">
            <div class="two columns tituloVida">
              <img src="images/nuevosArtes/btn_seccion/investigacion.png">
            </div>
			<div class="ten columns subMenuInvestigacion servicios">
				<?php submenus(7); ?> 
			</div>
          </div>
        </div>

      </div>

      <div class="ocultarContenidoSubMenu">
        <div class="container" id="componentes-menu7">
          
			<button type="button" style="display:none" id="descargar-repositorio-investigacion" class="btn btn-primary">Ir a Repositorio</button>
        </div>
      </div>
    </div> 
  </div>


  <!-- FIN SECCION INVESTIGACION
  ––––––––––––––––––––––––––––––––––––––––––––––––– -->
  



  <!--  SECCION  RESPONSIVE  INVESTIGACION
  ––––––––––––––––––––––––––––––––––––––––––––––––– -->

  <div id="investigacionShowResponsive" class="investigacion_reponsive_container">
    <div class="container">
      
      <div class="row">
        <div class="investigacion_cuadro_responsive">   
          <p class="azulFuerte">PROYECTOS</p>
          <a href="#"><img src="images/nuevosArtes/images_responsive/ir.png"></a>
        </div>  
        
        <div class="investigacion_cuadro_responsive">
          <p class="azulFuerte">PUBLICACIONES</p>
          <a href="#"><img src="images/nuevosArtes/images_responsive/ir.png"></a>
        </div>
      </div>
      
      <div class="row">
        <div class="investigacion_cuadro_responsive">
          <p class="azulFuerte">PUBLICACIONES</p>
          <a href="#"><img src="images/nuevosArtes/images_responsive/ir.png"></a>
        </div>
      </div>

    </div> 
  </div>


  <!-- FIN SECCION  RESPONSIVE  INVESTIGACION
  ––––––––––––––––––––––––––––––––––––––––––––––––– -->




  <!-- SECCION CONTACTO
  ––––––––––––––––––––––––––––––––––––––––––––––––– -->

  <div id="contactoShow">
      <div class="contenedorContacto">
          <div class="container marginEntreSecciones">
              <div class="row">

                  <div class="headerSubMenuContacto">
                      <div class="two columns tituloContacto">
                          <img src="images/nuevosArtes/btn_seccion/contacto.png">
                      </div>

						<div class=" ten subMenuAccionSocial contacto">
							<div class="subMenuUnoAccionSocial" onclick="contacto(0)">Directorio de Atención al Publico</div>
							<div class="subMenuUnoAccionSocial" onclick="contacto(1)">Mapas</div>
							<div class="subMenuUnoAccionSocial" onclick="contacto(2)">¿Cómo llegar?</div>
							<div class="subMenuUnoAccionSocial" onclick="contacto(3)">Contacto</div>
						</div>
                  </div>

                  <div class="ten columns submenu">
                    <!-- <div class="cuadrosSubmenu">Contenido Uno</div>
                    <div class="cuadrosSubmenu">Contenido Uno</div>
                    <div class="cuadrosSubmenu">Contenido Uno</div>
                    <div class="cuadrosSubmenu">Contenido Uno</div> -->
                  </div>

                  
                  </div>

                  <div class="tituloContenedorContacto">
                      <div class="contenedorTituloContacto">
                          <div class="contenedorTituloContacto">
                              <div class="textoTituloContacto ">
                                  <p>Benjamin Franklin 47<br> Col. Condesa, Delg. Cuauhtémoc <br>México, DF, CP 06140 <br><span class="telefonoContacto">52 (55) 5278 9500</span></p>
                              </div>
                          </div>
                      </div>
                  </div>
				  <div class="row" id="overs-atencion">
						<div class="contenedorImagenTextoInvestigacion" style="background: url(&quot;images/nuevosArtes/banner/auditorioimg.jpg&quot;) no-repeat fixed;">
							<div class="container">
								<div class="contenedorDiagonalUniversidad2" id="scrollImagen2">
									<p class="descripcionTituloUniversidad- ws scroll-box-150">La Universidad La Salle es una institución de educación superior internacional e innovadora, con responsabilidad social, sentido humano, visión a futuro y más de cincuenta años de prestigio formando Profesionales con Valor. El modelo educativo de la Universidad La Salle, ha sido distinguido por la Secretaría de Educación Pública al otorgarle el título de Universidad de Excelencia con Liderazgo Nacional; contamos con la acreditación Lisa y Llana por parte de la Federación de Instituciones Mexicanas Particulares de Educación Superior (FIMPES) y cada programa de estudios está reconocido por el respectivo organismo acreditador; lo que certifica la calidad de los procesos administrativos, académicos, formativos y académico-administrativos. Formamos parte de, y de la Red de Universidades La Salle con 15 sedes en México y de La Salle Universities con 76 universidades en el mundo. Nuestra filosofía educativa está fundamentada en el carisma de Juan Bautista De La Salle, quien revolucionó la educación al contemplar el desarrollo de todas y cada una de las dimensiones del ser humano. Mantenemos una importante vinculación académica, de investigación y extensión con más de 100 universidades nacionales y extranjeras y pertenecemos a las asociaciones educativas y profesionales, nacionales e internacionales más importantes. En La Salle formamos profesionales con valor, impulsando su potencial para brindar soluciones innovadoras que transformen a México y el mundo con responsabilidad y compromiso social</p>
								</div>
							</div>
						</div>
						<div class="cuadros-grises-centrados"> 
							<div class="col-md-6"> <h5>CAS</h5><span><a href="mailto:luis@kreativeco.com">Enviar correo</a><br>55 0123 4568 </span></div>
						</div>
				  </div>
				  
					<div class="container" id="slide-mapas" style="width: 100%;">
					  <div id="slider-contacto" class="carousel slide" data-ride="carousel">
						<!-- Wrapper for slides -->
						<div class="carousel-inner" role="listbox">
						  <div class="item active">
							<img src="https://feriadelempleoverde.files.wordpress.com/2012/06/google-maps-mide1.jpg" alt="Chania" width="100%" height="345">
							<div class="carousel-caption">
							  <h3>Chania</h3>
							  <p>The atmosphere in Chania has a touch of Florence and Venice.</p>
							</div>
						  </div>
						  <div class="item">
							<img src="https://feriadelempleoverde.files.wordpress.com/2012/06/google-maps-mide1.jpg" alt="Chania" width="100%" height="345">
							<div class="carousel-caption">
							  <h3>Chania</h3>
							  <p>The atmosphere in Chania has a touch of Florence and Venice.</p>
							</div>
						  </div>
						</div>
					   <!-- Left and right controls -->
						<a class="left carousel-control" href="#slider-contacto" role="button" data-slide="prev">
						  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
						  <span class="sr-only">Previous</span>
						</a>
						<a class="right carousel-control" href="#slider-contacto" role="button" data-slide="next">
						  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
						  <span class="sr-only">Next</span>
						</a>
					  </div>
					</div>
				  <div class="container" style="display:none" id="mosaico-contacto">
						<div class="row">
							<section class="image-grid">
								<article class="image__cell is-collapsed">
									<div class="image--basic"><span class="titulos-cuadros-azulitos">Las Unidades principales</span>
										<a href="#expand-jump-0"><img id="expand-jump-0" class="basic__img" src="images/nuevosArtes/vida_estudiantil/1.jpg" alt="Fashion 8"></a>
										<div class="arrow--up"></div>
									</div>
									<div class="image--expand"><img class="image--large" src="images/nuevosArtes/vida_estudiantil/1.jpg" alt="Fashion 8">
										<div class="descripcion-cuadros-azules">Unidad I: unidad principal, acceso por Carlos B. Zetina y Francisco Murguía
											<br> Unidad II: acceso por Benjamín Hill
											<br> Unidad III: acceso por Francisco Murguía
											<br> Unidad IV: San Fernando, Tlalpan, D. F.
											<br> 
										</div>
									</div>
								</article>
								<article class="image__cell is-collapsed">
									<div class="image--basic"><span class="titulos-cuadros-azulitos">Instalaciones de formación</span>
										<a href="#expand-jump-0"><img id="expand-jump-0" class="basic__img" src="images/nuevosArtes/vida_estudiantil/3.jpg" alt="Fashion 8"></a>
										<div class="arrow--up"></div>
									</div>
									<div class="image--expand"><img class="image--large" src="images/nuevosArtes/vida_estudiantil/3.jpg" alt="Fashion 8">
										<div class="descripcion-cuadros-azules">Unidad Deportiva
											<br> Canchas de Fútbol Soccer y Rápido, Tenis, Voleibol, Basquetbol
											<br> Gimnasio de duela
											<br> Gimnasio de pesas
											<br> Alberca
											<br> 
										</div>
									</div>
								</article>
								<article class="image__cell is-collapsed">
									<div class="image--basic"><span class="titulos-cuadros-azulitos">Servicios complementarios</span>
										<a href="#expand-jump-0"><img id="expand-jump-0" class="basic__img" src="images/nuevosArtes/vida_estudiantil/4.jpg" alt="Fashion 8"></a>
										<div class="arrow--up"></div>
									</div>
									<div class="image--expand"><img class="image--large" src="images/nuevosArtes/vida_estudiantil/4.jpg" alt="Fashion 8">
										<div class="descripcion-cuadros-azules">2 librerías
											<br> 4 cafeterías
											<br> Ananá Frozen Yogurt
											<br> Banco Santander
											<br> Lumen
											<br> 
										</div>
									</div>
								</article>
								<article class="image__cell is-collapsed">
									<div class="image--basic"><span class="titulos-cuadros-azulitos">La Condesa, Barrio Universitario</span>
										<a href="#expand-jump-0"><img id="expand-jump-0" class="basic__img" src="images/nuevosArtes/vida_estudiantil/5.jpg" alt="Fashion 8"></a>
										<div class="arrow--up"></div>
									</div>
									<div class="image--expand"><img class="image--large" src="images/nuevosArtes/vida_estudiantil/5.jpg" alt="Fashion 8">
										<div class="descripcion-cuadros-azules">El campus universitario se encuentra en la Condesa, barrio en la zona centro, con gran cantidad de centros culturales, teatros, foros, cafés, librerías, restaurantes galerías y parques.
											<br> Forma parte del eje de mayor desarrollo de la capital, punto de encuentro para residentes y visitantes nacionales y extranjeros.
											<br> 
										</div>
									</div>
								</article>
							</section>
						</div>
					</div>
                <div class="descripcionTituloContacto ws scroll-box-150">
                    <div class="contactoShow">
                    <div class="contenedorDiagonalContacto">
                    <p></p>
                    </div>
                    </div>
					
              </div>
          </div>

          <div class="ocultarContenidoSubMenu">
              <div class="row">
                  <div class="containerMapa"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3763.0933688603855!2d-99.18326728521801!3d19.408371386898096!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0x3108b5797f9c9ecd!2sUniversidad+La+Salle+Unidad+Condesa!5e0!3m2!1ses!2smx!4v1464736984878" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe></div>
              </div>

              <div class="container">
                  <div class="row">

                      <form class="formContacto">
                          <input type="text" class="formNombre" placeholder="Nombre"><br>
                          <input type="email" class="formEmail" placeholder="Email">
                          <input type="tel" class="formTelefono" placeholder="Teléfono móvil"><br>
              <textarea class="comentario" placeholder="Comentario">
              </textarea><br>
                          <input class="enviar" type="submit" value="Enviar">
                      </form>

                  </div>
              </div>
          </div>

      </div>
  </div>
</div>




  <!-- FIN SECCION CONTACTO
  ––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <?php  
	// status del calendario
		$query = mysql_query("SELECT proyectos FROM micrositios where id=".$_GET["micrositio"]."");
		$results = mysql_fetch_array($query); mysql_free_result($query);
		$status_proyectos = $results['proyectos'];
		if($status_proyectos != 0){ ?>
		
  <div  class="container marginEntreSecciones">
    <div class="row">
		<div class="col-sm-2">
			<ul class="ul-cats">
				<?php
					function project(){
						$sql= "select * from micrositios_proyectos_categorias where id_micrositio=".$_GET["micrositio"]." order by orden asc";
						$query=mysql_query($sql) or die(mysql_error());
						while ($resultados = mysql_fetch_assoc($query)){
							$id_cat[] = $resultados["id"];
							$nombre_cat[] = $resultados["nombre"];
						}mysql_free_result($query);
						$i=0; while($i < count(@$id_cat)){
						unset($id_sub,$nombre_sub);
				?>
				<li class="li-cat">
					<h5><span><?php echo $nombre_cat[$i]; ?> <img src="images/nuevosArtes/banner/flechita.png" class="flechita-drop"></span></h5>
					<ul class="ul-subs">
						<?php
							$sql= "select * from micrositios_proyectos where id_categoria='".$id_cat[$i]."' order by orden asc";
							$query=mysql_query($sql) or die(mysql_error());
							while ($resultados = mysql_fetch_assoc($query)){
								$id_sub[] = $resultados["id"];
								$nombre_sub[] = $resultados["nombre"];
							}mysql_free_result($query);
							$k=0; while($k < count(@$id_sub)){ ?>
						<li class="li-sub" onclick="muestraproyecto(<?php echo $id_sub[$k]; ?>);"><span><?php echo $nombre_sub[$k]; ?></span></li>
							<?php	$k++; }	?>
					</ul>
				</li>
				<?php
					$i++; } } project();
				?>
			</ul>	
		</div>
		<div class="col-sm-10" id="aqui-proyecto">
			<div class="col-sm-12 col-md-12">
				<div class="col-sm-12 col-md-12">
					  <img class="mySlides intro-1" src="http://secure.kreativeco.com/kreativeco/images/portafolio/panel/3/2.jpg">
					  <img class="mySlides intro-2" src="http://secure.kreativeco.com/kreativeco/images/portafolio/panel/3/1.jpg">
					  <div class="w3-left w3-padding-left w3-hover-text-khaki" onclick="plusDivs(-1)">&#10094;</div>
					  <div class="w3-right w3-padding-right w3-hover-text-khaki" onclick="plusDivs(1)">&#10095;</div>
				</div>
				<div class="col-sm-12 col-md-12 miniaturas">
					<div class="col-sm-2 col-md-2">
						<img class="demo w3-opacity w3-hover-opacity-off miniatura-1" src="http://secure.kreativeco.com/kreativeco/images/portafolio/panel/3/2.jpg" onclick="currentDiv(1)">
					</div>
					<div class="col-sm-2 col-md-2">
						<img class="demo w3-opacity w3-hover-opacity-off miniatura-2" src="http://secure.kreativeco.com/kreativeco/images/portafolio/panel/3/1.jpg" onclick="currentDiv(2)">
					</div>
				</div>
				<div class="col-sm-12 col-md-12 parrafo-gris">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				</div>
				<div class="col-sm-12 col-md-12">
					<div id="estudiante2" class="cuadroEstudiante">
						<p>Archivo de ejemplo
							<br><span class="azul">Subtitulo</span></p>
						<a href="centro/archivos/ejemplo.png" target="_blank"><img src="images/nuevosArtes/aspirante/ir.png"></a>
					</div>
					<div id="estudiante2" class="cuadroEstudiante">
						<p>Archivo de ejemplo
							<br><span class="azul">Subtitulo</span></p>
						<a href="centro/archivos/ejemplo.png" target="_blank"><img src="images/nuevosArtes/aspirante/ir.png"></a>
					</div>
					<div id="estudiante2" class="cuadroEstudiante">
						<p>Archivo de ejemplo
							<br><span class="azul">Subtitulo</span></p>
						<a href="centro/archivos/ejemplo.png" target="_blank"><img src="images/nuevosArtes/aspirante/ir.png"></a>
					</div>
					<div id="estudiante2" class="cuadroEstudiante">
						<p>Archivo de ejemplo
							<br><span class="azul">Subtitulo</span></p>
						<a href="centro/archivos/ejemplo.png" target="_blank"><img src="images/nuevosArtes/aspirante/ir.png"></a>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>
  
<?php  }
	// status del calendario
		$query = mysql_query("SELECT slider FROM micrositios where id=".$_GET["micrositio"]."");
		$results = mysql_fetch_array($query); mysql_free_result($query);
		$status_slider = $results['slider'];
		if($status_slider != 0){ ?>
  <!-- SLIDER CON EFECTO Y BANNERS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->


  <div  class="container marginEntreSecciones">
    <div class="row">
	
	  <div id="myCarouselsalle" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner" role="listbox" style="max-height:400px;">
			<?php
		// Seleccionamos noticias
		function SliderNoticias(){
			$query = mysql_query("SELECT noticias_slider FROM micrositios where id=".$_GET["micrositio"]."");
			$results = mysql_fetch_array($query); mysql_free_result($query);
			$noticias_id = $results['noticias_slider'];
			if($noticias_id != NULL){
			$sql= "select * from noticias where id IN(".$noticias_id.") and estado=1 order by id asc";
			$query=mysql_query($sql) or die(mysql_error());
			while ($resultados = mysql_fetch_assoc($query)){
				$id[] = $resultados["id"];
				$titulo[] = $resultados["titulo"];
				$subtitulo[] = $resultados["subtitulo"];
				$imagen[] = $resultados["imagen"];
				$parrafo[] = $resultados["parrafo_1"];
			}mysql_free_result($query);
			$query = mysql_query("select color from micrositios where id=".$_GET["micrositio"]."");
			$results = mysql_fetch_array($query); mysql_free_result($query);
			$color = $results['color'];
				$i=0; while($i < count(@$id)){
		?>
		  <div class="item <?php if($i == 0){ echo 'active'; } ?>">
				<div class="container_pic_slider slide">
				  <div id="effect-1" class="effects clearfix responsiveSliderPrincipal"> 
					<div id="imgsn<?php echo $id[$i]; ?>" class="imgSliderNoticias"> 
					  
					  <div class="barra_gris textRight" style="background:url(images/colores/<?php echo $color; ?>/barra_gris_micro.png)"> 
						<div id="ctds1" class="container_titulo_description_slider"> 
							<p style="font-size:150%;"><?php echo $titulo[$i]; ?></p>
							<p><?php echo $subtitulo[$i]; ?></p>
							<button id="btn_more_slider_one" class="btn_slider"><a href="notice.php?noticia=<?php echo $id[$i]; ?>"><img src="images/nuevosArtes/banner/icono_mas.png" style="height: 30px;width: 30px;"></a></button>
						</div>
					  </div>
					  <img src="noticias/img/<?php echo $imagen[$i]; ?>">
					</div>
				  </div>
				</div>
		  </div>
		  <?php 	$i++; } 
			} ?>
		</div>
		<?php if($noticias_id != NULL){ ?>
		<ol class='carousel-indicators'>
		<?php $i=0; while($i < count(@$id)){ ?>
			<!-- Indicators -->
			
				<li data-target='#myCarouselsalle' data-slide-to='<?php echo $i; ?>' <?php if($i == 0){ ?>class='active' <?php } ?>><img src='http://placehold.it/100x50&text=Noticia <?php echo $i; ?>' alt='' /></li>
			
		<?php $i++; } ?>
		</ol>
		<?php } } SliderNoticias(); ?>
    </div>
  </div>
</div>


  <!-- FIN SLIDER CON EFECTO Y BANNERS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
<?php } ?>


  <!-- SECCIONES RESPONSIVA CONTACTO
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->


  <div id="contactoShowResponsive">
    <div class="contenedorInvestigacion marginEntreSecciones"> 
      <div class="container">
        
        <div class="row">
          
          <div class="subMenuResponsiveUniversidad">
            <div class="flechita">
              <select>
                <option value="">Contacto</option>
                <option value="">Direccion de Atención al Publico</option>
                <option value="">Organigrama<br>Institucional</option>
                <option value="">Mapas</option>
                <option value="">Como Llegar</option>
                <option value="">Redes Sociales Institucionales</option>
              </select>
            </div>
          </div>
          
          <div class="container_skew_text">
            <div class="contenedor_text_responsive_skew_left_principal">
              <div class="content_text_red">
                <p>Benjamin Franklin 47<br> Col. Condesa, Delg. Cuauhtémoc <br>México, DF, CP 06140 <br><span class="telefonoContacto">52 (55) 5278 9500</span></p>
              </div>
            </div>
            
            <div class="container_map_responsive">
              <img src="">
            </div>
          </div>

          <div class="title_ubicacion_responsive">
            <p class="azulLaSalle textHelveticaBold">Cómo llegar</p>
          </div>
          
          <div class="fomulario_contacto_responsive">
            <p>Contáctanos</p>
            <input class="" type="text" placeholder="Nombre">
            <input class="" type="email" placeholder="Email">
            <input class="" type="text" placeholder="Teléfono">
            <textarea class="text_area" type="text" placeholder="Comentarios"></textarea>
            <input class="submit_form_responsive" type="submit" value="Enviar">
          </div>

        </div>

      </div>
    </div>
  </div> 

  <div class="container marginResponsive titulosResponsive">
    <div class="row">
      <div class="three columns title_calendar">
        <p class="azulLaSalle"><img src="images/nuevosArtes/images_responsive/icono_noticia.png"> Noticias</p>
      </div>
    </div>
  </div>

    
  <div  class="container marginEntreSecciones">
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
  <div id="cinnn" class="carousel-inner" role="listbox">
  </div>   
  </div>

  <div class="container marginEntreSecciones" id="desc_slider">
    <div class="row">   
      <div class="desc">
        <div class="title_desc">
          <h3>XXIX FERIA DE POSGRADOS</h3>
        </div>
      </div> 
      
      <div class="desc_desc">
        <p>"23647234t7236 Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.... "<a href="notice.html" target="NEW">Ver más ..</a></p>
      </div>
    </div>
  </div>


  


    <br /><br /><br />

<?php 
	// status del calendario
		$query = mysql_query("SELECT noticias FROM micrositios where id=".$_GET["micrositio"]."");
		$results = mysql_fetch_array($query); mysql_free_result($query);
		$status_noticias = $results['noticias'];
		if($status_noticias != 0){ ?>
  
  <div class="container marginEntreSecciones noticias">
    
    <div id="effect-1" class="effects clearfix"> 
    <?php
		// Seleccionamos noticias
		
		function noticiasPrincipales($tono){
			$query = mysql_query("SELECT noticias_noticias FROM micrositios where id=".$_GET["micrositio"]."");
			$results = mysql_fetch_array($query); mysql_free_result($query);
			$noticias_id = $results['noticias_noticias'];
			if($noticias_id != NULL){
			$sql= "select * from noticias where id IN(".$noticias_id.") and estado=1 order by id asc";
			$query=mysql_query($sql) or die(mysql_error());
			while ($resultados = mysql_fetch_assoc($query)){
				$id[] = $resultados["id"];
				$titulo[] = $resultados["titulo"];
				$subtitulo[] = $resultados["subtitulo"];
				$imagen[] = $resultados["imagen"];
				$parrafo[] = $resultados["parrafo_1"];
			}mysql_free_result($query);
			$query = mysql_query("select color from micrositios where id=".$_GET["micrositio"]."");
		$results = mysql_fetch_array($query); mysql_free_result($query);
		$color = $results['color'];
		$tono = null;
		if($color == 1){$tono = '#083a81';}
		if($color == 2){$tono = '#2a7fb8';}
		if($color == 3){$tono = '#818d8d';}
		if($color == 4){$tono = '#7537c3';}
		if($color == 5){$tono = '#eb740f';}
		if($color == 6){$tono = '#292929';}
		if($color == 7){$tono = '#bf3a29';}
		if($color == 8){$tono = '#019a34';}
				$i=0; while($i < count(@$id)){
		?>
      <div class="imgNoticia">
        <div class="overlayTituloNoticia">
          <h6><?php echo $titulo[$i]; ?></h6>
        </div>
        <img src="noticias/img/<?php echo $imagen[$i]; ?>" alt="">
        <div class="overlayNoticia"  style="background:<?php echo $tono; ?>">
          <h6><?php echo $subtitulo[$i]; ?></h6>
          <p><?php echo $parrafo[$i]; ?></p>
           <a id="btn_more_noticia_uno" class="btn_noticia_more" href="notice.php?noticia=<?php echo $id[$i]; ?>"><img src="images/nuevosArtes/Noticias/icon_more.png"></a>
        </div>

      </div>
	  
	  <?php 	$i++; } 
			} } noticiasPrincipales(); ?>
     
    </div>

  </div>

  <!-- FIN SECCION NOTICIAS 
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    
<?php  }
	// status del calendario
		$query = mysql_query("SELECT slider2 FROM micrositios where id=".$_GET["micrositio"]."");
		$results = mysql_fetch_array($query); mysql_free_result($query);
		$status_slider = $results['slider2'];
		if($status_slider != 0){ ?>


 <div  class="container marginEntreSecciones">
    <div class="row">
	
	  <div id="myCarousel" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner" role="listbox" style="max-height:280px;">
			<?php
		// Seleccionamos noticias
		function SliderNoticias2(){
			$query = mysql_query("SELECT noticias_slider2 FROM micrositios where id=".$_GET["micrositio"]."");
			$results = mysql_fetch_array($query); mysql_free_result($query);
			$noticias_id = $results['noticias_slider2'];
			if($noticias_id != NULL){
			$sql= "select * from noticias where id IN(".$noticias_id.") and estado=1 order by id asc";
			$query=mysql_query($sql) or die(mysql_error());
			while ($resultados = mysql_fetch_assoc($query)){
				$id[] = $resultados["id"];
				$titulo[] = $resultados["titulo"];
				$subtitulo[] = $resultados["subtitulo"];
				$imagen[] = $resultados["imagen"];
				$parrafo[] = $resultados["parrafo_1"];
			}mysql_free_result($query);
			$query = mysql_query("select color from micrositios where id=".$_GET["micrositio"]."");
			$results = mysql_fetch_array($query); mysql_free_result($query);
			$color = $results['color'];
				$i=0; while($i < count(@$id)){
		?>
		  <div class="item <?php if($i == 0){ echo 'active'; } ?>">
				<div class="container_pic_slider slide">
				  <div id="effect-1" class="effects clearfix responsiveSliderPrincipal"> 
					<div id="imgsn<?php echo $id[$i]; ?>" class="imgSliderNoticias"> 
					  <div class="barra_morada" style="top:-34%;background:url(images/colores/<?php echo $color; ?>/barra_morada2.png)">
						<div class="container_imagen_titulo_section_slider">
						  <div class="caption cap1">
							<a href="notice.php?noticia=<?php echo $id[$i]; ?>">
							  <img src="images/nuevosArtes/banner/icono_noticias.png">
							</a>
						  </div>
						  <p>NOTICIAS
						  </p> 
						</div>
					  </div>
					  <div class="barra_gris2 textRight"> 
						<div id="ctds1" class="container_titulo_description_slider"> 
							<p style="font-size:150%;"><?php echo $titulo[$i]; ?></p>
							<p><?php echo $subtitulo[$i]; ?></p>
							<button id="btn_more_slider_one" class="btn_slider"><a href="notice.php?noticia=<?php echo $id[$i]; ?>"><img src="images/nuevosArtes/banner/icono_mas.png" style="height: 30px;width: 30px;"></a></button>
						</div>
					  </div>
					  <img src="noticias/img/<?php echo $imagen[$i]; ?>">
					</div>
				  </div>
				</div>
		  </div>
		  <?php 	$i++; } 
			} ?>
		</div>
		
		<?php  } SliderNoticias2(); ?>
    </div>
  </div>
</div>

<?php } ?>
<?php 
	// status del calendario
		$query = mysql_query("SELECT noticias2 FROM micrositios where id=".$_GET["micrositio"]."");
		$results = mysql_fetch_array($query); mysql_free_result($query);
		$status_noticias = $results['noticias2'];
		if($status_noticias != 0){ ?>

<div class="container marginEntreSecciones noticias">
    
    <div id="effect-1" class="effects clearfix"> 
    <?php
		
		// Seleccionamos noticias
		function noticiasPrincipales2($tono){
			$query = mysql_query("SELECT noticias_noticias2 FROM micrositios where id=".$_GET["micrositio"]."");
			$results = mysql_fetch_array($query); mysql_free_result($query);
			$noticias_id = $results['noticias_noticias2'];
			if($noticias_id != NULL){
			$sql= "select * from noticias where id IN(".$noticias_id.") and estado=1 order by id asc";
			$query=mysql_query($sql) or die(mysql_error());
			while ($resultados = mysql_fetch_assoc($query)){
				$id[] = $resultados["id"];
				$titulo[] = $resultados["titulo"];
				$subtitulo[] = $resultados["subtitulo"];
				$imagen[] = $resultados["imagen"];
				$parrafo[] = $resultados["parrafo_1"];
			}mysql_free_result($query);
			$query = mysql_query("select color from micrositios where id=".$_GET["micrositio"]."");
		$results = mysql_fetch_array($query); mysql_free_result($query);
		$color = $results['color'];
		$tono = null;
		if($color == 1){$tono = '#083a81';}
		if($color == 2){$tono = '#2a7fb8';}
		if($color == 3){$tono = '#818d8d';}
		if($color == 4){$tono = '#7537c3';}
		if($color == 5){$tono = '#eb740f';}
		if($color == 6){$tono = '#292929';}
		if($color == 7){$tono = '#bf3a29';}
		if($color == 8){$tono = '#019a34';}
				$i=0; while($i < count(@$id)){
		?>
      <div class="imgNoticia">
        <div class="overlayTituloNoticia">
          <h6><?php echo $titulo[$i]; ?></h6>
        </div>
        <img src="noticias/img/<?php echo $imagen[$i]; ?>" alt="">
        <div class="overlayNoticia" style="background:<?php echo $tono; ?>">
          <h6><?php echo $subtitulo[$i]; ?></h6>
          <p><?php echo $parrafo[$i]; ?></p>
           <a id="btn_more_noticia_uno" class="btn_noticia_more" href="notice.php?noticia=<?php echo $id[$i]; ?>"><img src="images/nuevosArtes/Noticias/icon_more.png"></a>
        </div>

      </div>
	  
	  <?php 	$i++; } 
			} } noticiasPrincipales2(); ?>
     
    </div>

  </div>
<?php } ?>
  <!-- CONTENIDO DE NOTICIA SELECCIONADA
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
<!-- 
  <div class="container marginEntreSecciones" id="desc_noticias">   
    <div class="row">
      
      <div class="desc">
        <div class="title_desc">
          <h3>XXIX FERIA DE POSGRADOS</h3>
        </div>
      </div> 
      
      <div class="desc_desc">
        <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>

        <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
      </div> 
    
    </div>
  </div> -->

  <!-- FIN CONTENIDO DE NOTICIA SELECCIONADA
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->




  <br /><br /><br />
  
  
<?php 
	// status del calendario
		$query = mysql_query("SELECT calendario FROM micrositios where id=".$_GET["micrositio"]."");
		$results = mysql_fetch_array($query); mysql_free_result($query);
		$status_calendario = $results['calendario'];
		if($status_calendario != 0){ ?>
  <!-- TITULO CONTENIDO CALENDARIO
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->

  <div  id="title_calendario" class="container marginApartirDeCalendario">
    <div class="row">
        <div class="three columns title_calendar">
          <p><img src="images/nuevosArtes/calendario/calendario.png"> Calendario</p>
        </div>
    </div>
  </div>

  <!-- FIN TITULO CONTENIDO CALENDARIO
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->

  <!-- CONTENIDO CALENDARIO
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->

  <div class="container " id="calendario">
    <div class="menuCalendario">

      <div class="two columns menuSecciones">  
        <ul>
		<?php
		function CategoriasCalendario(){
			$sql= "select * from noticias_categorias where es_calendario=1";
			$query=mysql_query($sql) or die(mysql_error());
			while ($resultados = mysql_fetch_assoc($query)){
				$id[] = $resultados["id"];
				$nombre[] = $resultados["nombre"];
			}mysql_free_result($query);
		$i=0; while($i < count(@$id)){	?>
          <li><button class="seccion academicas" id="openUno" onclick="MostrarEventosCategoria(<?php echo $id[$i]; ?>)"><?php echo $nombre[$i]; ?></button></li>
		  <?php	$i++; } } CategoriasCalendario(); ?>
        </ul>
      </div>

      <div class="ten columns contenedorFilasNoticias">

        <div class="four columns menuNoticiaCalendario">  

          <div class="responsive-calendar">

            <div class="controls">
              <a class="pull-left" data-go="prev"><div class="btn"><img class="fechas" src="images/nuevosArtes/left_arrow.png"></div></a>
              <h4><span data-head-month></span></h4>
              <a class="pull-right" data-go="next"><div class="btn"><img class="fechas" src="images/nuevosArtes/right_arrow.png"></div></a>
            </div>

            <div class="day-headers">
              <div class="day header">L</div>
              <div class="day header">M</div>
              <div class="day header">M</div>
              <div class="day header">J</div>
              <div class="day header">V</div>
              <div class="day header">S</div>
              <div class="day header">D</div>
            </div>
            
            <div class="days" data-group="days"></div>

          </div>
        </div>

        <div class="eight columns menuCalendario">  
          <div id="effect-3" class="effects clearfix eventos-dinamicos">   

		  <!--
			<div class="imgSliderNoticias not-pinocho imgNoticiaCalendario">
				<div class="overlayCalendarioFecha">
					<div class="text_btn_event_calendario">
						<div class="row">
							<div class="date_event_calendario_responsive">
								<p class="fecha-cal"><span class="dia-cal">23</span> Agosto 2015</p>
							</div>
						</div>
					</div>
				</div>
				<img src="images/nuevosArtes/calendario/calendario_imagenes.jpg" title="Hola" class="img-post">
				<img src="images/nuevosArtes/calendario/cal.png" class="iconito-cal">
				<div class="overlayCalendario22" style="display:none;">
					<span class="tituloCalendario2">Rehabilitación de jardín aledaño a La Salle<br>Lugar: <br>01/07/2016 5:00:00 pm<br> Descripción: </span>
					<br>
					<a href="#"><img src="images/nuevosArtes/calendario/calr.png" class="img-link-cal"></a>
				</div>
			</div>
          -->  
			
		<!--
            <div class="imgNoticiaCalendario2">
              <div id="tituloCalendario2" class="overlayTituloCalendario">
                <p><span class="title">Obra de teatro: Pinocho<br>por Tania Ruiz</span></p>
              </div>
              <img src="images/nuevosArtes/calendario/calendario_imagenes2.jpg" class="img-post" alt="" style="max-width: 288px">
              <div class="overlayCalendario">
                  <a href="javascript: void(0)" onclick="open_modal(this)" >
                      <br/>
                      <span class="tituloCalendario">Obra de teatro: <br><span>El lobo</span><br>por Tania Ruiz</span>
                      <br/>
                      <img src="images/nuevosArtes/calendario/boton_mas_hover.png" style="width: 30px; position: relative; right: -236px;">
                  </a>
              </div>
            </div>
		-->

          </div>     
        </div>

      </div>

  
    </div>
  </div>

  <!-- FIN CONTENIDO CALENDARIO
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->


<?php } ?>




  <!-- TTITULO SECCION CALENDARIO RESPONSIVE
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->

  <div class="container marginResponsive titulosResponsive">
    <div class="row">
      <div class="three columns title_calendar">
        <p class="azulLaSalle"><img src="images/nuevosArtes/calendario/calendario.png"> Calendario</p>
      </div>
    </div>
  </div>

  <!-- FIN TTITULO SECCION CALENDARIO RESPONSIVE
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->








  <!-- SECCION RESPONSIVE CALENDARIO RESPONSIVE
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->

  <div class="container marginApartirDeCalendario rest" id="calendarioResponsive" >
    <div class="row">
      
      <div class="slider2">
       		
		<?php CalendarioResponsive(); ?>

      </div>
    </div>
  </div>

  <!-- FIN  SECCION RESPONSIVE CALENDARIO RESPONSIVE
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->

<?php 
	// status de tabs
		$query = mysql_query("SELECT tabs FROM micrositios where id=".$_GET["micrositio"]."");
		$results = mysql_fetch_array($query); mysql_free_result($query);
		$status_tabs = $results['tabs'];
		if($status_tabs != 0){ ?>
		
  <div class="container">
  <ul class="nav nav-tabs tabs-micro">
<?php 		$sql= "select * from micrositios_tabs where id_micrositio=".$_GET["micrositio"]." order by orden asc";
			$query=mysql_query($sql) or die(mysql_error());
			while ($resultados = mysql_fetch_assoc($query)){
				$id_tabs[] = $resultados["id"];
				$nombre_tabs[] = $resultados["nombre"];
				$texto_tabs[] = $resultados["contenido"];
			}mysql_free_result($query);
			$i=0; while($i < count(@$id_tabs)){ ?>
    <li <?php if($i == 0){ ?> class="active" <?php } ?>><a href="#tab-<?php echo $i + 1; ?>"><?php echo $nombre_tabs[$i]; ?></a></li>
	<?php $i++; } ?>
  </ul>

  <div class="tab-content tabs-componente">
	<?php $i=0; while($i < count(@$id_tabs)){ ?>
    <div id="tab-<?php echo $i + 1; ?>" class="tab-pane fade <?php if($i == 0){ ?> in active<?php } ?>">
      <h3><?php echo $nombre_tabs[$i]; ?></h3>
      <p><?php echo $texto_tabs[$i]; ?></p>
    </div>
	<?php $i++; } ?>
  </div>
</div>
<?php } ?>

<?php 
	// status de directorio
		$query = mysql_query("SELECT directorio FROM micrositios where id=".$_GET["micrositio"]."");
		$results = mysql_fetch_array($query); mysql_free_result($query);
		$status_directorio = $results['directorio'];
		if($status_directorio != 0){ 
		
		// facultad a generar
			$query = mysql_query("SELECT directorio_facultad FROM micrositios where id=".$_GET["micrositio"]."");
			$results = mysql_fetch_array($query); mysql_free_result($query);
			$facu = $results['directorio_facultad'];
		?>

<div class="container">
	<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-6">
			<form class="formSearch" action="microsites/catalogo_2.php" method="get">
				<input id="buscar" name="buscar" value="" class="search" type="text" list="postdl" placeholder="Nombre, Facultad, Especialidad" style="font-style: inherit;">  
				<input id="buscarpost" class="submit" type="submit" value="">
			</form>
		</div>
		<div class="col-md-3"></div>
		<?php
			$sql= "select * from catalogo_docente where facultad='".$facu."' order by id asc";
			$query=mysql_query($sql) or die(mysql_error());
			while ($resultados = mysql_fetch_assoc($query)){
				$id_docente[] = $resultados["id"];
				$nombre_docente[] = $resultados["nombre"];
				$email_docente[] = $resultados["email"];
				$facultad_docente[] = $resultados["facultad"];
				$especialidad_docente[] = $resultados["especialidad"];
				$telefono_docente[] = $resultados["telefono"];
				$extension_docente[] = $resultados["extension"];
			}mysql_free_result($query);
			
			$query = mysql_query("select nombre from catalogo_facultades where id=".$facu." limit 1");
			$results = mysql_fetch_array($query); mysql_free_result($query);
			$nombre_facu = $results['nombre'];
			
			$i=0; while($i < count(@$id_docente)){ ?>
	<!-- WHILE -->	
		<div class="col-md-6">
			<div class="barra-superior-micro" style="background:<?php echo $tono; ?>">
				<h5><?php echo $nombre_docente[$i]; ?></h5>
			</div>
			<div class="contenido-directorio-micro">
				Facultad: <?php echo $nombre_facu; ?><br>
				Especialidad: <?php echo $especialidad_docente[$i]; ?><br>
				Telefono: <?php echo $telefono_docente[$i]; ?> Ext: <?php echo $extension_docente[$i]; ?><br>
				Email: <?php echo $email_docente[$i]; ?><br>
			</div>
		</div>
	<!-- WHILE -->		
		<?php $i++; } ?>
	</div>
</div>
<?php } ?>
  <br /><br /><br />



  <!-- SECCION ACCESO DIRECTO 
  ––––––––––––––––––––––––––––––––––––––––––––––––– -->

  <!--<div class="container marginApartirDeCalendario">
    <section class="accesoDirecto">
      <div class="row">

        <div class="four columns">
          <a href=""><img src="images/nuevosArtes/accesoDirecto/Button_Egresados_blue.png" onmouseover="this.src='images/nuevosArtes/accesoDirecto/Button_Egresados_red.png'" onmouseout="this.src='images/nuevosArtes/accesoDirecto/Button_Egresados_blue.png'"></a><br>
          <img src="images/nuevosArtes/accesoDirecto/texto_actualizacion_egresados.png">
        </div>

        <div class="four columns">
          <a href=""><img src="images/nuevosArtes/accesoDirecto/Button_Fichas_blue.png" onmouseover="this.src='images/nuevosArtes/accesoDirecto/Button_Fichas_red.png'" onmouseout="this.src='images/nuevosArtes/accesoDirecto/Button_Fichas_blue.png'"></a><br>
          <img src="images/nuevosArtes/accesoDirecto/texto_descarga_ficha.png">
        </div>

        <div class="four columns">
          <a href=""><img src="images/nuevosArtes/accesoDirecto/Button_Pagos_Blue.png" onmouseover="this.src='images/nuevosArtes/accesoDirecto/Button_Pagos_red.png'" onmouseout="this.src='images/nuevosArtes/accesoDirecto/Button_Pagos_Blue.png'"></a><br>
          <img src="images/nuevosArtes/accesoDirecto/texto_pagos-por-internet.png">
        </div>

      </div>
    </section>
  </div>-->
<?php
	$query = mysql_query("select color from micrositios where id=".$_GET["micrositio"]."");
	$results = mysql_fetch_array($query); mysql_free_result($query);
	$color = $results['color'];
?>

  <div class="container marginApartirDeCalendario circle">
    <section class="accesoDirecto">
      <div class="row">
        <div class="two columns">
          <div class="primerCirculo">
            <a href="http://delasalle.ulsa.edu.mx/actualizacion/login.aspx" target="NEW"><img src="images/nuevosArtes/accesoDirecto/Button_Egresados_blue.png" onmouseover="this.src='images/colores/<?php echo $color; ?>/Button_Egresados_red2.png'" onmouseout="this.src='images/nuevosArtes/accesoDirecto/Button_Egresados_blue.png'"></a><br>
            <img src="images/nuevosArtes/accesoDirecto/texto_actualizacion_egresados.png">
          </div>
        </div>
        <div class="two columns">
          <div class="segundoCirculo">
            <a href="http://sgu.ulsa.edu.mx/psulsa/" target="NEW"><img src="images/nuevosArtes/accesoDirecto/Button_Fichas_blue.png" onmouseover="this.src='images/colores/<?php echo $color; ?>/Button_Fichas_red2.png'" onmouseout="this.src='images/nuevosArtes/accesoDirecto/Button_Fichas_blue.png'"></a><br>
            <img src="images/nuevosArtes/accesoDirecto/texto_descarga_ficha.png">
          </div>
        </div>

        <div class="two columns ">
          <div class="primerCirculo">
            <a href="http://sgu.ulsa.edu.mx/psulsa/" target="NEW"><img src="images/nuevosArtes/accesoDirecto/Button_Pagos_Blue.png" onmouseover="this.src='images/colores/<?php echo $color; ?>/Button_Pagos_red2.png'" onmouseout="this.src='images/nuevosArtes/accesoDirecto/Button_Pagos_Blue.png'"></a><br>
            <img src="images/nuevosArtes/accesoDirecto/texto_pagos-por-internet.png">
          </div>
        </div>
        <div class="two columns ">
          <a href="http://delasalle.ulsa.mx/pagos/" target="NEW"><img src="images/nuevosArtes/accesoDirecto/Button_Facturacion_Blue.png" onmouseover="this.src='images/colores/<?php echo $color; ?>/Button_Facturacion_red2.png'" onmouseout="this.src='images/nuevosArtes/accesoDirecto/Button_Facturacion_Blue.png'"></a><br>
          <img src="images/nuevosArtes/accesoDirecto/texto_factura-electronica.png">
        </div>

        <div class="two columns">
          <a href="http://lasalle.mx/profesionalesconvalor/" target="NEW"><img src="images/nuevosArtes/accesoDirecto/Button_profesionales.png" onmouseover="this.src='images/colores/<?php echo $color; ?>/Button_profesionales_red2.png'" onmouseout="this.src='images/nuevosArtes/accesoDirecto/Button_profesionales.png'"></a><br>
          <img src="images/nuevosArtes/accesoDirecto/texto_profesionales.png">
        </div>

        <div class="two columns">
          <a href="http://hoy.lasalle.mx/nuevo-portal-de-pagos-en-linea/" target="NEW"><img src="images/nuevosArtes/accesoDirecto/Button_todo-en-1.png" onmouseover="this.src='images/nuevosArtes/accesoDirecto/Button_todo-en-1_OVER.png'" onmouseout="this.src='images/nuevosArtes/accesoDirecto/Button_todo-en-1.png'"></a><br>
          <img src="images/nuevosArtes/accesoDirecto/texto_todo.png">
        </div>

      </div>
    </section>
  </div>

  <!-- FIN SECCION ACCESO DIRECTO 
  ––––––––––––––––––––––––––––––––––––––––––––––––– -->









  <!-- SECCION RESPONSIVE ACCESOS DIRECTO 
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->

  <div class="container marginApartirDeCalendario">
    <section class="accesoDirectoResponsive">
      <div class="row">      
        <div class="two columns">
          <div class="primerCirculo">
            <a href="http://delasalle.ulsa.edu.mx/actualizacion/login.aspx" target="NEW"><img src="images/nuevosArtes/accesoDirecto/Button_Egresados_blue.png" onmouseover="this.src='images/nuevosArtes/accesoDirecto/Button_Egresados_red2.png'" onmouseout="this.src='images/nuevosArtes/accesoDirecto/Button_Egresados_blue.png'"></a><br>
            <img src="images/nuevosArtes/accesoDirecto/texto_actualizacion_egresados.png">
          </div>

          <div class="segundoCirculo">
            <a href="http://sgu.ulsa.edu.mx/psulsa/" target="NEW"><img src="images/nuevosArtes/accesoDirecto/Button_Fichas_blue.png" onmouseover="this.src='images/nuevosArtes/accesoDirecto/Button_Fichas_red2.png'" onmouseout="this.src='images/nuevosArtes/accesoDirecto/Button_Fichas_blue.png'"></a><br>
            <img src="images/nuevosArtes/accesoDirecto/texto_descarga_ficha.png">
          </div>
        </div>

        <div class="two columns">
          <div class="primerCirculo">
            <a href="http://sgu.ulsa.edu.mx/psulsa/" target="NEW"><img src="images/nuevosArtes/accesoDirecto/Button_Pagos_Blue.png" onmouseover="this.src='images/nuevosArtes/accesoDirecto/Button_Pagos_red2.png'" onmouseout="this.src='images/nuevosArtes/accesoDirecto/Button_Pagos_Blue.png'"></a><br>
            <img src="images/nuevosArtes/accesoDirecto/texto_pagos-por-internet.png">
          </div>

          <div class="segundoCirculo">
            <a href="http://delasalle.ulsa.mx/pagos/" target="NEW"><img src="images/nuevosArtes/accesoDirecto/Button_Facturacion_Blue.png" onmouseover="this.src='images/nuevosArtes/accesoDirecto/Button_Facturacion_red2.png'" onmouseout="this.src='images/nuevosArtes/accesoDirecto/Button_Facturacion_Blue.png'"></a><br>
            <img src="images/nuevosArtes/accesoDirecto/texto_factura-electronica.png">
          </div>
        </div>

        <div class="two columns">
          <div class="primerCirculo">
            <a href="http://lasalle.mx/profesionalesconvalor/" target="NEW"><img src="images/nuevosArtes/accesoDirecto/Button_profesionales.png" onmouseover="this.src='images/nuevosArtes/accesoDirecto/Button_profesionales_red2.png'" onmouseout="this.src='images/nuevosArtes/accesoDirecto/Button_profesionales.png'"></a><br>
            <img src="images/nuevosArtes/accesoDirecto/texto_profesionales.png">
          </div>

          <div class="segundoCirculo">
            <a href="http://hoy.lasalle.mx/nuevo-portal-de-pagos-en-linea/" target="NEW"><img src="images/nuevosArtes/accesoDirecto/Button_todo-en-1.png" onmouseover="this.src='images/nuevosArtes/accesoDirecto/Button_todo-en-1_OVER2.png'" onmouseout="this.src='images/nuevosArtes/accesoDirecto/Button_todo-en-12.png'"></a><br>
            <img src="images/nuevosArtes/accesoDirecto/texto_todo.png">
          </div>
        </div>     
      </div>
    </section>
  </div>
  
</div>

<!-- FIN SECCION RESPONSIVE ACCESOS DIRECTO 
–––––––––––––––––––––––––––––––––––––––––––––––––– -->









<!-- LINE TITLE SECCION FOOTER
–––––––––––––––––––––––––––––––––––––––––––––––––– -->

  <!--<div class="redLaSalleContenedor">
    <div class="container">
      
      <div class="line">
        <p>Red La Salle</p>
      </div>

    </div>  
  </div>-->

<!-- FIN LINE TITLE SECCION FOOTER
–––––––––––––––––––––––––––––––––––––––––––––––––– -->









<!--  SECCION PREFOOTER
–––––––––––––––––––––––––––––––––––––––––––––––––– -->

  <div class="preFooter">
    <div class="container">
    
      <div class="two columns logoProfesionales">
        <div class="iconoProfesionales">
          <img src="images/nuevosArtes/footer/icon_ProfesionalesValor.png">
        </div>
      </div>

      <div class="ten columns">
        <div class="redLaSalle">
          <ul class="redLaSalleResponsiveUl">
            <li class="mapaSitio"><img src="images/nuevosArtes/footer/icon_redArrow.png"><a href="">Mapa del Sitio</a></li>
            <li><img src="images/nuevosArtes/footer/icon_redArrow.png"><a href="">Directorio</a></li>
            <li><img src="images/nuevosArtes/footer/icon_redArrow.png"><a href="">Trabaja en La Salle</a></li>
            <li><img src="images/nuevosArtes/footer/icon_redArrow.png"><a href="">Universidad Segura</a></li>
            <li><img src="images/nuevosArtes/footer/icon_redArrow.png"><a href="">¿Como llegar?</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>

<!--  FIN SECCION PREFOOTER
–––––––––––––––––––––––––––––––––––––––––––––––––– -->








<!--  SECCION LOGOS FOOTER RESPONSIVE
–––––––––––––––––––––––––––––––––––––––––––––––––– -->

  <div class="footerResponsive logosResponsive" id="logosResponsive">
    <div class="container"> 
      <div class="logosFooterResponsive">
        
        <div class="logoUnoResponsive">
          <img src="images/nuevosArtes/footer/icon_LogoSalleUno.png">
        </div>
              
        <div class="logoDosResponsive">
          <img src="images/nuevosArtes/footer/icon_LogoSalleDos.png">
        </div>

      </div>
    </div>
  </div>

<!-- FIN SECCION LOGOS FOOTER RESPONSIVE
–––––––––––––––––––––––––––––––––––––––––––––––––– -->








  <!--  SECCION ACREDITACION RESPONSIVE
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->

  <div class="acreditacion_responsive">
    <div class="container">
    <button id="btn_proceso_acreditacion_responsive" class="btn_acreditacion_responsive">Proceso de Acreditacion La Salle</button>
    </div>
  </div>


  <div class="postFooter" id="acreditacion_la_salle_responsive">
    <div class="container"> 
      <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>

      <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>    
    </div>
  </div>

  <!--  FIN SECCION ACREDITACION RESPONSIVE
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->


<!-- SECCION LOCALIDADES FOOTER 
–––––––––––––––––––––––––––––––––––––––––––––––––– -->

  <div class="footerLocalidades">
    <div class="container"> 
      <div class="columnasFooter">
        <div class="listaLocalidades">
          
          <div class="listaUnoLocalidades">
            <ul>
              <li><a href="">Bajio</a></li>
              <li><a href="">Cancun</a></li>
              <li><a href="">Chihuahua</a></li>
              <li><a href="">Cuernavaca</a></li>
              <li><a href="">Laguna</a></li>
              <li><a href="">Monterrey</a></li>
              <li><a href="">Morelia</a></li>
            </ul>
          </div>
          
          <div class="listaDosLocalidades">
            <ul>
              <li><a href="">Nezahualcoyotl</a></li>
              <li><a href="">Noroeste</a></li>
              <li><a href="">Oaxaca</a></li>
              <li><a href="">Pachuca</a></li>
              <li><a href="">Puebla</a></li>
              <li><a href="">Saltillo</a></li>
              <li><a href="">Victoria</a></li>
            </ul>
          </div>
        </div>    
      </div>
		
      <div class="columnasFooter">
        <div class="text_responsive_contacto">
          <p class="text_responsive_title">Contáctanos: </p>
          <p class="text_responsive_telephone">+52 1 5278-9500</p>
          <p>Benjamin Franklin 47 Col. Condesa.<br>Del. Cuauhtemoc México DF. CP 06140</p>
        </div>
      </div>
    </div>
  </div>

  <!-- FIN SECCION LOCALIDADES FOOTER 
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->




  
    <!-- SECCION FOOTER FULL RESPONSIVE 
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<?php 
		$sql= "select twitter,facebook from micrositios where id=".$_GET["micrositio"]." limit 1";
		$query=mysql_query($sql) or die(mysql_error());			
		while ($resultados = mysql_fetch_assoc($query)){
			$twitter = $resultados["twitter"];
			$facebook = $resultados["facebook"];
		}mysql_free_result($query);
	?>
  <div class="footer" id="not_responsive">
    <div class="container"> 
      <div class="row footer_new_row">
        <!--<div style="color:transparent;" class="two columns">
         .
        </div>
        <div class="two columns">
         <img src="images/nuevosArtes/footer/icon_ProfesionalesValor.png">
        </div>-->
        <div class="two columns" style="margin-top: 5%;">
           <img src="images/nuevosArtes/footer/icon_LogoSalleUno.png">
        </div>
        <div class="two columns" style="margin-top: 5%;">
          <img src="images/nuevosArtes/footer/icon_LogoSalleDos.png">
        </div>
		
		<div class="one columns" style="margin-top: 5%;margin-left: 0;width: 5%;">
			<a style="background:<?php echo $tono; ?>;float: left;padding: 1px;margin-right: 2px;" href="<?php echo $twitter; ?>"><img src="images/nuevosArtes/footer_icons_red_social/social_1.png"></a>
		    <a style="background:<?php echo $tono; ?>;float: left;padding: 1px;" href="<?php echo $facebook; ?>"><img src="images/nuevosArtes/footer_icons_red_social/social_2.png"></a>
        </div>
		
        <div class="seven columns white menuIconsRedes">
          <nav>
            <ul>
              <li>
                <a href="https://www.facebook.com/LaSalleMX" target="NEW"><img src="images/nuevosArtes/footer_icons_red_social/social_1.png"></a>
              </li>
               <li>
                <a href="https://twitter.com/LaSalle_MX" target="NEW"><img src="images/nuevosArtes/footer_icons_red_social/social_2.png"></a>
              </li>
               <li>
                <a href="https://www.instagram.com/lasalle_mx/" target="NEW"><img src="images/nuevosArtes/footer_icons_red_social/social_3.png"></a>
              </li>
               <li>
                <a href="" target="NEW"><img src="images/nuevosArtes/footer_icons_red_social/social_4.png"></a>
              </li>
               <li>
                <a href="https://www.linkedin.com/edu/universidad-la-salle-a.c.-15234" target="NEW"><img src="images/nuevosArtes/footer_icons_red_social/social_5.png"></a>
              </li>
               <li>
                <a href="" target="NEW"><img src="images/nuevosArtes/footer_icons_red_social/social_6.png"></a>
              </li>
               <li>
                <a href="https://es.foursquare.com/v/universidad-la-salle/4b8ffab0f964a520c16d33e3?#tipsPage=2" target="_blank"><img src="images/nuevosArtes/footer_icons_red_social/social_7.png"></a>
              </li>
               <li>
                <a href="https://www.snapchat.com/add/lasallemx" target="NEW"><img src="images/nuevosArtes/footer_icons_red_social/social_8.png"></a>
              </li>
              <li>
                <a href="https://vine.co/u/1033132484508213248" target="NEW"><img src="images/nuevosArtes/footer_icons_red_social/social_9.png"></a>
              </li>
              <li>
                <a href="https://www.pinterest.com/lasallemx/pins/" target="NEW"><img src="images/nuevosArtes/footer_icons_red_social/social_10.png"></a>
              </li>
              <li>
                <a href="https://www.youtube.com/user/LaSalleMX" target="NEW"><img src="images/nuevosArtes/footer_icons_red_social/social_11.png"></a>
              </li>
            </ul>
          </nav>
          <p>Benjamin Franklin 47 Col. Condesa. Del. Cuauhtemoc</p>
          <p>Ciudad de México, 06140. <span>(+52) 5278-9500</span></p>
        </div>
      </div>
      <div class="row new_list_estados">
        <ul>
          <li><img src="images/nuevosArtes/footer/icon_whiteArrow.png"><a href="http://bajio.delasalle.edu.mx/">Bajio</a></li>
          <li><img src="images/nuevosArtes/footer/icon_whiteArrow.png"><a href="http://lasallecancun.edu.mx/">Cancun</a></li>
          <li><img src="images/nuevosArtes/footer/icon_whiteArrow.png"><a href="http://ulsachihuahua.edu.mx/">Chihuahua</a></li>
          <li><img src="images/nuevosArtes/footer/icon_whiteArrow.png"><a href="http://www.ulsac.edu.mx/">Cuernavaca</a></li>
          <li><img src="images/nuevosArtes/footer/icon_whiteArrow.png"><a href="http://www.ulsalaguna.edu.mx/">Laguna</a></li>
          <li><img src="images/nuevosArtes/footer/icon_whiteArrow.png"><a href="http://www.ceslas.mx/">Monterrey</a></li>
          <li><img src="images/nuevosArtes/footer/icon_whiteArrow.png"><a href="http://www.lasallemorelia.edu.mx/">Morelia</a></li>
          <li><img src="images/nuevosArtes/footer/icon_whiteArrow.png"><a href="http://ulsaneza.edu.mx/">Nezahualcoyotl</a></li>
          <li><img src="images/nuevosArtes/footer/icon_whiteArrow.png"><a href="http://www.ulsa-noroeste.edu.mx/">Noroeste</a></li>
          <li><img src="images/nuevosArtes/footer/icon_whiteArrow.png"><a href="http://www.ulsaoaxaca.edu.mx/">Oaxaca</a></li>
          <li><img src="images/nuevosArtes/footer/icon_whiteArrow.png"><a href="http://www.lasallep.edu.mx/ulsaweb/index.php/">Pachuca</a></li>
          <li><img src="images/nuevosArtes/footer/icon_whiteArrow.png"><a href="http://www.universidadlasallebenavente.edu.mx/">Puebla</a></li>
          <li><img src="images/nuevosArtes/footer/icon_whiteArrow.png"><a href="http://www.ulsasaltillo.edu.mx/">Saltillo</a></li>
          <li><img src="images/nuevosArtes/footer/icon_whiteArrow.png"><a href="http://www.ulsavictoria.edu.mx/">Victoria</a></li>    
        </ul>
      </div>
      <!---<div class="row contacto_new_contact">
        <div class="six columns telefonoContactanos"><p>Contáctanos: <span>+52 1 5278-9500</span></p></div>
        <div class="six columns contactanoFooter"> <p>Benjamin Franklin 47 Col. Condesa. Del. Cuauhtemoc México DF. CP 06140</p></div>
      </div>-->
    </div>
  </div>

  <!-- FIN SECCION FOOTER FULL RESPONSIVE 
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->




 


  <!-- SECCION FOOTER FULL NOT RESPONSIVE 
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->

  <div class="footer" id="footerResponsive">
    <div class="container"> 

      <div class="columnasFooter" id="logosFooterGris">
        <div class="logosFooter">
          <img src="images/nuevosArtes/footer/icon_LogoSalleUno.png"><br>
          <img src="images/nuevosArtes/footer/icon_LogoSalleDos.png"><br>
          <p>Aviso de Privacidad</p>
        </div>
      </div>

      <div class="columnasFooter CambioToSelect">
        <div class="listaLocalidades">
          
          <div class="listaUnoLocalidades">
            <ul>
              <li><a href="http://bajio.delasalle.edu.mx/" target="NEW">Bajio</a></li>
              <li><a href="http://lasallecancun.edu.mx/" target="NEW">Cancun</a></li>
              <li><a href="http://ulsachihuahua.edu.mx/" target="NEW">Chihuahua</a></li>
              <li><a href="http://www.ulsac.edu.mx/" target="NEW">Cuernavaca</a></li>
              <li><a href="http://www.ulsalaguna.edu.mx/" target="NEW">Laguna</a></li>
              <li><a href="http://www.ceslas.mx/" target="NEW">Monterrey</a></li>
              <li><a href="http://www.lasallemorelia.edu.mx/" target="NEW">Morelia</a></li>
            </ul>
          </div>
          
          <div class="listaDosLocalidades">
            <ul>
              <li><a href="http://ulsaneza.edu.mx/" target="NEW">Nezahualcoyotl</a></li>
              <li><a href="http://www.ulsa-noroeste.edu.mx/" target="NEW">Noroeste</a></li>
              <li><a href="http://www.ulsaoaxaca.edu.mx/" target="NEW">Oaxaca</a></li>
              <li><a href="http://www.lasallep.edu.mx/ulsaweb/index.php/" target="NEW">Pachuca</a></li>
              <li><a href="http://www.universidadlasallebenavente.edu.mx/" target="NEW">Puebla</a></li>
              <li><a href="http://www.ulsasaltillo.edu.mx/" target="NEW">Saltillo</a></li>
              <li><a href="http://www.ulsavictoria.edu.mx/" target="NEW">Victoria</a></li>
            </ul>
          </div>
        </div>
      </div>
	  <div class="columnasFooter SelectCampus">
		<select style=" background: #a0a0a0;color: white;">
			<option>Bajio</option>
			<option>Cancun</option>
			<option>Chihuahua</option>
			<option>Cuernavaca</option>
			<option>Laguna</option>
			<option>Monterrey</option>
			<option>Morelia</option>
			<option>Nezahualcoyotl</option>
			<option>Noroeste</option>
			<option>Oaxaca</option>
			<option>Pachuca</option>
			<option>Puebla</option>
			<option>Saltillo</option>
			<option>Victoria</option>
		</select>		
	  </div>
    </div>
  </div>

  <!-- FIN SECCION FOOTER FULL RESPONSIVE 
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->









  <!-- SECCION CONTENEDOR BOTON POSTFOOTER ACREDITACION
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  
  <div class="acreditacion">
    <div style="text-align:center;"class="container">
      <p>Profesionales con <span style="font-size:2em;" >Valor</span>
      </p>
    </div>
  </div>

  <div class="acreditacion_white">
    <div class="container">
      <div class="two columns"><a href="http://delasalle.ulsa.mx/privacidad/" target="NEW">Aviso de Privacidad</a></div>
      <div class="two columns"><a href="http://www.lasalle.mx/terminos-y-condiciones/" target="NEW">Terminos y Condiciones</a></div>
      <div class="seven columns rightTextFooterAcreditacion"><button id="btn_process_success" class="btn_process_success"> <img id="imageUp" class="rotate" src="images/nuevosArtes/footer/icon_redArrow.png">
        <img id="downImageDown" class="rotate upImage" src="images/nuevosArtes/footer/icon_redArrow.png">
        </button>Proceso de Acreditacion La Salle</div>
    </div>
  </div>

  <!-- FIN SECCION CONTENEDOR BOTON POSTFOOTER ACREDITACION
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    







  <!-- SECCION POSTFOOTER ACREDITACION
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->

  <div class="postFooter" id="acreditacion_la_salle">
    <div class="container"> 
      <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>

      <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>    
    </div>
  </div>

  <!-- FIN SECCION POSTFOOTER ACREDITACION
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->


  <!--  SECCION REDES SOCIALES RESPONSIVE
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <div class="back_redes_responsive">
   <div class="container ">
    <div class="redesSocialesResponsive">
      <ul>
        <li>
          <a href="https://www.facebook.com/LaSalleMX" target="NEW"><img src="images/nuevosArtes/footer_icons_red_social/social_1.png"></a>
        </li>
        <li>
          <a href="https://twitter.com/LaSalle_MX" target="NEW"><img src="images/nuevosArtes/footer_icons_red_social/social_2.png"></a>
        </li>
        <li>
          <a href="https://www.instagram.com/lasalle_mx/" target="NEW"><img src="images/nuevosArtes/footer_icons_red_social/social_3.png"></a>
        </li>
        <li>
          <a href="" target="NEW"><img src="images/nuevosArtes/footer_icons_red_social/social_4.png"></a>
        </li>
        <li>
          <a href="https://www.linkedin.com/edu/universidad-la-salle-a.c.-15234" target="NEW"><img src="images/nuevosArtes/footer_icons_red_social/social_5.png"></a>
        </li>
        <li>
          <a href="" target="NEW"><img src="images/nuevosArtes/footer_icons_red_social/social_6.png"></a>
        </li>
        <li>
          <a href="https://es.foursquare.com/v/universidad-la-salle/4b8ffab0f964a520c16d33e3?#tipsPage=2" target="_blank"><img src="images/nuevosArtes/footer_icons_red_social/social_7.png"></a>
        </li>
        <li>
          <a href="https://www.snapchat.com/add/lasallemx" target="NEW"><img src="images/nuevosArtes/footer_icons_red_social/social_8.png"></a>
        </li>
        <li>
          <a href="https://vine.co/u/1033132484508213248" target="NEW"><img src="images/nuevosArtes/footer_icons_red_social/social_9.png"></a>
        </li>
        <li>
          <a href="https://www.pinterest.com/lasallemx/pins/" target="NEW"><img src="images/nuevosArtes/footer_icons_red_social/social_10.png"></a>
        </li>
        <li>
          <a href="https://www.youtube.com/user/LaSalleMX" target="NEW"><img src="images/nuevosArtes/footer_icons_red_social/social_11.png"></a>
        </li>
      </ul>
    </div>
  </div>
  </div>

  <!--  FIN SECCION REDES SOCIALES RESPONSIVE
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <div class="columnasDireccion">
    <div class="container">
      <div class="content_footer_contact_responsive" id="columnasDireccionFooterResponsive" >
        <p class="telefonoContactanos">+52 1 5278-9500</p>
        <p>Benjamin Franklin 47 Col. Condesa.<br>Del. Cuauhtemoc México DF. CP 06140</p>
      </div>
    </div>
  </div>







  <!--  SECCION AVISO DE PRIVACIDAD RESPONSIVE
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->

  <div class="avisoDePrivacidad">
    <div class="container">
      <a href="http://delasalle.ulsa.mx/privacidad/" target="NEW">Aviso de Privacidad</a>
    </div>
  </div>

  <!--  FIN SECCION AVISO DE PRIVACIDAD RESPONSIVE
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->



  <!--  SECCION AVISO DE PRIVACIDAD RESPONSIVE
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->

  <div class="avisoDePrivacidad">
    <div class="container">
      <a href="http://www.lasalle.mx/terminos-y-condiciones/" target="NEW">Terminos y condiciones</a>
    </div>
  </div>


  <!--  FIN SECCION AVISO DE PRIVACIDAD RESPONSIVE
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->






  <!-- MODALES
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->


  <div data-remodal-id="modal2" role="dialog" aria-labelledby="modal2Title" aria-describedby="modal2Desc" class="custom-modal">
      <button data-remodal-action="confirm" class="remodal-confirm">X</button>

      <!--<img style="width:100%;" src="images/nuevosArtes/modales/prueba.png" alt="Eventos">-->

      <table width="100%" border="0">
          <tr>
              <td width="50%" valign="top" class="img-noticia"><img style="width:100%;" src="images/nuevosArtes/modales/prueba.png" alt="Eventos"></td>
              <td width="50%" valign="top">
                  <h5 class="titulo-modal">Titulo</h5>
                  <!-- <div class="text">Texto</div> -->
                  <div id="textogeneral" cols="30" rows="100"></div>
              </td>
          </tr>
      </table>

  </div>
  
  <!-- Modal -->
  <div class="modal fade" id="modal-ver-tabla" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 style="color:black;"> Tabla de acreditaciones</h4>
        </div>
        <div class="modal-body" style="overflow-y:scroll;max-height: 450px;">
			<table class="normal_table ta_center excel" cellspacing="0">
				<thead>
					<tr>
						<th>Programa</th>
						<th>Organismo</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Administración</td>
						<td>CACECA</td>
					</tr>
					<tr>
						<td>Arquitectura</td>
						<td>ANPADEH</td>
					</tr>
					<tr>
						<td>Ciencias de la Comunicación</td>
						<td>CONAC</td>
					</tr>
					<tr>
						<td>Ciencias de la Educación</td>
						<td>CEPPE</td>
					</tr>
					<tr>
						<td>Ciencias Religiosas</td>
						<td>COAPEHUM</td>
					</tr>
					<tr>
						<td>Ciencias Religiosas (No escolarizado)</td>
						<td>COAPEHUM</td>
					</tr>
					<tr>
						<td>Comercio y Negocios Internacionales</td>
						<td>CACECA</td>
					</tr>
					<tr>
						<td>Contaduría y Finanzas</td>
						<td>CACECA</td>
					</tr>
					<tr>
						<td>Derecho</td>
						<td>CONAED</td>
					</tr>
					<tr>
						<td>Diseño Gráfico</td>
						<td>COMAPROD</td>
					</tr>
					<tr>
						<td>Educación Primaria</td>
						<td>CIEES</td>
					</tr>
					<tr>
						<td>Filosofía</td>
						<td>COAPEHUM</td>
					</tr>
					<tr>
						<td>Filosofía (No escolarizado)</td>
						<td>COAPEHUM</td>
					</tr>
					<tr>
						<td>Gestión de Negocios y Tecnologías de Información</td>
						<td>CACECA</td>
					</tr>
					<tr>
						<td>Ingeniería Ambiental</td>
						<td>CACEI</td>
					</tr>
					<tr>
						<td>Ingeniería Biomédica</td>
						<td>CACEI</td>
					</tr>
					<tr>
						<td>Ingeniería Cibernética y Sistemas Computacionales</td>
						<td>CACEI</td>
					</tr>
					<tr>
						<td>Ingeniería Civil</td>
						<td>CACEI</td>
					</tr>
					<tr>
						<td>Ingeniería Electrónica y Comunicaciones</td>
						<td>CACEI</td>
					</tr>
					<tr>
						<td>Ingeniería Industrial</td>
						<td>CACEI</td>
					</tr>
					<tr>
						<td>Ingeniería Mecánica</td>
						<td>CACEI</td>
					</tr>
					<tr>
						<td>Ingeniería Mecatrónica</td>
						<td>CACEI</td>
					</tr>
					<tr>
						<td>Ingeniería Química</td>
						<td>CACEI</td>
					</tr>
					<tr>
						<td>Médico – Cirujano</td>
						<td>COMAEM</td>
					</tr>
					<tr>
						<td>Psicología</td>
						<td>CNEIP</td>
					</tr>
					<tr>
						<td>Químico Farmacéutico Biólogo</td>
						<td>COMAEF</td>
					</tr>
					<tr>
						<td>Relaciones Internacionales</td>
						<td>ACCECISO</td>
					</tr>
					<tr>
						<td>Mercadotecnia </td>
						<td>en proceso </td>
					</tr>
					<tr>
						<td>Actuaría </td>
						<td>en proceso </td>
					</tr>
					<tr>
						<td>Química en Alimentos </td>
						<td>en proceso </td>
					</tr>
					<tr>
						<td>Educación Preescolar</td>
						<td>no aplica</td>
					</tr>
				</tbody>
			</table>
        </div>
      </div>
    </div>
  </div> 


  <!-- FIN MODALES
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->



  <!-- JS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
<div id="scripts"></div>
  
  <script src="js/jquery-1.9.1.min.js"></script>
  <script src="js/jquery.bxslider.js"></script>
  <script src="js/jquery.bxsliderResponsive.js"></script>
  <script src="js/jquery.bxsliderPrincipal.js"></script>
  <script src="js/parallax/script.js" defer></script>
  <script src="js/gral/funciones.js" defer></script>
  <script src="js/gral/funcionesResponsive.js" defer></script>
  <script src="js/mapImage/jquery.rwdImageMaps.js" defer></script>
  <script src="js/mapImage/jquery.rwdImageMaps.min.js" defer></script>
  <script src="js/menuFullScreen/1.js" defer></script>
  <script src="js/menuFullScreen/2.js" defer></script>
  <script src="js/modal/remodal.js" defer></script>
  <script src="js/modal/modales.js" defer></script> 
  <script src="js/calendario/responsive-calendar.js" defer></script>
  <script src="js/slider/slider.min.js" defer></script>
  <script src="js/slider/script.js" defer></script>
  <script src="js/buttons/button.js" defer></script>
  <script src="js/sliderPrincipal/sliderPrincipal.js" defer></script>
  <script src="js/noticias/modernizr.js"></script>
  <script src="js/moment.js"></script>
  <script src="css/bootstrap/js/bootstrap.min.js" ></script>


 
  <script src="js/bootstrap-portfilter.min.js"></script>
  <!-- <script src="js/modernizr.custom.js"></script> -->
  <!-- <script src="js/grid.js"></script> -->

  <script>
    
    $(document).ready(function(){
      index();
    });

$(document).ready(function(){
    $(".nav-tabs a").click(function(){
        $(this).tab('show');
    });
    $('.nav-tabs a').on('shown.bs.tab', function(event){
        var x = $(event.target).text();         // active tab
        var y = $(event.relatedTarget).text();  // previous tab
        $(".act span").text(x);
        $(".prev span").text(y);
    });
});

    
    $(document).ready(function(){
      $('.slider11').bxSlider({
        slideWidth: 199,
        minSlides: 2,
        maxSlides: 5
      });
    });

    $(document).ready(function(){
      $('.slider2').bxSliderResponsiveCalendario({
        slideWidth: 500,
        minSlides: 1,
        maxSlides: 1
      });
    });

    $(document).scroll(function() {
      var y = $(this).scrollTop();
      if (y > 600) {
        $('#nav_main').show(1000);
          
      } else {
        $('#nav_main').hide(1000); 
      }
    });


    $(document).ready(function () {
      $('#nav_main').on('click',function(){
        $('html, body').animate({
          scrollTop: $('#video-container').offset().top
        }, 'slow');
      });
    });

    $(document).ready(function () {
      $("#_btn_select").on('click',function() {  //use a class, since your ID gets mangled
        $(this).addClass("redActive");      //add the class to the clicked element
        alert('me presionaste');
      });
    });

    function index(){
      if (Modernizr.touch) {
              // show the close overlay button
        $(".close-overlay").removeClass("hidden");

              // handle the adding of hover class when clicked
        $(".imgNoticia").click(function(e){
          if (!$(this).hasClass("hoverNoticia")) {
            $(this).addClass("hoverNoticia");
          }
        });

        $(".imgNoticiaCalendario").click(function(e){
          if (!$(this).hasClass("hoverCalendario")) {
            $(this).addClass("hoverCalendario");
          }
        });

        // handle the closing of the overlay

        $(".close-overlay").click(function(e){
          e.preventDefault();
          e.stopPropagation();
          if ($(this).closest(".imgNoticiaCalendario").hasClass("hoverCalendario")) {
            $(this).closest(".imgNoticiaCalendario").removeClass("hoverCalendario");
          }
        });

        $(".close-overlay").click(function(e){
          e.preventDefault();
          e.stopPropagation();
          if ($(this).closest(".imgNoticia").hasClass("hoverNoticia")) {
            $(this).closest(".imgNoticia").removeClass("hoverNoticia");
          }
        });

      } else {

        $(".imgNoticia").mouseenter(function(){
          $(this).addClass("hoverNoticia");
        })

        // handle the mouseleave functionality
        .mouseleave(function(){
          $(this).removeClass("hoverNoticia");
        });


        $(".imgNoticiaCalendario").mouseenter(function(){
          $(this).addClass("hoverCalendario");
          $('#tituloCalendario').hide("slow");
        })

                      // handle the mouseleave functionality
        .mouseleave(function(){
          $('#tituloCalendario').show("slow");
          $(this).removeClass("hoverCalendario");
        });


        $(".imgNoticiaCalendario2").mouseenter(function(){
          $(this).addClass("hoverCalendario");
          $('#tituloCalendario2').hide("slow");
        })


        .mouseleave(function(){
          $('#tituloCalendario2').show("slow");
          $(this).removeClass("hoverCalendario");
        });

        $(".imgNoticiaCalendario3").mouseenter(function(){
          $(this).addClass("hoverCalendario");
          $('#tituloCalendario3').hide("slow");
        })
                      // handle the mouseleave functionality
        .mouseleave(function(){
          $('#tituloCalendario3').show("slow");
          $(this).removeClass("hoverCalendario");
        });

        $(".imgNoticiaCalendario4").mouseenter(function(){
          $(this).addClass("hoverCalendario");
          $('#tituloCalendario4').hide("slow");
        })
                      // handle the mouseleave functionality
        .mouseleave(function(){
          $('#tituloCalendario4').show("slow");
          $(this).removeClass("hoverCalendario");
        });


        $("#estudiante").mouseenter(function(){
          $(this).addClass("cuadroActive");
          $(this).removeClass("azulFuerte");
        })
                      // handle the mouseleave functionality
        .mouseleave(function(){
          $(this).removeClass("cuadroActive");
        });
                      
        $("#estudiante2").mouseenter(function(){
          $(this).addClass("cuadroActive");
          $('#azulFuerte').removeClass("azulFuerte");
        })
                      // handle the mouseleave functionality
        .mouseleave(function(){
          $(this).removeClass("cuadroActive");
        });

        $("#estudiante3").mouseenter(function(){
          $(this).addClass("cuadroActive");
        })
                      // handle the mouseleave functionality
        .mouseleave(function(){
          $(this).removeClass("cuadroActive");
        });
                      
        $("#estudiante4").mouseenter(function(){
          $(this).addClass("cuadroActive");
        })
                      // handle the mouseleave functionality
        .mouseleave(function(){
          $(this).removeClass("cuadroActive");
        });

        $("#perfil").mouseenter(function(){
          $(this).addClass("cuadroActive");
        })
                      // handle the mouseleave functionality
        .mouseleave(function(){
          $(this).removeClass("cuadroActive");
        });

        $("#perfil2").mouseenter(function(){
          $(this).addClass("cuadroActive");
        })
                      // handle the mouseleave functionality
        .mouseleave(function(){
          $(this).removeClass("cuadroActive");
        });

        $("#perfil3").mouseenter(function(){
          $(this).addClass("cuadroActive");
        })
                      // handle the mouseleave functionality
        .mouseleave(function(){
          $(this).removeClass("cuadroActive");
        });

                      /*DOCENTE*/

        $("#docente").mouseenter(function(){
          $(this).addClass("cuadroActive");
        })
                      // handle the mouseleave functionality
        .mouseleave(function(){
          $(this).removeClass("cuadroActive");
        });

        $("#docente2").mouseenter(function(){
          $(this).addClass("cuadroActive");
        })
                      // handle the mouseleave functionality
        .mouseleave(function(){
          $(this).removeClass("cuadroActive");
        });

        $("#docente3").mouseenter(function(){
          $(this).addClass("cuadroActive");
        })
                      // handle the mouseleave functionality
        .mouseleave(function(){
          $(this).removeClass("cuadroActive");
        });

        /*ADMINISTRATIVO*/

        $("#administrativo").mouseenter(function(){
          $(this).addClass("cuadroActive");
        })
                      // handle the mouseleave functionality
        .mouseleave(function(){
          $(this).removeClass("cuadroActive");
        });

        $("#administrativo2").mouseenter(function(){
          $(this).addClass("cuadroActive");
        })
                      // handle the mouseleave functionality
        .mouseleave(function(){
          $(this).removeClass("cuadroActive");
        });

        $("#administrativo3").mouseenter(function(){
          $(this).addClass("cuadroActive");
        })
                      // handle the mouseleave functionality
        .mouseleave(function(){
          $(this).removeClass("cuadroActive");
        });

        /*EGRESADO*/

        $("#egresado").mouseenter(function(){
          $(this).addClass("cuadroActive");
        })
                      // handle the mouseleave functionality
        .mouseleave(function(){
          $(this).removeClass("cuadroActive");
        });

        $("#egregsado2").mouseenter(function(){
          $(this).addClass("cuadroActive");
        })
                      // handle the mouseleave functionality
        .mouseleave(function(){
          $(this).removeClass("cuadroActive");
        });

        /*PADRE DE FAMILIA*/

        $("#padre").mouseenter(function(){
          $(this).addClass("cuadroActive");
        })
                      // handle the mouseleave functionality
        .mouseleave(function(){
          $(this).removeClass("cuadroActive");
        });

        $("#padre2").mouseenter(function(){
          $(this).addClass("cuadroActive");
        })
                      // handle the mouseleave functionality
        .mouseleave(function(){
          $(this).removeClass("cuadroActive");
        });

        $("#padre3").mouseenter(function(){
          $(this).addClass("cuadroActive");
        })
                      // handle the mouseleave functionality
        .mouseleave(function(){
          $(this).removeClass("cuadroActive");
        });

      }
    }
	$(document).ready(function () {
		$('.descripcionTituloContacto').css('display','none');
		$("a#botonOcho").click(function(){
			if($('div#contactoShow').is(":hidden")){
				$('.descripcionTituloContacto').css('display','none');
			}
		});
	});

	$("button#ver-tabla").click(function(){
      $("#modal-ver-tabla").modal("show")
	});
	
	// Cuadros tipo google:
		var $cell = $('.image__cell');

		$cell.find('.image--basic').click(function() {
		  var $thisCell = $(this).closest('.image__cell');
		  
		  if ($thisCell.hasClass('is-collapsed')) {
		  $cell.not($thisCell).removeClass('is-expanded').addClass('is-collapsed');
		  $thisCell.removeClass('is-collapsed').addClass('is-expanded');
		  } else {
		  $thisCell.removeClass('is-expanded').addClass('is-collapsed');
		  }
		});

		$cell.find('.expand__close').click(function(){
		  
		  var $thisCell = $(this).closest('.image__cell');
		  
		  $thisCell.removeClass('is-expanded').addClass('is-collapsed');
		});
	// Cuadros tipo google por funcion
		function ampliar_cuadrito(data){
			var id = data.id;
			var $cell = $('.image__cell');

		  $cell.find('.image--basic').click(function() {
			var $thisCell = $(this).closest('.image__cell');
			
			if ($thisCell.hasClass('is-collapsed')) {
			$cell.not($thisCell).removeClass('is-expanded').addClass('is-collapsed');
			$thisCell.removeClass('is-collapsed').addClass('is-expanded');
			} else {
			$thisCell.removeClass('is-expanded').addClass('is-collapsed');
			}
		  });

		  $cell.find('.expand__close').click(function(){
			
			var $thisCell = $(this).closest('.image__cell');
			
			$thisCell.removeClass('is-expanded').addClass('is-collapsed');
		  });
		}
		// Mostramos 3er nivel de menus
		function subsubmenus(submenu,menu){
			$.ajax({
				url: "funciones-micrositios.php",
				type: "POST",
				data: { submenu:submenu,menu:menu,mostrarsubsubmenus: 1},
				cache: false,
				success: function(html){
					$('#scripts').html(html);
				}
			})
		}
		function componentes(tipo,menu,submenu,subsubmenu){
			// tipo (menu =1000,submenu =2000,subsubmenu =3000)
			$.ajax({
				url: "funciones-micrositios.php",
				type: "POST",
				data: { tipo:tipo,menu:menu,submenu:submenu,subsubmenu:subsubmenu,componentes3: 1},
				cache: false,
				success: function(html){
					if(html == ''){
						$('#componentes-menu'+menu).html('');
					}else{
						$('#componentes-menu'+menu).html(html);
					}
				}
			})
		}
		function primersubmenu(menu){
			$.ajax({
				url: "funciones-micrositios.php",
				type: "POST",
				data: { menu:menu,primersubmenu: 1},
				cache: false,
				success: function(html){
					if(html == ''){
						$('#componentes-menu'+menu).html('');
					}else{
						$('#componentes-menu'+menu).html(html);
					}
				}
			})
		}
		$(document).ready(function () {
			/*
			$(".responsive-calendar").responsiveCalendar({
				time: '2016-02',
				events: {
				//"2016-01-18": {"number": 5, "url": ""},
				"2016-01-18": {"url": "http://kreativeco.com"},
				"2016-02-01": {"url": "http://kreativeco.com"},
				"2016-06-12": {}}
			});
			*/
			function addLeadingZero(num) {
				if (num < 10) {
				  return "0" + num;
				} else {
				  return "" + num;
				}
			}
			$(".responsive-calendar").responsiveCalendar({
				time: initial_date,
				events: {
				<?php
				function MostrarEventosJS(){
					// Checamos las noticias que se muestran en calendario
						$query = mysql_query("select noticias_id from modulos_fijos where id=2");
						$results = mysql_fetch_array($query); mysql_free_result($query);
						$eventos = $results['noticias_id'];
					// Seleccionamos las noticias con la fecha especificada
						$sql= "select fecha_creacion from noticias where id IN(".$eventos.")";
						$query=mysql_query($sql) or die(mysql_error());
						while ($resultados = mysql_fetch_assoc($query)){
							$fecha[] = $resultados["fecha_creacion"];
						}mysql_free_result($query);
						$i=0; while($i < count(@$fecha)){	?>
							"<?php echo substr($fecha[$i], 0, 10); ?>": {}<?php if($i != count($fecha) -1){ echo ','; } ?>
				<?php	$i++; } 
				} MostrarEventosJS();
				?>
				},
				onMonthChange: function(){

					//console.log(this);

					var tmp = {
							month: this.currentMonth,
							year: this.currentYear
						}
						;

					tmp.month = tmp.month+1;
					if(tmp.month==13){
						tmp.month==1;
						tmp.year = tmp.year+1;
					}
				},
				onDayClick: function(events){
					var thisDayEvent, fecha,dia,mes,ano;
					fecha = $(this).data('year')+'-'+addLeadingZero( $(this).data('month') )+'-'+addLeadingZero( $(this).data('day') );
					dia = addLeadingZero( $(this).data('day') );
					mes = $(this).data('month');
					ano = $(this).data('year');
					//thisDayEvent = events[fecha];
					//console.log(fecha);
					$.ajax({
						url: "funciones-micrositios.php",
						type: "POST",
						data: { color:<?php echo $color; ?>,fecha:fecha,dia:dia,mes:mes,ano:ano,mostrarcalendarioeventos: 1},
						cache: false,
						success: function(html){
							$('.eventos-dinamicos').html(html);
						}
					})
				}
			});
			
		});
		function MostrarEventosCategoria(categoria){
			$(".responsive-calendar").responsiveCalendar('clearAll');
			$.ajax({
				url: "funciones-micrositios.php",
				type: "POST",
				data: { categoria:categoria,mostrarcalendarioeventoscategoria: 1},
				cache: false,
				success: function(html){
					$('#scripts').html(html);
					//$('.responsive-calendar').responsiveCalendar('edit',{html});
				}
			})
		}
		// Vinculacion empresarial
		$('#vinculacionEmpresarialShow .row .segundaFilaVinculacion').mouseenter(function(){
			$(this).addClass('activeVinculacion');
		}).mouseleave(function(){
			$(this).removeClass('activeVinculacion');
		});
		// Contacto
		function contacto(index){
			$('.formContacto').hide();
			if(index == 0){ // Direccion de atención al público
				$('#overs-atencion').show();
			}else{
				$('#overs-atencion').hide();
			}
			if(index == 1){ // Mapas slider
				$('#slide-mapas').show();
			}else{
				$('#slide-mapas').hide();
			}
			if(index == 2){	// Como llegar
				$('#mosaico-contacto,.containerMapa').show();
			}else{
				$('#mosaico-contacto,.containerMapa').hide();
			}
			if(index == 3){	 // Contacto
				$('.formContacto').show();
			}else{
				$('.formContacto').hide();
			}
		}
		// Mostramos 3er nivel de subsubmenus - Oferta educativa
		function subsubmenusOferta(submenu,menu){
			$.ajax({
				url: "funciones-micrositios.php",
				type: "POST",
				data: { submenu:submenu,menu:menu,mostrarsubsubmenusoferta: 1},
				cache: false,
				success: function(html){
					$('#subsubmenus-menu'+menu).html('');
					$('#mosaico-dinamico').html('');
					$('.item-selecciono').hide('slow');
					$('#scripts').html(html);
				}
			})
		}
		// Mostramos tabs de cuotas en oferta educativa
		function OfertaCuotas(id){
			$.ajax({
				url: "funciones-micrositios.php",
				type: "POST",
				data: { cuota:id,mostrarsemestre: 1},
				cache: false,
				success: function(html){
					$('#texto-cuota').html(html);
				}
			})
		}
		// Muestra proyecto
		function muestraproyecto(iditem){
			$.ajax({
				url: "funciones-micrositios.php",
				type: "POST",
				data: { id:iditem,mostrarproyecto: 1},
				cache: false,
				success: function(html){
					$('#aqui-proyecto').html(html);
				}
			})
		}
		// efecto para menus de aspirante
		function HacerEfecto(dato){
			var id = dato.id;
			$('button#'+id).hover(
				function(){ 
					$('button#'+id).find( "span" ).addClass('enter');
					$('button#'+id).find( "span" ).removeClass('reset');
				},
				function(){
					$('button#'+id).find( "span" ).removeClass('enter');
				$('button#'+id).find( "span" ).addClass('reset');
				}
			);
		}
		// Mostramos items de aspirante
		function ItemsAspirante(menu){
			/*
				$.ajax({
					url: "funciones.php",
					type: "POST",
					data: { menu:menu,mostraritemsaspirante: 1},
					cache: false,
					success: function(html){
						$('#aspirantes').fadeIn('slow');
						$('.contenedor-aspirante').html(html);
					}
				})
			*/
			window.location = 'microsites/perfiles.php?item='+menu;
		}
  </script>
  <script>
	$('.li-cat').children().click(function (event) {
		var span = $(event.target).is('h5,span');
		if(span){
			$(this).siblings('.ul-subs').slideToggle('slow');
		}
	});
	// slide portafolio
		var slideIndex = 1;
		showDivs(slideIndex);

		function plusDivs(n) {
		  showDivs(slideIndex += n);
		}
		function plusDivs2(n) {
		  showDivs2(slideIndex += n);
		}

		function currentDiv(n) {
		  showDivs(slideIndex = n);
		}

		function showDivs(n) {
		  var i;
		  var x = document.getElementsByClassName("mySlides");
		  var dots = document.getElementsByClassName("demo");
		  if (n > x.length) {slideIndex = 1}
		  if (n < 1) {slideIndex = x.length}
		  for (i = 0; i < x.length; i++) {
			 x[i].style.display = "none";
		  }
		  for (i = 0; i < dots.length; i++) {
			 dots[i].className = dots[i].className.replace(" w3-opacity-off", "");
		  }
		  x[slideIndex-1].style.display = "block";
		  dots[slideIndex-1].className += " w3-opacity-off";
		}
		function showDivs2(n) {
		  var i;
		  var x = document.getElementsByClassName("mySlides2");
		  var dots = document.getElementsByClassName("demo");
		  if (n > x.length) {slideIndex = 1}
		  if (n < 1) {slideIndex = x.length}
		  for (i = 0; i < x.length; i++) {
			 x[i].style.display = "none";
		  }
		  for (i = 0; i < dots.length; i++) {
			 dots[i].className = dots[i].className.replace(" w3-opacity-off", "");
		  }
		  x[slideIndex-1].style.display = "block";
		  dots[slideIndex-1].className += " w3-opacity-off";
		}


		var slideIndex = 0;
		carousel();
		carousel2();

		function carousel() {
			var i;
			var x = document.getElementsByClassName("mySlides");
			for (i = 0; i < x.length; i++) {
			  x[i].style.display = "none"; 
			}
			slideIndex++;
			if (slideIndex > x.length) {slideIndex = 1} 
			x[slideIndex-1].style.display = "block"; 
			setTimeout(carousel, 5000); // Change image every 2 seconds
		}
		function carousel2() {
			var i;
			var x = document.getElementsByClassName("mySlides2");
			for (i = 0; i < x.length; i++) {
			  x[i].style.display = "none"; 
			}
			slideIndex++;
			if (slideIndex > x.length) {slideIndex = 1} 
			x[slideIndex-1].style.display = "block"; 
			setTimeout(carousel, 5000); // Change image every 2 seconds
		}
</script>
  <!-- Create:hdzlruben.
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
</body>
</html>
<?php
// Unset para todas las variables declaradas
$variables = array_keys(get_defined_vars());
for ($i = 0; $i < count($variables); $i++) {
    unset($variables[$i]);
}
unset($variables,$i);
//echo memory_get_usage();
		mysql_close($conx);
?>