-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-09-2017 a las 18:33:09
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `web_lasalle`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admitidos`
--

CREATE TABLE `admitidos` (
  `id` int(11) NOT NULL,
  `folio` varchar(255) DEFAULT NULL,
  `clave` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aspirante_items_tabs`
--

CREATE TABLE `aspirante_items_tabs` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `contenido` text,
  `item_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `aspirante_items_tabs`
--

INSERT INTO `aspirante_items_tabs` (`id`, `nombre`, `contenido`, `item_id`) VALUES
(1, 'ejemplo 1', 'pp\n', 1),
(2, 'ejemplo 2', 'mas\n', 1),
(3, 'otro mas', '									<div class="col-sm-6">\r\n										<h3>Exámenes</h3>\r\n										<ul class="lista-perfiles">\r\n											<li>Grupos Estudiantiles <a href="#">Ver...</a></li>\r\n											<li>Grupos Estudiantiles <a href="#">Ver...</a></li>\r\n											<li>Grupos Estudiantiles <a href="#">Ver...</a></li>\r\n											<li>Grupos Estudiantiles <a href="#">Ver...</a></li>\r\n											<li>Grupos Estudiantiles <a href="#">Ver...</a></li>\r\n										</ul>\r\n									</div>\r\n									<div class="col-sm-6">\r\n										<h3>Exámenes</h3>\r\n										<ul class="lista-perfiles">\r\n											<li>Grupos Estudiantiles <a href="#">Ver...</a></li>\r\n											<li>Grupos Estudiantiles <a href="#">Ver...</a></li>\r\n											<li>Grupos Estudiantiles <a href="#">Ver...</a></li>\r\n											<li>Grupos Estudiantiles <a href="#">Ver...</a></li>\r\n											<li>Grupos Estudiantiles <a href="#">Ver...</a></li>\r\n										</ul>\r\n									</div>\r\n									<div class="col-sm-6">\r\n										<h3>Exámenes</h3>\r\n										<ul class="lista-perfiles">\r\n											<li>Grupos Estudiantiles <a href="#">Ver...</a></li>\r\n											<li>Grupos Estudiantiles <a href="#">Ver...</a></li>\r\n											<li>Grupos Estudiantiles <a href="#">Ver...</a></li>\r\n											<li>Grupos Estudiantiles <a href="#">Ver...</a></li>\r\n											<li>Grupos Estudiantiles <a href="#">Ver...</a></li>\r\n										</ul>\r\n									</div>\r\n									<div class="col-sm-6">\r\n										<h3>Exámenes</h3>\r\n										<ul class="lista-perfiles">\r\n											<li>Grupos Estudiantiles <a href="#">Ver...</a></li>\r\n											<li>Grupos Estudiantiles <a href="#">Ver...</a></li>\r\n											<li>Grupos Estudiantiles <a href="#">Ver...</a></li>\r\n											<li>Grupos Estudiantiles <a href="#">Ver...</a></li>\r\n											<li>Grupos Estudiantiles <a href="#">Ver...</a></li>\r\n										</ul>\r\n									</div>\r\n								', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aspirante_menus`
--

CREATE TABLE `aspirante_menus` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `orden` int(11) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `ultimo_cambio` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `aspirante_menus`
--

INSERT INTO `aspirante_menus` (`id`, `nombre`, `orden`, `fecha_creacion`, `ultimo_cambio`) VALUES
(1, 'ASPIRANTE', 1, NULL, NULL),
(2, 'ESTUDIANTE', 2, NULL, NULL),
(3, 'DOCENTE', 3, NULL, NULL),
(4, 'ADMINISTRATIVO', 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aspirante_menus_items`
--

CREATE TABLE `aspirante_menus_items` (
  `id` int(11) NOT NULL,
  `imagen` varchar(255) DEFAULT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `parrafo` text,
  `menu_id` int(11) DEFAULT NULL COMMENT 'id del menu',
  `estado` int(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `aspirante_menus_items`
--

INSERT INTO `aspirante_menus_items` (`id`, `imagen`, `titulo`, `parrafo`, `menu_id`, `estado`) VALUES
(1, 'ejemplo.png', 'ADMISIONES', '<p>Texto de ejemplo</p>', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `catalogo_docente`
--

CREATE TABLE `catalogo_docente` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `facultad` varchar(255) DEFAULT NULL,
  `especialidad` varchar(255) DEFAULT NULL,
  `telefono` varchar(255) DEFAULT NULL,
  `extension` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `formacion_academica` text,
  `experiencia_profesional` text,
  `info_adicional` text,
  `cv` varchar(255) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `catalogo_docente`
--

INSERT INTO `catalogo_docente` (`id`, `nombre`, `facultad`, `especialidad`, `telefono`, `extension`, `email`, `formacion_academica`, `experiencia_profesional`, `info_adicional`, `cv`, `foto`) VALUES
(3, 'Jose ', '1', 'REDES', '23456789', '898', 'jose@gmail.com', 'UCC', 'KREATIVECO', 'NINGUNA', '01-CUENCA.pdf', 'articulo-02-10-cosas-clientes.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `catalogo_docente_tags`
--

CREATE TABLE `catalogo_docente_tags` (
  `id` int(11) NOT NULL,
  `tag` varchar(255) DEFAULT NULL,
  `id_docente` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `catalogo_facultades`
--

CREATE TABLE `catalogo_facultades` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `catalogo_facultades`
--

INSERT INTO `catalogo_facultades` (`id`, `nombre`) VALUES
(1, 'Ciencias');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `centro_componentes`
--

CREATE TABLE `centro_componentes` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `componente_tipo` int(11) DEFAULT NULL COMMENT 'ID del tipo de componente segun la tabla',
  `menu_id` int(11) DEFAULT NULL,
  `orden` int(11) DEFAULT NULL,
  `estado` int(1) DEFAULT '1',
  `fecha_creacion` datetime DEFAULT NULL,
  `ultimo_cambio` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `centro_componentes`
--

INSERT INTO `centro_componentes` (`id`, `nombre`, `componente_tipo`, `menu_id`, `orden`, `estado`, `fecha_creacion`, `ultimo_cambio`) VALUES
(1, 'pepe', 7, 1, 1, 1, '2016-10-31 16:30:09', '2016-10-31 16:30:09'),
(2, 'Descargas', 9, 1, 2, 1, '2016-11-01 12:15:50', '2016-11-01 12:15:50'),
(5, 'Slider de descargas multimedia', 10, 1, 5, 1, '2016-11-01 16:22:41', '2016-11-01 16:22:41'),
(6, 'Video de ejemplo', 11, 1, 7, 1, '2016-11-01 17:06:55', '2016-11-01 17:06:55'),
(7, 'Filtro de ejemplo', 8, 3, 1, 1, '2016-11-07 12:34:29', '2016-11-07 12:34:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `centro_componentes_items`
--

CREATE TABLE `centro_componentes_items` (
  `id` int(11) NOT NULL,
  `componente_id` int(11) DEFAULT NULL,
  `imagen` varchar(255) DEFAULT NULL COMMENT 'Imagen para cuadros tipo google, carrusel, etc',
  `parrafo1` text,
  `parrafo2` text,
  `archivo` varchar(255) DEFAULT NULL COMMENT 'Archivo de descarga',
  `titulo` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `noticias_id` varchar(255) DEFAULT NULL COMMENT 'ids de noticias',
  `subtitulo` varchar(255) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `centro_componentes_items`
--

INSERT INTO `centro_componentes_items` (`id`, `componente_id`, `imagen`, `parrafo1`, `parrafo2`, `archivo`, `titulo`, `link`, `noticias_id`, `subtitulo`) VALUES
(1, 1, NULL, NULL, NULL, NULL, NULL, NULL, '3,2,1', NULL),
(2, 2, NULL, NULL, NULL, 'correo.png', 'pppp33', NULL, NULL, 'oooo22'),
(4, 2, NULL, NULL, NULL, 'articulo-02-10-cosas-clientes.jpg', 'otro', NULL, NULL, 'mas'),
(5, 3, NULL, NULL, NULL, NULL, NULL, NULL, '2', NULL),
(6, 4, NULL, NULL, NULL, NULL, NULL, NULL, '2,1', NULL),
(7, 5, NULL, NULL, NULL, 'foto.jpg', 'Ejemplo', NULL, NULL, 'Nota'),
(8, 6, NULL, NULL, NULL, NULL, NULL, 'iOe6dI2JhgU', NULL, NULL),
(9, 7, NULL, NULL, NULL, NULL, NULL, NULL, '3,2,1', NULL),
(10, 5, NULL, NULL, NULL, 'image_news7.png', 'Primero', NULL, NULL, 'Primero sub'),
(11, 5, NULL, NULL, NULL, 'image_news5.png', 'Segundo', NULL, NULL, 'segundo sub'),
(12, 5, NULL, NULL, NULL, 'back_noticias.png', 'Tercero', NULL, NULL, 'tercero sub'),
(13, 5, NULL, NULL, NULL, 'image_news3.png', 'Cuarto', NULL, NULL, 'cuarto sub');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `centro_info_menus`
--

CREATE TABLE `centro_info_menus` (
  `id` smallint(6) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `orden` int(11) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `ultimo_cambio` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `centro_info_menus`
--

INSERT INTO `centro_info_menus` (`id`, `nombre`, `orden`, `fecha_creacion`, `ultimo_cambio`) VALUES
(1, 'Home', 1, NULL, NULL),
(3, 'Noticias', 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudad`
--

CREATE TABLE `ciudad` (
  `id` int(11) NOT NULL,
  `ciudad` varchar(255) DEFAULT NULL,
  `codigo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ciudad`
--

INSERT INTO `ciudad` (`id`, `ciudad`, `codigo`) VALUES
(61, 'veracruz', 'tag1,tag2,tag3'),
(62, 'mexico', 'tag4,tag5,tag6');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudad_tags`
--

CREATE TABLE `ciudad_tags` (
  `id` int(11) NOT NULL,
  `tag` varchar(255) DEFAULT NULL,
  `ciudad_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ciudad_tags`
--

INSERT INTO `ciudad_tags` (`id`, `tag`, `ciudad_id`) VALUES
(7, 'tag1', 61),
(8, 'tag2', 61),
(9, 'tag3', 61),
(10, 'tag4', 62),
(11, 'tag5', 62),
(12, 'tag6', 62);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentarios`
--

CREATE TABLE `comentarios` (
  `id` int(11) NOT NULL,
  `id_noticia` int(11) DEFAULT NULL,
  `comentario` text,
  `autor` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `comentarios`
--

INSERT INTO `comentarios` (`id`, `id_noticia`, `comentario`, `autor`, `email`, `fecha`) VALUES
(2, 1, 'Otro comentario mas Otro comentario mas Otro comentario mas Otro comentario mas Otro comentario mas Otro comentario mas Otro comentario mas Otro comentario mas ', 'Pepe', 'jose@kreativeco.com', '2016-11-15 08:56:32'),
(3, 1, 'Otro comentario mas Otro comentario mas Otro comentario mas Otro comentario mas Otro comentario mas Otro comentario mas Otro comentario mas Otro comentario mas ', 'Jose', 'jose@kreativeco.com', '2016-11-15 08:56:32');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `componentes`
--

CREATE TABLE `componentes` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `submenu_id` int(11) DEFAULT NULL,
  `componente_tipo` int(11) DEFAULT NULL COMMENT 'ID del tipo de componente segun la tabla',
  `menu_id` int(11) DEFAULT NULL,
  `subsubmenu_id` int(11) DEFAULT NULL,
  `orden` int(11) DEFAULT NULL,
  `estado` int(1) DEFAULT '1',
  `fecha_creacion` datetime DEFAULT NULL,
  `ultimo_cambio` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `componentes`
--

INSERT INTO `componentes` (`id`, `nombre`, `submenu_id`, `componente_tipo`, `menu_id`, `subsubmenu_id`, `orden`, `estado`, `fecha_creacion`, `ultimo_cambio`) VALUES
(1, 'carousel', NULL, 5, NULL, 9, 2, 1, '2016-10-28 17:05:06', '2016-10-28 17:05:06');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `componentes_items`
--

CREATE TABLE `componentes_items` (
  `id` int(11) NOT NULL,
  `componente_id` int(11) DEFAULT NULL,
  `imagen` varchar(255) DEFAULT NULL COMMENT 'Imagen para cuadros tipo google, carrusel, etc',
  `parrafo1` text,
  `parrafo2` text,
  `archivo` varchar(255) DEFAULT NULL COMMENT 'Archivo de descarga',
  `titulo` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `componentes_items`
--

INSERT INTO `componentes_items` (`id`, `componente_id`, `imagen`, `parrafo1`, `parrafo2`, `archivo`, `titulo`, `link`) VALUES
(2, 1, 'articulo-02-10-cosas-clientes.jpg', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `componentes_tipos`
--

CREATE TABLE `componentes_tipos` (
  `id` int(11) NOT NULL,
  `tipo` varchar(255) DEFAULT NULL,
  `estado` int(1) DEFAULT '1' COMMENT '1= activo, 0= inactivo',
  `area` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `componentes_tipos`
--

INSERT INTO `componentes_tipos` (`id`, `tipo`, `estado`, `area`) VALUES
(1, 'Goosalle', 1, 'home'),
(2, 'Cuadros grises', 1, 'home'),
(3, 'Cajas de texto', 1, 'home'),
(4, 'Info-bottom', 0, 'home'),
(5, 'Carrusel imagen', 1, 'home'),
(6, 'Carrusel video', 0, 'home'),
(7, 'News', 1, 'centro'),
(8, 'Filtro de noticias', 1, 'centro'),
(9, 'Descargas', 1, 'centro'),
(10, 'Slider de descargas', 1, 'centro'),
(11, 'Video', 1, 'centro');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `iconos_oferta`
--

CREATE TABLE `iconos_oferta` (
  `id` int(11) NOT NULL,
  `objetivo` int(1) DEFAULT '1',
  `perfil` int(1) DEFAULT '1',
  `cuotas` int(1) DEFAULT '1',
  `plan` int(1) DEFAULT '1',
  `becas` int(1) DEFAULT '1',
  `contacto` int(1) DEFAULT '1',
  `id_item` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menus`
--

CREATE TABLE `menus` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `orden` int(11) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `ultimo_cambio` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `menus`
--

INSERT INTO `menus` (`id`, `nombre`, `orden`, `fecha_creacion`, `ultimo_cambio`) VALUES
(1, 'Somos La Salle', 1, NULL, NULL),
(2, 'Oferta Educativa', 2, NULL, NULL),
(3, 'Vida Estudiantil', 3, NULL, NULL),
(4, 'Servicios', 4, NULL, NULL),
(5, 'Vinculación Empresarial', 5, NULL, NULL),
(6, 'Acción Social', 6, NULL, NULL),
(7, 'Investigación', 7, NULL, NULL),
(8, 'Contacto', 8, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `micrositios_facultades`
--

CREATE TABLE `micrositios_facultades` (
  `id` int(11) NOT NULL,
  `id_facultad` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modulos_fijos`
--

CREATE TABLE `modulos_fijos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `seccion` varchar(255) DEFAULT NULL,
  `estado` int(1) DEFAULT '1',
  `noticias_id` varchar(255) DEFAULT NULL COMMENT 'id de las noticias que contiene separadas por comas'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `modulos_fijos`
--

INSERT INTO `modulos_fijos` (`id`, `nombre`, `seccion`, `estado`, `noticias_id`) VALUES
(1, 'Slider de noticias', 'home', 1, '1'),
(2, 'Calendario', 'home', 1, '2,1'),
(3, 'Noticias principales', 'home', 1, '2,1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

CREATE TABLE `noticias` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `categoria_id` int(11) DEFAULT NULL,
  `subtitulo` text,
  `parrafo_1` text,
  `imagen` varchar(255) DEFAULT NULL,
  `estado` int(1) DEFAULT '1',
  `fecha_creacion` datetime DEFAULT NULL,
  `tags` varchar(255) DEFAULT NULL COMMENT 'ids de tags'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`id`, `titulo`, `categoria_id`, `subtitulo`, `parrafo_1`, `imagen`, `estado`, `fecha_creacion`, `tags`) VALUES
(1, 'Ejemplo', 1, 'Subtitulo', 'Parrafo\r\nParrafo 2', 'image_news3.png', 1, '2016-11-16 11:36:31', '1,2'),
(2, 'Noticia 2', 1, 'Subtitulo 2', 'Parrafo 2', 'image_news8.png', 1, '2016-11-16 11:36:31', '2'),
(3, 'Jornada academica', 5, 'Subtitulo de jornada', 'Parrafo de ejemplo para la noticia Parrafo de ejemplo para la noticia Parrafo de ejemplo para la noticia Parrafo de ejemplo para la noticia Parrafo de ejemplo para la noticia Parrafo de ejemplo para la noticia ', 'image_news2.png', 1, '2016-11-16 11:36:31', '1'),
(4, 'Titulo de tags', 1, 'sub de tags', '<p>dfdfdf</p>', 'articulo-02-10-cosas-clientes.jpg', 1, '2016-11-16 15:33:00', '1,12,3');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias_categorias`
--

CREATE TABLE `noticias_categorias` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `estado` int(1) DEFAULT '1',
  `es_calendario` int(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `noticias_categorias`
--

INSERT INTO `noticias_categorias` (`id`, `nombre`, `estado`, `es_calendario`) VALUES
(1, 'Institucionales', 1, 1),
(2, 'Académicas', 1, 1),
(3, 'Política', 1, 1),
(4, 'Arte', 1, 1),
(5, 'Vida estudiantil', 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oferta_educativa_items`
--

CREATE TABLE `oferta_educativa_items` (
  `id` int(11) NOT NULL,
  `submenu_id` int(11) DEFAULT NULL COMMENT 'id del submenu de oferta educativa',
  `imagen` varchar(255) DEFAULT NULL,
  `parrafo_1` text,
  `duracion` varchar(255) DEFAULT NULL,
  `creditos` varchar(255) DEFAULT NULL,
  `semestres` varchar(255) DEFAULT NULL,
  `plan` varchar(255) DEFAULT NULL,
  `objetivo_parrafo` text,
  `perfil_parrafo_1` text,
  `perfil_parrafo_2` text,
  `titulo` varchar(255) DEFAULT NULL,
  `subtitulo` varchar(255) DEFAULT NULL,
  `contacto_parrafo` text,
  `subsubmenu_id` int(11) DEFAULT NULL,
  `contacto_imagen` varchar(255) DEFAULT NULL,
  `modalidad` varchar(255) DEFAULT NULL,
  `tipo` varchar(255) DEFAULT NULL,
  `antecedentes` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `oferta_educativa_items`
--

INSERT INTO `oferta_educativa_items` (`id`, `submenu_id`, `imagen`, `parrafo_1`, `duracion`, `creditos`, `semestres`, `plan`, `objetivo_parrafo`, `perfil_parrafo_1`, `perfil_parrafo_2`, `titulo`, `subtitulo`, `contacto_parrafo`, `subsubmenu_id`, `contacto_imagen`, `modalidad`, `tipo`, `antecedentes`) VALUES
(4, 8, 'ejemplo.png', 'parrafooo', '9', '9', '3', '43', NULL, NULL, NULL, 'Benjamin', 'Unidad Benjamín', NULL, NULL, NULL, NULL, NULL, NULL),
(5, 10, 'ejemplo.png', 'parrafo lic', '2', '3', '4', '5', '111', '22', '22222', 'Nuevo lic', 'sub lic', '666', 2, 'correo.png', NULL, NULL, NULL),
(6, 8, 'ejemplo.png', 'p2', '62', '92', '6542', '332', NULL, NULL, NULL, 'pepe2', 'p2', NULL, 8, NULL, '15', '16', '17'),
(7, 8, 'facultad_derecho.png', 'parraof derecho', '1', '2', '3', '4', NULL, NULL, NULL, 'derecho', 'subtitulo derecho', NULL, 8, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oferta_educativa_items_cuotas`
--

CREATE TABLE `oferta_educativa_items_cuotas` (
  `id` int(11) NOT NULL,
  `semestre` varchar(255) DEFAULT NULL,
  `parrafo` text,
  `oferta_educativa_item_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oferta_educativa_items_plan_becas`
--

CREATE TABLE `oferta_educativa_items_plan_becas` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `texto` text,
  `oferta_educativa_item_id` int(11) DEFAULT NULL,
  `tipo` varchar(255) DEFAULT NULL COMMENT 'plan o becas'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='plan de estudios y becas de cada item de oferta educativa';

--
-- Volcado de datos para la tabla `oferta_educativa_items_plan_becas`
--

INSERT INTO `oferta_educativa_items_plan_becas` (`id`, `titulo`, `texto`, `oferta_educativa_item_id`, `tipo`) VALUES
(1, 'ppp', 'pppp', 1, 'plan'),
(2, 'ss', 'sss', 1, 'becas'),
(3, '44456', '44456g', 5, 'becas'),
(4, '5556', '555677', 5, 'plan');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `submenus`
--

CREATE TABLE `submenus` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `orden` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `submenus`
--

INSERT INTO `submenus` (`id`, `titulo`, `menu_id`, `orden`) VALUES
(1, 'La Salle Hoy', 1, 1),
(2, 'Modelo Educativo', 1, 2),
(3, 'Campus', 3, NULL),
(7, 'Arte y cultura', 3, NULL),
(8, 'Bachillerato', 2, NULL),
(9, 'Biblioteca', 4, NULL),
(10, 'Licenciatura', 2, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subsubmenus`
--

CREATE TABLE `subsubmenus` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) DEFAULT NULL COMMENT 'id del menu al que pertenecen',
  `titulo` varchar(255) DEFAULT NULL,
  `subtitulo` varchar(255) DEFAULT NULL,
  `parrafo` text,
  `fecha_creacion` datetime DEFAULT NULL,
  `ultimo_cambio` datetime DEFAULT NULL,
  `submenu_id` int(11) DEFAULT NULL,
  `orden` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='3er- nivel';

--
-- Volcado de datos para la tabla `subsubmenus`
--

INSERT INTO `subsubmenus` (`id`, `menu_id`, `titulo`, `subtitulo`, `parrafo`, `fecha_creacion`, `ultimo_cambio`, `submenu_id`, `orden`) VALUES
(2, NULL, 'Facultad de derecho', 'Facultad de derecho', '', NULL, NULL, 10, 1),
(6, NULL, 'qqq', '', '', NULL, NULL, 2, 0),
(8, NULL, 'kk', '', '', NULL, NULL, 8, 0),
(9, NULL, 'mision', 'ooo', '', NULL, NULL, 1, 0),
(11, NULL, 'vision2', 'subtitulo de vision 2 ', '', NULL, NULL, 1, 23),
(12, NULL, 'nuevo', 'nuevo', '', NULL, NULL, 1, 233),
(13, NULL, 'ejemplo', 'otro', '', NULL, NULL, 8, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tags`
--

CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `tag` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tags`
--

INSERT INTO `tags` (`id`, `tag`) VALUES
(1, 'Periodico'),
(2, 'Revsita');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `test`
--

CREATE TABLE `test` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `test`
--

INSERT INTO `test` (`id`, `name`, `email`) VALUES
(1, 'name', 'email'),
(2, 'pepe', 'pepe@correo.com'),
(3, 'jose', 'jose@jose.com'),
(4, 'name2', 'email2'),
(5, 'pepe', 'pepe@correo.com'),
(6, 'jose', 'jose@jose.com'),
(7, 'name2', 'email2'),
(8, 'pepe', 'pepe@correo.com'),
(9, 'jose', 'jose@jose.com'),
(10, 'primero', 'tiene un costo de 100'),
(11, 'segundo', 'costo de 200'),
(12, 'primero', 'tiene un costo de 100'),
(13, 'segundo', 'costo de 200');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `usuario` varchar(255) DEFAULT NULL,
  `contrasena` varchar(255) DEFAULT NULL,
  `estado` int(1) DEFAULT NULL,
  `ultimo_acceso` datetime DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `ultimo_cambio` datetime DEFAULT NULL,
  `foto` varchar(255) DEFAULT 'user.png',
  `tipo` int(11) DEFAULT NULL COMMENT '1= EDITOR, 2= Admin'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `usuario`, `contrasena`, `estado`, `ultimo_acceso`, `fecha_creacion`, `ultimo_cambio`, `foto`, `tipo`) VALUES
(1, 'Luis Flores', 'luis@kreativeco.com', '123456', 1, NULL, NULL, NULL, 'user.png', 2),
(3, 'nuevoi', 'nuevo@kreativeco.com', 'nuevo', 1, NULL, NULL, NULL, 'user.png', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `video`
--

CREATE TABLE `video` (
  `id` int(11) NOT NULL,
  `video` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `video`
--

INSERT INTO `video` (`id`, `video`) VALUES
(1, 'home.mp4');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vinculacion_empresarial`
--

CREATE TABLE `vinculacion_empresarial` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `parrafo` text,
  `estado` int(1) DEFAULT '1',
  `orden` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admitidos`
--
ALTER TABLE `admitidos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `aspirante_items_tabs`
--
ALTER TABLE `aspirante_items_tabs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `aspirante_menus`
--
ALTER TABLE `aspirante_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `aspirante_menus_items`
--
ALTER TABLE `aspirante_menus_items`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `catalogo_docente`
--
ALTER TABLE `catalogo_docente`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `catalogo_docente_tags`
--
ALTER TABLE `catalogo_docente_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `catalogo_facultades`
--
ALTER TABLE `catalogo_facultades`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `centro_componentes`
--
ALTER TABLE `centro_componentes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `centro_componentes_items`
--
ALTER TABLE `centro_componentes_items`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `centro_info_menus`
--
ALTER TABLE `centro_info_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ciudad_tags`
--
ALTER TABLE `ciudad_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `componentes`
--
ALTER TABLE `componentes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `componentes_items`
--
ALTER TABLE `componentes_items`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `componentes_tipos`
--
ALTER TABLE `componentes_tipos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `iconos_oferta`
--
ALTER TABLE `iconos_oferta`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `micrositios_facultades`
--
ALTER TABLE `micrositios_facultades`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `modulos_fijos`
--
ALTER TABLE `modulos_fijos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `noticias_categorias`
--
ALTER TABLE `noticias_categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `oferta_educativa_items`
--
ALTER TABLE `oferta_educativa_items`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `oferta_educativa_items_cuotas`
--
ALTER TABLE `oferta_educativa_items_cuotas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `oferta_educativa_items_plan_becas`
--
ALTER TABLE `oferta_educativa_items_plan_becas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `submenus`
--
ALTER TABLE `submenus`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `subsubmenus`
--
ALTER TABLE `subsubmenus`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `vinculacion_empresarial`
--
ALTER TABLE `vinculacion_empresarial`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `admitidos`
--
ALTER TABLE `admitidos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `aspirante_items_tabs`
--
ALTER TABLE `aspirante_items_tabs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `aspirante_menus`
--
ALTER TABLE `aspirante_menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `aspirante_menus_items`
--
ALTER TABLE `aspirante_menus_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `catalogo_docente`
--
ALTER TABLE `catalogo_docente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `catalogo_docente_tags`
--
ALTER TABLE `catalogo_docente_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `catalogo_facultades`
--
ALTER TABLE `catalogo_facultades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `centro_componentes`
--
ALTER TABLE `centro_componentes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `centro_componentes_items`
--
ALTER TABLE `centro_componentes_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `centro_info_menus`
--
ALTER TABLE `centro_info_menus`
  MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT de la tabla `ciudad_tags`
--
ALTER TABLE `ciudad_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `componentes`
--
ALTER TABLE `componentes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `componentes_items`
--
ALTER TABLE `componentes_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `componentes_tipos`
--
ALTER TABLE `componentes_tipos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `iconos_oferta`
--
ALTER TABLE `iconos_oferta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `micrositios_facultades`
--
ALTER TABLE `micrositios_facultades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `modulos_fijos`
--
ALTER TABLE `modulos_fijos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `noticias`
--
ALTER TABLE `noticias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `noticias_categorias`
--
ALTER TABLE `noticias_categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `oferta_educativa_items`
--
ALTER TABLE `oferta_educativa_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `oferta_educativa_items_cuotas`
--
ALTER TABLE `oferta_educativa_items_cuotas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `oferta_educativa_items_plan_becas`
--
ALTER TABLE `oferta_educativa_items_plan_becas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `submenus`
--
ALTER TABLE `submenus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `subsubmenus`
--
ALTER TABLE `subsubmenus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `test`
--
ALTER TABLE `test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `video`
--
ALTER TABLE `video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `vinculacion_empresarial`
--
ALTER TABLE `vinculacion_empresarial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
